#include "Automata.cuh"

// NOTE: put it here instead in Automata.cuh to avoid conflicts...
#include "AutomataKernels.cuh" // Automata kernels 
#include "Utility.cuh" // Utility functions
// ROOT 
#include <TROOT.h> 
#include <TTree.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TLine.h>


//===================================================
// Perform Automata tracking
//===================================================
cudaError_t Automata (const Batch& inputBatch,
                      std::vector<Data> &trackCollection) {

  // Grid/block size
  dim3 grid;
  dim3 block;

  // Set GPU device
#ifdef PROFILE_AUTOMATA
  CudaSafeCall( cudaSetDevice( 0 ) );
  cudaEvent_t fake;
  CudaSafeCall(cudaEventCreate(&fake));
#endif

  // Host objects
  const int nEvents = inputBatch.size(); 
  std::vector<uint8_t> buffer; 

  // Device objects
  uint8_t *d_buffer;
  PatFwdHitsContainer *d_hits;
  XTrackletsContainer *d_xtracklets;

  ////////////////////
  // Prepare to run //
  ////////////////////
  // Create data buffer
  // (find size and offset of each event in the buffer)
  for (auto data : inputBatch) {
    buffer.insert( buffer.end(), (*data).begin(), (*data).end() );
  }

#ifdef DEBUG_AUTOMATA

  float elapsedTime;
  cudaEvent_t start, stop;
  CudaSafeCall(cudaEventCreate(&start));
  CudaSafeCall(cudaEventCreate(&stop));
  CudaSafeCall(cudaEventRecord(start, 0));

#endif

  // Allocate memory on GPU
  CudaSafeCall( cudaMallocManaged( &d_buffer, buffer.size() * sizeof(uint8_t) ) );
  CudaSafeCall( cudaMalloc( &d_hits, nEvents*sizeof(PatFwdHitsContainer) ) );
  CudaSafeCall( cudaMalloc( &d_xtracklets, nEvents*sizeof(XTrackletsContainer) ) );

  memcpy(d_buffer, buffer.data(), buffer.size() * sizeof(uint8_t));
  
#ifdef DEBUG_AUTOMATA
  CudaSafeCall( cudaEventRecord(stop, 0));
  CudaSafeCall( cudaEventSynchronize(stop) );
  CudaSafeCall( cudaEventElapsedTime(&elapsedTime, start, stop) );
  std::cout << "Elapsed time for memory setup: " << elapsedTime << " ms" << std::endl;
#endif

  // Copy det info to constant memory
  Detector det; 
  det.set();
  copyToConstMem( &det );

  XTrackletsContainer *d_stubs;
  CudaSafeCall( cudaMalloc(&d_stubs, nEvents*sizeof(XTrackletsContainer)) );
  
  Candidates* d_candidates;
  CudaSafeCall( cudaMalloc(&d_candidates, nEvents*sizeof(Candidates)) ); 
  
  CAtracks* d_catracks;
  CudaSafeCall( cudaMalloc(&d_catracks, nEvents*sizeof(CAtracks)) );

  grid = dim3( nEvents );
  block = dim3( NUM_STA, NUM_REG );
  clearTracks <<< grid, block >>> ( d_xtracklets, d_stubs,  d_candidates, d_catracks );
  CudaCheckError();

  ///////////////////////
  // Unpack data batch //
  ///////////////////////
  grid = dim3( nEvents );
  block = dim3( NUM_STA, NUM_LAY, NUM_REG ); // TODO: fit to 32-threads warp size...
#ifdef DEBUG_AUTOMATA
  CudaSafeCall(cudaEventRecord(start, 0));
#endif
  unpack<<<grid, block>>> (d_buffer, d_hits);
  CudaCheckError();
#ifdef DEBUG_AUTOMATA
  CudaSafeCall( cudaEventRecord(stop, 0) );
  CudaSafeCall( cudaEventSynchronize(stop) );
  CudaSafeCall( cudaEventElapsedTime(&elapsedTime, start, stop) );
  std::cout << "Elapsed time for unpacking: " << elapsedTime << " ms" << std::endl;
#endif
  // free unused memory
  CudaSafeCall( cudaFree( d_buffer ) );

  //////////////////////
  // Make X-tracklets //
  //////////////////////
  grid = dim3( nEvents, NUM_STA, NUM_REG - 4 ); // OT only!
  block = dim3( 640 );
#ifdef DEBUG_AUTOMATA
  CudaSafeCall(cudaEventRecord(start, 0));
#endif

  makeXTracklets <<<grid, block>>> (d_hits, d_xtracklets);
  CudaCheckError();
#ifdef DEBUG_AUTOMATA
  CudaSafeCall( cudaEventRecord(stop, 0));
  CudaSafeCall( cudaEventSynchronize(stop) );
  CudaSafeCall( cudaEventElapsedTime(&elapsedTime, start, stop) );
  std::cout << "Elapsed time for making tracklets: " << elapsedTime << " ms" << std::endl;
#endif

  grid = dim3( nEvents, NUM_STA, NUM_REG - 4 ); // OT only!
  block = dim3( 640 );
  makeStereo <<<grid, block>>> (d_hits, d_xtracklets, d_stubs);
  CudaCheckError();
  // free unused memory
  CudaSafeCall( cudaFree( d_xtracklets ) );
  sortTracklets <<<grid, 1024>>> (d_hits, d_stubs);
  CudaCheckError();
  
  grid = dim3( MAX_XTRACK_PER_REG/1024, nEvents, 2 * (NUM_REG - 4)  ); // OT only!
  findNeighbours <<<grid, 1024>>> (d_hits, d_stubs);
  CudaCheckError();

  grid = dim3( nEvents, NUM_STA - 1, NUM_REG - 4 ); // OT only!
  linkTracklets<<< grid, 1024 >>>(d_stubs, d_candidates);
  CudaCheckError(); 
  
  selectTracks<<< dim3(1024,nEvents), 256 >>>(d_hits, d_stubs, d_candidates, d_catracks);
  CudaCheckError(); 

  // free unused memory
  CudaSafeCall( cudaFree( d_stubs ) ); 
  CudaSafeCall( cudaFree( d_candidates ) ); 
    
  LhcbOutTracks* d_lhcbtks;
  CudaSafeCall( cudaMallocManaged(&d_lhcbtks, nEvents*sizeof(LhcbOutTracks)) );   
  
  cudaDeviceSynchronize();
  for (int i=0; i<nEvents; ++i) 
    d_lhcbtks[i].clear();  
   
  int* d_indexes; 
  CudaSafeCall( cudaMalloc(&d_indexes, nEvents*MAX_OUTTRACK*sizeof(int)) );      
    
  killClones <<< nEvents, 1024 >>> (d_hits, d_catracks, d_indexes, d_lhcbtks);
  CudaCheckError();

  cudaDeviceSynchronize();
  for (int i = 0; i < nEvents; ++i) {
    int noTracks = d_lhcbtks[i].size();

    if ( noTracks >  0) {
      trackCollection[i].resize(noTracks * sizeof(LhcbOutTrack));
      LhcbOutTrack* solutionTracks = (LhcbOutTrack*)&trackCollection[i][0];
      int count = 0;
      for (int j = 0; j < noTracks; ++j)
        solutionTracks[count++] = d_lhcbtks[i][j];
      trackCollection[i].resize(count * sizeof(LhcbOutTrack));
    }
  }
  
  ////////////////////
  // Debug Automata //
  ////////////////////
#ifdef DEBUG_AUTOMATA
  printMemUsage();
#endif

  ///////////////////////////
  // Free memory on device //
  ///////////////////////////
  CudaSafeCall( cudaFree( d_hits ) );
  CudaSafeCall( cudaFree( d_catracks ) );
  CudaSafeCall( cudaFree( d_indexes ) );  
  CudaSafeCall( cudaFree( d_lhcbtks ) );
#ifdef PROFILE_AUTOMATA
  CudaSafeCall( cudaDeviceReset() );
#endif

  return cudaSuccess;
}
