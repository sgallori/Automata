#ifndef AUTOMATA_CUH
#define AUTOMATA_CUH 1

#include <iostream> // C++ 
#include <stdint.h>
#include <vector>

#include "cuda_runtime.h"  // CUDA 
 
typedef std::vector<uint8_t>     Data;
typedef std::vector<const Data*> Batch;

/* Main automata function */
cudaError_t Automata (const Batch &batch, std::vector<Data> &tracks);

#endif
