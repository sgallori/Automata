#include <algorithm>
#include <vector>
#include <ctime>

#include "AutomataCudaHandler.h"
#include "Automata.cuh"

using namespace std;

DECLARE_COMPONENT(AutomataCudaHandler)

void AutomataCudaHandler::operator() (
    const Batch & batch,
    Alloc         allocResult,
    AllocParam    allocResultParam) {

  vector<Data> trackCollection;
  
  trackCollection.resize(batch.size());

  Automata(batch, trackCollection);

  for (size_t i = 0; i < trackCollection.size(); ++i) {
    uint8_t * buffer = allocResult(i, trackCollection[i].size(), allocResultParam);
    copy(trackCollection[i].begin(), trackCollection[i].end(), buffer);
  }
}
