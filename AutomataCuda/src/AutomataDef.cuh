#ifndef AUTOMATA_DEF_CUH
#define AUTOMATA_DEF_CUH 1

//==========================
// Num. sta, lay, reg
//==========================
#define NUM_STA 3 
#define NUM_LAY 4 
#define NUM_REG 6 
#define PLANES 12 // STA*LAY 

//==========================
// Data packet size
//==========================
// Packet header size. Encode (sta, lay, reg, num. hits) per evt
#define HEADER_SIZE (4 * sizeof(uint32_t)) 

// Packet hit size. Encode (id, x, z, w, r, ybeg, yend) per hit
#define HIT_SIZE (sizeof(uint32_t) + 6 * sizeof(float)) 

//==========================
// SoA container size 
//==========================
// PatFwdHitsContainer size
#define MAX_HITS_PER_REG  1024 // Padding to keep a sort of "alignment" intra data
#define MAX_HITS_PER_EVT  (MAX_HITS_PER_REG * NUM_STA * NUM_LAY * NUM_REG) // (2^13 * 3^2 to keep powers of 2 in stations)
#define MAX_HITS          MAX_HITS_PER_EVT

// ClustersContainer size
#define MAX_CLUS MAX_HITS 

// X-tracklets container size
#define MAX_XTRACK_PER_REG  90112 // multiple of 1024
#define MAX_XTRACK_PER_STA  2 * MAX_XTRACK_PER_REG  // only OT 
#define MAX_XTRACK          NUM_STA * MAX_XTRACK_PER_STA
// CATracks container size 
#define MAX_OUTTRACK 32768 // must be a power of 2. 
#define MAX_LHCBTK 256

// Max. clusters per region (for OT!)
#define MAX_CLUS_PER_REG MAX_HITS_PER_REG

// maxNeighbours for tracklets
#define maxNB 4

// Max numbers of hits per fwd track or per candidate track
#define FWDHITS 50
#define CANDHITS 24

//==========================
// Debug flags
//==========================
//#define PROFILE_AUTOMATA 1
//#define DEBUG_AUTOMATA 1

//==========================
// Costants
//==========================
#define c_zReference     8520.
#define c_dRatio		     -3.2265e-4
#define c_initialArrow   4.25307e-09
// nominal slope of the plane => the rotation angle of the beamline respect the horizontal plane
#define c_dzDy           0.00360102
#define c_dxDy           0.0874892
// for layer u, v theta is 5 degrees
#define c_sinT           0.0871563


#endif
