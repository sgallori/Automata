#ifndef AUTOMATAKERNELS_CU
#define AUTOMATAKERNELS_CU 1

#include "AutomataKernels.cuh"
#include "AutomataTool.cuh"

// Round up to the next highest power of 2
__device__ unsigned long upper_power_of_two(unsigned long v) {
    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v++;
    return v;
}

__constant__ Detector d_det;

__host__ void copyToConstMem( Detector *det )
{ cudaMemcpyToSymbol( d_det, det, sizeof(Detector) ); }

// initialize all Tracks
__global__ void clearTracks ( XTrackletsContainer *cxtracklets, XTrackletsContainer *stubs, 
                             Candidates* candidates, CAtracks* catk) {
  const unsigned int evt = (blockIdx.x);  // event 
  const unsigned int sta = (threadIdx.x);  // station 
  const unsigned int reg = (threadIdx.y);  // region 
  cxtracklets[evt].clean(sta, reg);
  stubs[evt].clean(sta, reg);
  candidates[evt].clear();
  catk[evt].clear();
  return;
}

//=============================================================================

__device__ float dxDy( const int layer ) {
  return ( layer == 1 ) ? c_dxDy : ( layer == 2 ) ? -c_dxDy : 0;
}

//=============================================================================

__device__ float distanceHitToTrack( Tracklet& track, 
                           PatFwdHitsContainer* hits, const int hitidx, const int layer )  {

  float y = ( track.yAtZ(0.0) + track.getTy() * hits->getZ(hitidx) ) / ( 1. - c_dzDy * track.getTy() );
  float xHit = hits->getX(hitidx) + y * dxDy( layer ) ;
  float zHit = hits->getZ(hitidx) + y * c_dzDy;
  float dist = xHit - track.xAtZ( zHit );
  // IT next time
  // if (hit->hit()->type() == Tf::RegionID::IT) return dist;
  dist *= track.cosXZ();
  float dx = hits->getDriftDist( hitidx );
  if (fabs( dist - dx ) < fabs( dist + dx ))
	  return dist - dx;
  else
	  return dist + dx;
}

__device__ float distanceHitToTrack( FwdTrack& track, FwdHit fwdh ) {
	
	float dist = ( fwdh.x() - track.xAtZ( fwdh.z() ) ) * track.cosine();
	//if (!hit->isOT()) return dist/track.cosine();
	const float dx = fwdh.driftDistance();
	
  if (fabs( dist - dx ) < fabs( dist + dx ))
	  return dist - dx;
  else
	  return dist + dx;	   

}

//=============================================================================

__device__ float chi2Hit( Tracklet& track,
                           PatFwdHitsContainer* hits, const int hitidx, const int layer )  {
                           
  const float dist = distanceHitToTrack( track, hits, hitidx, layer );
  return dist * dist * hits->getWeight( hitidx );
}

__device__ float chi2Hit( FwdTrack& fwdt, FwdHit fwdh ) {

  const float dist = distanceHitToTrack( fwdt, fwdh);
  return dist * dist * fwdh.weight();
} 
                          
//=============================================================================

__device__ float chi2Cluster( Tracklet& track,
                           PatFwdHitsContainer* hits, const int clidx, const int layer )  {
  if ( clidx == -1 ) return 0;
  float chi2 = chi2Hit( track, hits, clidx, layer );
  if ( hits->isCluster( clidx ) == 2 ) 
    chi2 += chi2Hit( track, hits, clidx+1, layer );
  return chi2;
}                         

//=============================================================================

__device__ float chi2nDoF( Tracklet& track,
                           PatFwdHitsContainer* hits )  {
  float chi2 = 0.0;                        
  for (int i=0; i < 4; ++i)
    chi2 += chi2Cluster( track, hits, track.getCl(i), i);
  return chi2;                        
}


__device__ float chi2nDoF( FwdTrack& fwdt )  {
  float chi2 = 0.0;                        
  for (int i=0; i < fwdt.getNumHits(); ++i)
    if ( fwdt.hit(i).isUsed() )
      chi2 += chi2Hit( fwdt, fwdt.hit(i) );
  return chi2;                                 
                           
}                           

//=============================================================================
__device__ void updateHitForTrack ( PatFwdHitsContainer* hits, FwdHit* fhit,
                                  const double y0, const double dyDz) {

  const double zy0 = hits->getZ( fhit->index() );
  const double y  = ( y0 + dyDz * zy0 ) / ( 1. - fhit->dzDy( ) * dyDz );

  // ?????  TODO TODO
  /*
  if (fwdHits->isOT( idx ))
    hit.setDriftDistance(otHit->untruncatedDriftDistance(y));
  */

  fhit->setZ( zy0 + y * fhit->dzDy( ) );
  fhit->setX( hits->getX( fhit->index() ) + y * fhit->dxDy( ) );
  
  return;
}

__device__ void updateHitsForTrack ( PatFwdHitsContainer* hits, FwdTrack* track) {
 
  for (int i=0; i < track->getNumHits(); ++i ) 
    // update hit using y parameters: m_ay and m_by
    updateHitForTrack( hits, track->hitptr(i), track->yAtZ(0), track->ySlope(0) );

}

//=============================================================================
// returns ( wx +/- wdx ) - wproj
__device__ double wmeass(FwdTrack* track, FwdHit hit, bool RLAmb = false) {
    int idx = hit.index();
    double errweight = sqrt( hit.weight() );
    double w = errweight * track->cosine();
    double wdx = hit.driftDistance() * errweight;
    double wmeas = w * ( hit.x() - track->xAtZ( hit.z() ) ); 
    if ( RLAmb && 0 != hit.rlAmb() ) {
      if ( hit.rlAmb() > 0 )
        wmeas += wdx;    
      else
        wmeas -= wdx;   
    } else {
      if ( abs( wmeas + wdx ) < abs ( wmeas - wdx ) )
        wmeas += wdx;      
      else
        wmeas -= wdx;   
    }
    return wmeas;  
}


//=========================================================================
//  Fit procedures  
//=========================================================================

__device__ bool fitStub ( PatFwdHitsContainer* hits, FwdTrack* track, double arrow)  {

  bool retval = false;
  double pby, pax, pbx;
  unsigned int kk;
  
  FitParabola  parabola;
  
  for ( kk = 0; kk < 10; ++kk ) {

    parabola.reset();

    for (int itH = 0; itH < track->getNumHits(); ++itH) {
      FwdHit hit = track->hit(itH);
      int idx = hit.index();
      double errweight = sqrt(hits->getWeight( idx ));
      double w = errweight * track->cosine();
      double dz = ( hit.z() - track->z0() ) * 1.e-3;
      double y = hit.dxDy() * hit.z() / track->z0();
      double wdx = hit.driftDistance() * errweight;
      double wmeas = w * ( hit.x() - track->xAtZ( hit.z() ) );   
      if ( abs( wmeas + wdx ) < abs ( wmeas - wdx ) )
        wmeas += wdx;
      else
        wmeas -= wdx;  
        
      parabola.addPoint( -w * y, w, w * dz, wmeas);

    }
    
    if (!parabola.solve()) return false;
    pby = parabola.param0();
    pax = parabola.param1();
    pbx = parabola.param2() * 1.e-3;

    if ( fabs( pby ) < 1e3 &&
         fabs( pax ) < 1e3 &&
         fabs( pbx ) < 5.0  ) {
      track->updateParameters( pax, pbx, -arrow * (pax - track->z0() * pbx),
                              0, pby / track->z0() );
                              
      updateHitsForTrack( hits, track );
    } 

    if ( fabs( pby ) < 5e-3 &&  // should be 1e-5
         fabs( pax ) < 5e-3 &&
         fabs( pbx ) < 5e-6    ) {
          retval = true; 
          break;
      } // wait until stable, due to OT.
  }

  return retval;

} 

__device__ bool fitXY ( PatFwdHitsContainer* hits, FwdTrack* track, int maxIter )  {

  // enough parameters?
  if ( track->getNumHits() < 5 ) return false;

  bool retval = false;
  double wgrad[5];
  
  bool RLAmb = false;
  if ( maxIter == 1 ) RLAmb = true;
 
  FitCubic cubic;
  
  for ( int kk = 0; kk < maxIter; ++kk ) {
  
    cubic.reset();
    
    for (int itH = 0; itH < track->getNumHits(); ++itH) {
      FwdHit hit = track->hit(itH);
      if ( !hit.isUsed() ) continue;
      double errweight = sqrt( hit.weight() );
      double w = errweight * track->cosine();
      double dz = ( hit.z() - track->z0() ) * 1.e-3;
      double wmeas = wmeass(track, hit, RLAmb);
    
      wgrad[0] = w;
      wgrad[1] = w * dz;
      wgrad[2] = w * dz * dz * (1 + dz * track->dRatio() * 1.e3);
      wgrad[3] = -w * hit.dxDy();
      wgrad[4] = -w * hit.dxDy() * dz;
   
      cubic.addPoint( wgrad, wmeas );
    }
    
  if (!cubic.solve()) return false;

    track->updateParameters( cubic.sol(0), cubic.sol(1)*1.e-3, cubic.sol(2)*1.e-6,
                             cubic.sol(3)-track->z0()*cubic.sol(4)*1.e-3, cubic.sol(4)*1.e-3);
                           
    updateHitsForTrack( hits, track );
    
    if ( fabs( cubic.sol(0) ) < 5e-3 &&
         fabs( cubic.sol(1) ) < 5e-3 &&
         fabs( cubic.sol(2) ) < 5e-3 &&
         fabs( cubic.sol(3) ) < 5e-2 &&
         fabs( cubic.sol(4) ) < 5e-3    ) {
          retval = true; 
          break;
    } // wait until stable, due to OT.
  }
  
  // In one iteration we can't converge!
  if ( maxIter == 1 ) retval = true; 

  return retval;                         

}

/* 
__device__ bool fitX ( PatFwdHitsContainer* hits, FwdTrack* track )  {

  double pax, pbx, pcx;

  FitParabola  parabola;

  for (int itH = 0; itH < track->getNumHits(); ++itH) {
    FwdHit hit = track->hit(itH);
    int idx = hit.index();
    double errweight = sqrt( hit.weight() );
    double w = errweight * track->cosine();
    double dz = ( hit.z() - track->z0() ) * 1.e-3;
    double wmeas = wmeass(track, hit);
       
    parabola.addPoint( w, w*dz, w * dz * dz * (1 + dz * track->dRatio() * 1.e3), wmeas);
  }
    
  if (!parabola.solve()) return false;
  pax = parabola.param0();
  pbx = parabola.param1() * 1.e-3;
  pcx = parabola.param2() * 1.e-6;

  track->updateParameters( pax, pbx, pcx, 0, 0 );
                              
  updateHitsForTrack( hits, track );


  // we are not interesting in convergence    
  if ( fabs( pax ) < 5e-3 &&  
       fabs( pbx ) < 5e-6 &&
       fabs( pcx ) < 5e-9    ) {
        retval = true; 
  } 

  
  return true;
} 

__device__ bool fitY ( PatFwdHitsContainer* hits, FwdTrack* track )  {

  bool retval = false;
  double pay, pby;

  FitLine  line;

  for (int itH = 0; itH < track->getNumHits(); ++itH) {
    FwdHit hit = track->hit(itH);
    int idx = hit.index();
    double errweight = sqrt(hits->getWeight( idx ));
    double w = errweight * track->cosine();
    double dz = ( hit.z() - track->z0() ) * 1.e-3;
    double wmeas = wmeass(track, hit);
       
    line.addPoint( -w * hit.dxDy(), -w * hit.dxDy() * dz,  wmeas);
  }
    
  if (!line.solve()) return false;
  pay = line.param0();
  pby = line.param1() * 1.e-3;

  track->updateParameters( 0, 0, 0, pay - track->z0() * pby, pby );
                              
  updateHitsForTrack( hits, track );
     
  if ( fabs( pay ) < 5e-2 &&  
       fabs( pby ) < 5e-6    ) {
        retval = true; 
  } 
  
  return retval;
} 
*/

//=========================================================================
// return true if tk1 < tk2
//=========================================================================
__device__ bool compare(FwdTrack* tk1, FwdTrack* tk2) {
  if (tk1->getNumHits() < tk2->getNumHits()) return true;
	if (tk1->getNumHits() > tk2->getNumHits()) return false;
	return tk1->chi2PerDoF() > tk2->chi2PerDoF();
}

//=========================================================================
// return frac of used hits
//=========================================================================
__device__ float fracUsed(PatFwdHitsContainer* hits, FwdTrack* cand) {
  int tot = 0;
  for (int i = 0; i < cand->getNumHits(); ++i)
    if ( hits->isUsed( cand->hitptr( i )->index() ) > 1 ) tot++;
  return float(tot) / float( cand->getNumHits() );
}

//=========================================================================
// iterating procedure which removes hits with large chi2
//=========================================================================
__device__ bool removeHitsWhileChi2TooLarge(PatFwdHitsContainer* hits, FwdTrack* track, 
                                            const double maxChi2, const unsigned minPlanes) {

  /// little helper to make things a bit more readable
  struct WorstHitFinder {
    double m_chi2;
    double m_worstchi2;
    int m_worst;
    __device__ WorstHitFinder(FwdTrack* track) : m_chi2(0), m_worstchi2(0), m_worst(0) {
      for (int it = m_worst; it < track->getNumHits(); ++it) {
	      if ( !track->hit(it).isUsed() ) continue;
	      double wmeas = wmeass(track, track->hit(it));
	      double chi2hit = wmeas * wmeas;
	      m_chi2 += chi2hit;
	      if (chi2hit > m_worstchi2) m_worstchi2 = chi2hit, m_worst = it;
      }
    }
  };
  
  // keep track of number of degrees of freedom ourselves...
  int nHitsX = -3;
  int nHitsStereo = -2;
  for (int it = 0; it < track->getNumHits(); ++it) {
    if (!track->hit(it).isUsed()) continue;
    if (track->hit(it).isX()) ++nHitsX;
    else ++nHitsStereo;
  }
  // ... and stop early if we don't have enough hits
  if (0 > nHitsX || 0 > nHitsStereo) return false;
  
  if ( !fitXY( hits, track, 10) ) return false;
  
  FwdCounter planeCount( track->hits(), 0, track->getNumHits() );
 
  bool okay = true;
 
  do {
    WorstHitFinder worst(track);
    
    okay = (!(0. < maxChi2) || maxChi2 >= worst.m_worstchi2) &&
      minPlanes <= planeCount.nbDifferent() && 0 <= nHitsX && 0 <= nHitsStereo;

    // if we're above thresholds, and can remove hits, do so, and refit
    if (!okay) {
      // keep track of what we're about to remove...
      --nHitsX;
      planeCount.removeHit( track->hit( worst.m_worst ) );
      // ... remove ...
      track->hitptr( worst.m_worst )->setIsUsed( false );
      // ... and refit only if we know it should work...
      const bool expectFitOkay = 0 <= nHitsX && 0 <= nHitsStereo && 
                                minPlanes <= planeCount.nbDifferent() &&
                                track->countUsed() > 5;
      if ( !expectFitOkay || !fitXY( hits, track, 10 ) ) {
        return false;
      }
      // force next iteration
      continue;
    }
    // okay, we're done (either below chi^2 threshold, or not enough planes)
    // set chi2/ndf, but avoid division by 0
    if ( track->countUsed() > 5 )
      track->setChi2PerDoF(worst.m_chi2 / ( track->countUsed() - 5 ));
    else
      track->setChi2PerDoF(99999.0);
  } while (!okay);
  
  return okay;

}

//=========================================================================
//  Check if a candidate track can be a track
//=========================================================================
__device__ bool checkCand( PatFwdHitsContainer* hits, FwdTrack* fwd, int longtk ) {

  /* Constants */
  unsigned int minNbPlanes = 10; // Min. number of layers a tracks should have
  unsigned int maxNbOutliers = 15; // Max. number of outliers
  double maxChi2 = 6.0; // Max. chi2
  double minQuality = -0.5; // Min. value for track quality 
  // Track quality
  double w0 = 0.0930419521624;
  double w1 = -0.208728666352;
  double w2 = -0.124468775361;
  double w3 = 0.17558325965;
  double w4 = -0.167157852572;
  
  if ( longtk == -1 ) { // short track
    minNbPlanes = 7;
    maxNbOutliers = 2;
    maxChi2 = 5.0;
    minQuality = 0.0;
    w0 = -1.15104136013;
    w1 = -0.126391514861;
    w2 = -0.140524353725;
    w3 = 0.154541380645;
    w4 = -0.0097035742262; 
  }     
       
  // fit fwd
  if ( !fitXY( hits, fwd, 1 ) ) { return false; }
  if ( !removeHitsWhileChi2TooLarge( hits, fwd, maxChi2, minNbPlanes) ) { return false; }

  int used = fwd->countUsed();
  
  // Cut on chi2/nDoF
  double chi2nd = chi2nDoF(*fwd) / float(used - 5);
  if (chi2nd > maxChi2) return false;
  fwd->setChi2PerDoF( chi2nd );

  // Cut on num. of outliers
  unsigned int nOutliers = fwd->getNumHits() - used;
  if ( nOutliers > maxNbOutliers ) return false;
 
  FwdCounter planeCount( fwd->hits(), 0, fwd->getNumHits() );
  
  // Track quality
  double quality = w0 +
                   w1 * chi2nd +
                   w2 * nOutliers +
                   w3 * used +
                   w4 * planeCount.nbDifferent();
 		               
  // Cut on track quality
  if ( quality < minQuality ) return false;
  
  return true;
}


//=====================================================
// Make X-tracklets
//=====================================================
__global__ void makeXTracklets ( PatFwdHitsContainer *chits,
                                 XTrackletsContainer *cxtracklets ) {
                                 
  const unsigned int evt = (blockIdx.x);  // event 
  const unsigned int sta = (blockIdx.y);  // station 
  const unsigned int reg = (blockIdx.z);  // region 
  const unsigned int tid = (threadIdx.x); // threads index  

  PatFwdHitsContainer *hits = &chits[evt];
  XTrackletsContainer *xtracklets = &cxtracklets[evt];
  
  // Load clusters on lay3 on shared mem.!
  __shared__ float x3_shared[MAX_CLUS_PER_REG];
  __shared__ float z3_shared[MAX_CLUS_PER_REG];
  __shared__ int   isClus_shared[MAX_CLUS_PER_REG];
  __shared__ int   nclus3_shared;

  for (int id = tid; id < hits->getNHits(sta, 3, reg); id+=blockDim.x) {
    int idx3 = hits->offset(sta, 3, reg) + id;
    isClus_shared[id] = hits->isCluster(idx3);
    x3_shared[id] = hits->xCluster(idx3);
    z3_shared[id] = hits->zCluster(idx3);
  }
  if (tid == 0 ) nclus3_shared = hits->getNHits(sta, 3, reg);

  __syncthreads();

  /* Constants */
  // Search window for hits on lay3
  const float maxDx = 150.0; //mm
  // Max. dx/dz slope
  const float maxSlope = 1.5; //rad
  // Max. ellipse in x1/(z1-zMag) vs tx plane
  const float maxEllipse = 1.0; 
  // Ellipse constants
  const float invsigma = 2; // 1/sigma
  const float rho = 0.95;

  const float zRatio = d_det.zLay3[sta][reg] / d_det.zLay0[sta][reg];

  for (int id = tid; id < hits->getNHits(sta, 0, reg); id+=blockDim.x) {
    if ( !hits->isCluster( sta, 0, reg, tid) ) continue;
    // Load clus0 
    int idx0 = hits->offset(sta, 0, reg) + id;
    float x0 = hits->xCluster(idx0);
    float z0 = hits->zCluster(idx0);
    float x3Pred = x0 * zRatio; // assume track points to the origin
    float f = invsigma * x0 / (z0 - d_det.zMagnet); // used in elliptic calculation
    float fsquare = f * f;

    // Loop over clusters on lay3
    for (int i3 = 0; i3 < nclus3_shared; i3++) {
      if ( !isClus_shared[i3] ) continue;
      float x3 = x3_shared[i3];
      float z3 = z3_shared[i3];

      // Search inside a window around predicted x2-position
      if (x3 - x3Pred < -maxDx) continue;
      if (x3 - x3Pred > maxDx) break;
    
      // Cut on dx/dz
      float tx = (x3 - x0) / (z3 - z0);
      if (fabs(tx) > maxSlope) continue;
 
      // Elliptic cut on x0 / (z0-zMag) vs tx
      float s = tx * invsigma;
      float ell = fsquare + s*s - 2.0 * rho * f * s;
      if (ell > maxEllipse) continue;

      Tracklet xtck(idx0, hits->offset(sta, 3, reg)+i3);
      xtck.setParam(0.5*(x0+x3), 0.5*(z0+z3), tx);
      if ( xtracklets->getNumTk( sta, reg) >= MAX_XTRACK_PER_REG ) return;
      xtracklets->atomicaddTracklet( sta, reg, xtck ); 

    } // cluster 3
  } // cluster 0  
}

__global__ void makeStereo( PatFwdHitsContainer *chits, 
                            XTrackletsContainer *cxtracklets, 
                            XTrackletsContainer *cstubs ) {

  const unsigned int evt = (blockIdx.x);  // event 
  const unsigned int sta = (blockIdx.y);  // station 
  const unsigned int reg = (blockIdx.z);  // region 
  const unsigned int tid = (threadIdx.x); // thread index  
  
  PatFwdHitsContainer *hits = &chits[evt];

  // Load clusters on layU/V on shared mem.!
  __shared__ float xU_shared[MAX_CLUS_PER_REG];
  __shared__ float zU_shared[MAX_CLUS_PER_REG];
  __shared__ int   isClusU_shared[MAX_CLUS_PER_REG];
  
  __shared__ float xV_shared[MAX_CLUS_PER_REG];
  __shared__ float zV_shared[MAX_CLUS_PER_REG];
  __shared__ int   isClusV_shared[MAX_CLUS_PER_REG];
  
  __shared__ float dUMin;
  __shared__ float dUMax;
  __shared__ float dVMin;
  __shared__ float dVMax;
  
  if ( tid == 0 ) {
    // U Range
    dUMin = fabs(hits->getYMax(sta, 1, reg) * dxDy(1));
    dUMax = fabs(hits->getYMin(sta, 1, reg) * dxDy(1));
  // V range
    dVMin = fabs(hits->getYMin(sta, 2, reg) * dxDy(2));
    dVMax = fabs(hits->getYMax(sta, 2, reg) * dxDy(2));  
  }

  for (int id = tid; id < hits->getNHits(sta, 1, reg); id+=blockDim.x) {
    int idxU = hits->offset(sta, 1, reg) + id;
    isClusU_shared[id] = hits->isCluster(idxU);
    xU_shared[id] = hits->xCluster(idxU);
    zU_shared[id] = hits->zCluster(idxU);
  }  

  for (int id = tid; id < hits->getNHits(sta, 2, reg); id+=blockDim.x) {
    int idxV = hits->offset(sta, 2, reg) + id;
    isClusV_shared[id] = hits->isCluster(idxV);
    xV_shared[id] = hits->xCluster(idxV);
    zV_shared[id] = hits->zCluster(idxV);
  }   
  
  __syncthreads();

  /* Constants */
  // Search window for x
  const float maxDistFromPred = 6.0; //m
  // Max. dy/dz slop
  const float maxSlopeY = 0.5; //rad
  
  XTrackletsContainer *xtracklets = &cxtracklets[evt]; // in                            
  XTrackletsContainer *stubs = &cstubs[evt];           // out               
                        
  for (int id = tid; id < xtracklets->getNumTk(sta, reg); id += blockDim.x) {
    Tracklet xtck = xtracklets->getTracklet(sta, reg, id);
//    int nhits0 = hits->isCluster( xtck.getCl(0) );
//    int nhits3 = hits->isCluster( xtck.getCl(3) );    
    
    const float shift = xtck.getX() - xtck.getTx() * xtck.getZ();
  
    // Loop over hits in u-layer
    for (int iU = 0; iU < hits->getNHits(sta, 1, reg); ++iU) {
      if ( !isClusU_shared[iU] ) continue;
      float xU = xU_shared[iU];
      float zU = zU_shared[iU];
   
      // Hits must be inside sensitive area 
      float valU = xU - xtck.getTx() * zU - shift;
	    if ( valU < -dUMin ) continue;
	    if ( valU > dUMax) break;

	    float yU = (xtck.xAtZ( zU ) - xU) / dxDy( 1 ); 
    
      // Loop over hits in v-layer
      for (int iV = 0; iV < hits->getNHits(sta, 2, reg); ++iV) {
        if ( !isClusV_shared[iV] ) continue;
        float xV = xV_shared[iV];
        float zV = zV_shared[iV];
     
        // Hits must be inside sensitive area
	      float valV = xV - xtck.getTx() * zV - shift;
	      if ( valV < -dVMin) continue;
	      if ( valV > dVMax) break;
         
        // Cut on hit - track position at v-layer
        float xAtV = xtck.xAtZ( zV );
	      float yV = (xAtV - xV) / dxDy(2); 
	      if (std::abs( xV + dxDy(2) * yU * (zV/zU) - xAtV ) > maxDistFromPred) continue;
      
        // Cut on dy/dz slope
	      float ty = (yU/zU + yV/zV) / 2.0;
	      if (std::abs(ty) > maxSlopeY) continue;
      
        Tracklet stub(xtck.getCl(0), hits->offset(sta, 1, reg)+iU, hits->offset(sta, 2, reg)+iV, xtck.getCl(3));
        stub.setParam(xtck.getX(), 0.5*(yU+yV), xtck.getZ(), xtck.getTx(), ty);

/*
        // Cut on chi2nDoF  
        // Max. chi2
        double maxChi2 = 300.0;
	      float chi2s = 0.0;
	      unsigned int nDoFs = nhits0 + nhits3 + isClusV_shared[iV] + isClusU_shared[iU] - 3;
	      chi2s = chi2nDoF( stub, hits );
	      if ( chi2s > nDoFs*maxChi2 ) continue;
	        
	      FwdTrack fwd = FwdTrack(stub, hits, c_zReference, c_dRatio, c_initialArrow);
	      updateHitsForTrack( hits, &fwd );
	      
	      // fit stub
	      if (!fitStub( hits, &fwd, c_initialArrow)) continue;
	      
	      // Cut on chi2/nDoF after fit
	      maxChi2 = 200.0;  
        chi2s = chi2nDoF( fwd );
        if (chi2s > nDoFs*maxChi2) continue; 

        // Set params
        double z0 = stub.getZ();
	      stub.setParam( fwd.xAtZ(z0), fwd.yAtZ(z0), z0, fwd.xSlope(z0), fwd.ySlope(z0) );
        stub.setChi2( chi2s/float(nDoFs) );
*/
        // Save stubs
        if ( stubs->getNumTk( sta, reg) >= MAX_XTRACK_PER_REG ) return;
        stubs->atomicaddTracklet( sta, reg, stub ); 

      } // cluster V
    } // cluster U
  } // tracks      
}       

__global__ void sortTracklets(PatFwdHitsContainer *chits,
                           XTrackletsContainer *cstubs ) {
  
  
  const unsigned int evt = (blockIdx.x);  // event 
  const unsigned int sta = (blockIdx.y);  // station 
  const unsigned int reg = (blockIdx.z);  // region 
  const unsigned int tid = (threadIdx.x); // thread index  

  PatFwdHitsContainer *hits = &chits[evt];
  XTrackletsContainer *stubs = &cstubs[evt]; 
 
  float zmid = 0.5 * ( hits->zRegion( sta, 0, reg ) + hits->zRegion( sta, 3, reg ) ); 
  if (sta == 0 )  zmid = 0.5 * ( hits->zRegion( sta+1, 0, reg ) + hits->zRegion( sta+1, 3, reg ) ); 
  
  int countTk = stubs->getNumTk(sta, reg);
  int last = upper_power_of_two( countTk );
  int off = stubs->offset(sta, reg);
  
  Tracklet fake(-1, -1);
  fake.setParam(99999, 0, 0);
  
  __syncthreads();

  // complete the vector with fake tracks
  for ( int s = countTk+tid; s < last; s+=blockDim.x)
    stubs->atomicaddTracklet( sta, reg, fake ); 
 

   __syncthreads();

  // sort tracklet by x(zmiddle) 
  for (int k = 2; k <= last; k *= 2) { // Parallel bitonic sort 
    for (int j = k / 2; j>0; j /= 2) { // Bitonic merge
      for (unsigned int id = tid; id < last; id+=blockDim.x) {
        int ixj = id ^ j; //XOR
        if (ixj > id) {
          if ((id & k) == 0) { // ascending – descending
            if ( stubs->getTracklet(sta, reg, id).xAtZ(zmid) > stubs->getTracklet(sta, reg, ixj).xAtZ(zmid) )
              stubs->swaptk(off + id, off + ixj);
          } else {
            if ( stubs->getTracklet(sta, reg, id).xAtZ(zmid) < stubs->getTracklet(sta, reg, ixj).xAtZ(zmid) )
              stubs->swaptk(off + id, off + ixj);
          }
        }
      }
      __syncthreads();
    }
  }
  
  __syncthreads();
  
  // reset tk counter
  if (tid == 0 ) stubs->resetCounter(sta, reg, countTk);
   
}  


__global__ void findNeighbours( PatFwdHitsContainer *chits,
                           XTrackletsContainer *cstubs ) {
                            
  const unsigned int evt = (blockIdx.y);  // event 
  const unsigned int idx = (blockIdx.z);  // 2*region
  
  unsigned int tid = (blockIdx.x * blockDim.x) + threadIdx.x;
  
  unsigned int sta = idx/2;
  unsigned int reg = idx%2;
  
  XTrackletsContainer *stubs = &cstubs[evt];   
  
  //if ( s >= stubs->getNumTk(sta, reg) ) return;
  
  // Max. neighbours per tracklet
  unsigned int maxNbNeighbours = maxNB; //4
  // Search window in x
  const float dxPredMax = 30.0; //30.0 mm
  // Search window in y
  const float dyPredMax = 60; //60.0 mm
  // Max. kink in x
  const float dtxMax = 0.05; //0.05 rad
  // Max. kink in y
  const float dtyMax = 0.01; //0.01 rad

  PatFwdHitsContainer *hits = &chits[evt];
  
  float zNext = 0.5 * ( hits->zRegion( sta+1, 0, reg ) + hits->zRegion( sta+1, 3, reg ) ); 
  int nNeighbours = 0;
  
  // set offsets
  int offNS = stubs->offset(sta+1, reg);
  
  for (unsigned s = tid; s < stubs->getNumTk(sta, reg); s+=blockDim.x*gridDim.x) { 
  
    int ids = stubs->offset(sta, reg) + s;
    float dz = zNext - stubs->getZ0( ids );
    float xStub = stubs->getX0( ids ) + stubs->getTx( ids ) * dz;
    float yStub = stubs->getY0( ids ) + stubs->getTy( ids ) * dz;
   
    unsigned int ns = 0;
    // Find first compatible stub in station 
    while ( ns < stubs->getNumTk(sta+1, reg) ) {
      int idns = offNS + ns;
      float dzn = zNext - stubs->getZ0( idns );
      float xNextStub = stubs->getX0( idns ) + stubs->getTx( idns ) * dzn;  	 
      if ( xNextStub < xStub - dxPredMax ) ns++;
      else break;    
    }
    
    // loop over stubs on the next station
    for (;ns < stubs->getNumTk(sta+1, reg); ++ns) {
      // Stop search, enough neighbours!
      if ( nNeighbours >= maxNbNeighbours ) break;      
     
      int idns = offNS + ns;
      float dzn = zNext - stubs->getZ0( idns );
      float xNextStub = stubs->getX0( idns ) + stubs->getTx( idns ) * dzn;
      
      // Search only in a window around pred x
      if ( xNextStub > xStub + dxPredMax ) break; // stubs are sorted

      // Require small kink angles between them
      if ( abs( stubs->getTx( idns ) - stubs->getTx( ids ) ) > dtxMax ) continue;
      if ( abs( stubs->getTy( idns ) - stubs->getTy( ids ) ) > dtyMax ) continue;
 
      // Open search window around pred y
      float yNextStub = stubs->getY0( idns ) + stubs->getTy( idns ) * dzn;
      if ( abs(yNextStub - yStub) > dyPredMax ) continue;       
       
      stubs->setNB( ids, nNeighbours++, idns );
    }
  }
} 

__global__ void linkTracklets( XTrackletsContainer *cstubs,
                           Candidates *tkcand  ) {

  const unsigned int evt = (blockIdx.x);  // event 
  const unsigned int sta = (blockIdx.y);  // station ( 0 or 1 )
  const unsigned int reg = (blockIdx.z);  // region 
  const unsigned int tid = (threadIdx.x); // thread index  

  XTrackletsContainer *stubs = &cstubs[evt]; // in 

  int offS  = stubs->offset(sta, reg);
  int tracklet1, tracklet2, tracklet3;                           
   
  for (unsigned s = tid; s < stubs->getNumTk(sta, reg); s+=blockDim.x) {
    tracklet1 = offS + s;
    for ( unsigned idxn = 0; idxn < stubs->getnNB( tracklet1 ); ++idxn ) {
      tracklet2 = stubs->getNB( tracklet1, idxn );
      for ( unsigned idxnn = 0; idxnn < maxNB; ++idxnn ) {
        tracklet3 = stubs->getNB( tracklet2, idxnn );
        Candidate c;
        c.x[0] = tracklet1;
        c.x[1] = tracklet2;
        c.x[2] = tracklet3;
        tkcand[evt].atomicadd(c);
        if ( tracklet3 == -1 ) break;

      }
    }
  }                          
}
__global__ void selectTracks( PatFwdHitsContainer *chits,
                            XTrackletsContainer *cstubs,
                           Candidates *tkcand,
                           CAtracks *catracks ) {
                            
  const unsigned int evt = (blockIdx.y);  // event   
  unsigned int tid = (blockIdx.x * blockDim.x) + threadIdx.x;
  
  PatFwdHitsContainer *hits = &chits[evt];
  XTrackletsContainer *stubs = &cstubs[evt];   
  
  for (unsigned s = tid; s < tkcand[evt].size(); s+=blockDim.x*gridDim.x) { 
    Candidate c = tkcand[evt][s];
    FwdTrack fwd = FwdTrack( stubs->getTracklet(c.x[0]), hits, stubs->getZ0(c.x[0]), c_dRatio, c_initialArrow); 
    fwd.addTrackletCl(hits, stubs->getTracklet(c.x[1]));
    if ( c.x[2] != -1 ) fwd.addTrackletCl(hits, stubs->getTracklet(c.x[2]));
    updateHitsForTrack( hits, &fwd );
    if ( checkCand( hits, &fwd, c.x[2] ) ) {    
      fwd.cleanHits();
      catracks[evt].atomicadd(fwd);
      for (int i = 0; i < fwd.getNumHits(); ++i ) 
        hits->setUsed( fwd.hitptr(i)->index(), true );      
    }
  }
}


__global__ void killClones(PatFwdHitsContainer *chits, CAtracks *catracks, int *indexes, LhcbOutTracks *lhcbtks )  {

  const unsigned int evt = (blockIdx.x);  // event 
  const unsigned int tid = (threadIdx.x); // thread index    
  
  // Max. nUsed/nHits on track
  const double maxFracUsed = 0.30; //0.30
  
  int shift = evt*MAX_OUTTRACK;
  int countTk = catracks[evt].size();
  int last = upper_power_of_two( countTk );

  FwdTrack fake;  
  fake.forcenHits(CANDHITS);
  fake.setChi2PerDoF(0.0);
  
  __syncthreads();
  
  for (int s = tid; s < countTk; s+=blockDim.x )
    indexes[shift+s] = s;

  // complete the vector with fake tracks
  for ( int s = countTk+tid; s < last; s+=blockDim.x ) {
    catracks[evt].atomicadd( fake );
    indexes[shift+s] = s;
  }
  
  __syncthreads();

  // sort candidates in reverse order: worst first
  for (int k = 2; k <= last; k *= 2) { // Parallel bitonic sort 
    for (int j = k / 2; j>0; j /= 2) { //Bitonic merge
      for (unsigned int id = tid; id < last; id+=blockDim.x) {
        int ixj = id ^ j; //XOR
        if (ixj > id) {
          if ((id & k) != 0) { // descending - ascending
            if ( compare( &catracks[evt][indexes[shift+id]], &catracks[evt][indexes[shift+ixj]] ) ) {
              int tmp = indexes[shift+id];
              indexes[shift+id] = indexes[shift+ixj];
              indexes[shift+ixj] = tmp;
            }  
          } else {
            if ( compare( &catracks[evt][indexes[shift+ixj]], &catracks[evt][indexes[shift+id]] ) ) {
              int tmp = indexes[shift+id];
              indexes[shift+id] = indexes[shift+ixj];
              indexes[shift+ixj] = tmp;              
            }  
          }
        }
      }
      __syncthreads();
    }
  }
  
  __syncthreads();

  if ( tid == 0 ) {    
    for ( int id = 0; id < countTk; id++ ) {
      int i = indexes[shift+id];
      if ( fracUsed(&chits[evt], &catracks[evt][i] ) >= maxFracUsed ) {
        for (int j = 0; j < catracks[evt][i].getNumHits(); ++j )
          chits[evt].setUsed( catracks[evt][i].hitptr(j)->index(), false ); 
      } else {
        LhcbOutTrack lhcbtk;
        lhcbtk.x0 = catracks[evt][i].xAtZ(c_zReference);
        lhcbtk.y0 = catracks[evt][i].yAtZ(c_zReference);
        lhcbtk.tx = catracks[evt][i].xSlope(c_zReference);
        lhcbtk.ty = catracks[evt][i].ySlope(c_zReference);
        lhcbtk.chi2xdof = catracks[evt][i].chi2PerDoF();
        lhcbtk.curvature = catracks[evt][i].curvature();
        lhcbtk.HitsNum = catracks[evt][i].getNumHits();
        for (int j = 0; j < catracks[evt][i].getNumHits(); ++j )
          lhcbtk.Hits[j] = chits[evt].getLhcbId( catracks[evt][i].hitptr(j)->index() );
        lhcbtks[evt].add(lhcbtk);
      }
    }
  }   
} 

#endif               
