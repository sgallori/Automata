#ifndef KERNELS_CUH
#define KERNELS_CUH 1

#include <stdio.h> // C++
#include <stdint.h>

//#include <thrust/host_vector.h> // Thrust lib
//#include <thrust/device_vector.h>

#include "Det.cuh" // Det. description
#include "PatFwdHitsContainer.cuh" // Containers
#include "TrackletsContainer.cuh"

/* Unpack data batch from clients */
__global__ void unpack( uint8_t *buffer, 
                        PatFwdHitsContainer *hits );

/* Make X-tracklets */
__global__ void clearTracks ( XTrackletsContainer*, XTrackletsContainer*, Candidates*, CAtracks* );
__global__ void makeXTracklets( PatFwdHitsContainer*, XTrackletsContainer* );
__global__ void makeStereo( PatFwdHitsContainer*, XTrackletsContainer*, XTrackletsContainer * );       
__global__ void sortTracklets(PatFwdHitsContainer*, XTrackletsContainer* ); 
__global__ void findNeighbours( PatFwdHitsContainer*, XTrackletsContainer* );
__global__ void linkTracklets( XTrackletsContainer*, Candidates* );                                
__global__ void selectTracks( PatFwdHitsContainer*, XTrackletsContainer*, Candidates*, CAtracks* );
__global__ void killClones(PatFwdHitsContainer *chits, CAtracks *catracks, int*, LhcbOutTracks * );                        

__host__ void copyToConstMem( Detector *det );

#endif

