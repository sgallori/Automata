#ifndef AUTOMATATOOL_CUH
#define AUTOMATATOOL_CUH 1


// template for a std::vector-like class handling vectors of fixed dimension
template<class A, unsigned int N>
class PVector {
  public:
    // constructor
    __host__ __device__ PVector(): m_size (0) {}

    __device__ PVector(const PVector& v) {
      this->m_size = v.m_size;
      for (int i = 0; i < this->m_size; i++) 
        this->m_items[i] = v.m_items[i];
    }

    __device__ PVector& operator=(const PVector& v) {
      this->m_size = v.m_size;
      for (int i = 0; i < this->m_size; i++) 
        this->m_items[i] = v.m_items[i];
      return *this;
    }

    // members
    __host__ __device__ A& operator[] (unsigned int n) { return m_items[ n ]; }
    __host__ __device__ unsigned int size() const { return m_size; }
    __device__ A& front() { return m_items[ 0 ]; }
    __device__ A& back()  { return m_items[ m_size-1 ]; }
    __host__ __device__ void clear() { m_size = 0; }
    __device__ bool empty() const { return m_size == 0; }
    __device__ bool full() const { return m_size >= N; }
    __device__ void forceSize( unsigned int n ) { m_size = n; }
    __device__ A* elements() { return m_items; }

    // swap elements
    __device__ void swap(int i, int j) {
      if ( (i >= m_size) || (j >= m_size) ) {
        printf("ERROR! Index overflow!\n");
        return;
      }
      A tmp = m_items[i];
      m_items[i] = m_items[j];
      m_items[j] = tmp;
      return;
    }
    
    __device__ void sort() {
      int idx;

      for (int i = 0; i < m_size-1; ++i) {
        idx = i;
        for (int j = i + 1; j < m_size; ++j)
          if ( m_items[j] < m_items[idx] ) idx = j;
        if ( i != idx ) swap(idx, i);
      }
    }

    // add element and increase size
    __device__ void add(const A& a) {
      if (m_size < N) 
        m_items[m_size++] = a;
    }
     // atomicadd element, returns the index of the added element
    __device__ int atomicadd(const A& a) {
      int iold = atomicAdd (&m_size, 1);
      if (iold < N)
        m_items[iold] = a;
      else {
        printf("ERROR: PVector atomicadd: overflow detected (size = %i) \n", N);
        m_size = N;
      }
      return iold;
    }

    A m_items[N];
    unsigned int m_size;

};

__device__ static float atomicMax(float* address, float val)
{
    int* address_as_i = (int*) address;
    int old = *address_as_i, assumed;
    do {
        assumed = old;
        old = ::atomicCAS(address_as_i, assumed,
            __float_as_int(::fmaxf(val, __int_as_float(assumed))));
    } while (assumed != old);
    return __int_as_float(old);
}

__device__ static float atomicMin(float* address, float val)
{
    int* address_as_i = (int*) address;
    int old = *address_as_i, assumed;
    do {
        assumed = old;
        old = ::atomicCAS(address_as_i, assumed,
            __float_as_int(::fminf(val, __int_as_float(assumed))));
    } while (assumed != old);
    return __int_as_float(old);
}                                                                                      


//=============================================================================
// Simple class to fit


//=============================================================================
// 3 parameters
class FitParabola {

  public:
    /// Standard constructor
    __device__ FitParabola( ) {
      reset();
    }

     __device__ ~FitParabola( ) {}; ///< Destructor

    __device__ void reset() {
      for ( int i = 0; i < 6; ++i )
        m_mat[i] = 0;
      for ( int i = 0; i < 3; ++i ) {
        m_rhs[i] = 0;
        m_sol[i] = 0;
      }
    }    
    

    __device__ void addPoint( double wgrad0, double wgrad1, double wgrad2, double wmeas) {
      
      m_mat[0] += wgrad0 * wgrad0;
      m_mat[1] += wgrad0 * wgrad1;
      m_mat[2] += wgrad1 * wgrad1;
      m_mat[3] += wgrad0 * wgrad2;
      m_mat[4] += wgrad1 * wgrad2;
      m_mat[5] += wgrad2 * wgrad2;
      m_rhs[0] += wmeas * wgrad0;
      m_rhs[1] += wmeas * wgrad1;
      m_rhs[2] += wmeas * wgrad2;      

    }
    /// return false if matrix is singular
    __device__ bool solve() {

      // LDL(t) decomposition
      double d[3];
      double l[3];
      if ( m_mat[0] == 0 ) return false; // not correct but this should not happen
      d[0] = m_mat[0];
      l[0] = m_mat[1] / d[0];
      d[1] = m_mat[2] - l[0]*l[0]*d[0];
      if ( d[1] == 0 ) return false;
      l[1] = m_mat[3] / d[0];
      l[2] = (m_mat[4] - l[1]*d[0]*l[0]) / d[1];
      d[2] = m_mat[5] - (l[1]*l[1]*d[0] + l[2]*l[2]*d[1]);

      // solve Lz = b
      double z[3];
      z[0] = m_rhs[0];
      z[1] = m_rhs[1] - l[0]*m_rhs[0];
      z[2] = m_rhs[2] - l[1]*z[0] - l[2]*z[1];

      // solve L(t)x = y = D^(-1)z
      m_sol[2] = z[2]/d[2];
      m_sol[1] = z[1]/d[1] - l[2]*m_sol[2];
      m_sol[0] = z[0]/d[0] - l[0]*m_sol[1] - l[1]*m_sol[2];

      return true;
    }

    __device__ double param0() const { return m_sol[0]; }
    __device__ double param1() const { return m_sol[1]; }
    __device__ double param2() const { return m_sol[2]; }

  private:
    // Mx = b
    double m_mat[6]; /// matrix M
    double m_rhs[3]; /// (right hand side) vector b
    double m_sol[3]; /// (solution) vector x
};

// 5 parameters
class FitCubic {

  #define m_mat(row,col)   m_mat[6*row+col]
  
  public:
    /// Standard constructor
    __device__ FitCubic( ) {
      reset();
    }

    __device__ ~FitCubic( ) {}; ///< Destructor
    
    __device__ void reset() {
      for ( int i = 0; i < 30; ++i )
        m_mat[i] = 0;
    }

    __device__ void addPoint( double wgrads[5], double wmeas) {
      
      for ( int r=0; r < 5; ++r )
        for ( int c=0; c < 5; ++c )
          m_mat(r,c) += wgrads[r]*wgrads[c];   
      
      for ( int r=0; r < 5; ++r )
        m_mat(r,5) += wmeas * wgrads[r]; 
    }
    
    /// return false if matrix is singular
    __device__ bool solve() {

      // Pivoting strategy
      for (int r = 0; r < 5; ++r) {

        // look for pivot
        double val = abs(m_mat(r,r));
        int idx = r;
        for ( int k = r+1; k < 5; ++k ) 
          if ( abs(m_mat(r, k)) > val ) {
            val = abs(m_mat(r, k));
            idx = k;
          }

        if ( val == 0 ) return false;
        
        // Swap current row with pivot
        if ( idx != r ) 
          for ( int c = r; c < 6; ++c ) { 
            double tmp = m_mat(r,c);
            m_mat(r, c) = m_mat(idx, c);
            m_mat(idx, c) = tmp;
          }    

        // scale row
        double rec = 1.0/m_mat(r,r);
        for (int c = r+1; c < 6; ++c) {
          m_mat(r,c) *= rec;
        } 

        // eliminate above and below current row   
        for ( int k = 0; k < 5; ++k ) 
          if ( k != r ) 
            for ( int j = r+1; j < 6; ++j)  
              m_mat(k,j) -= m_mat(k,r)*m_mat(r,j);   
      }

    return true;
  }

  // returns solution parameters
  __device__ double sol( int i ) const { return m_mat(i, 5); }

  private:
    double m_mat[30];
};

// 2 parameters
class FitLine {
  public:
    /// Standard constructor
    __device__ FitLine( ) { 
      for ( int i = 0; i < 3; ++i )
        m_mat[i] = 0;
      for ( int i = 0; i < 2; ++i ) {
        m_rhs[i] = 0;
        m_sol[i] = 0;
      }  
    }

    __device__ ~FitLine( ) {}; ///< Destructor

    __device__ void addPoint( double wgrad0, double wgrad1, double wmeas ) {
      m_mat[0] += wgrad0 * wgrad0;
      m_mat[1] += wgrad0 * wgrad1;
      m_mat[2] += wgrad1 * wgrad1;
      m_rhs[0] += wmeas * wgrad0;
      m_rhs[1] += wmeas * wgrad1;    
    }

    /// return false if matrix is singular
    __device__ bool solve() {

      // det = 0 => singular
      double det = m_mat[0]*m_mat[2] - m_mat[1]*m_mat[1];
      if ( det == 0 ) return false;

      m_sol[1] = ( m_mat[0]*m_rhs[1] - m_mat[1]*m_rhs[0] ) / det;
      if ( m_mat[0] != 0 ) 
        m_sol[0] = ( m_rhs[0] - m_mat[1] * m_sol[1] ) / m_mat[0];
      else
        m_sol[0] = ( m_rhs[1] - m_mat[2] * m_sol[1] ) / m_mat[1];
      return true;

    }
    
    __device__ double param0() const { return m_sol[0]; }
    __device__ double param1() const { return m_sol[1]; }

  protected:

  private:
    double m_mat[3]; /// matrix M in Mx = b
    double m_rhs[2]; /// vector b in Mx = b
    double m_sol[2]; /// (solution) vector x in Mx = b
};

                               



#endif

