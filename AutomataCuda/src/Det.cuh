#ifndef DET_CUH
#define DET_CUH 1

#include "AutomataDef.cuh" // Include constants

//==========================
// Detector geometry
//==========================
struct Detector
{
  // Stereo angle
  float dxdy;    //rad

  // Magnet pos.
  float zMagnet; //mm

  // Layers pos.
  float zLay0[NUM_STA][NUM_REG]; //mm
  float zLay1[NUM_STA][NUM_REG];
  float zLay2[NUM_STA][NUM_REG];
  float zLay3[NUM_STA][NUM_REG];

  // Set values
  __host__ void set() 
  {
    dxdy = +0.087489; // +dxdy (u-lay), -dxdy (v-lay)
    zMagnet = 5383.17;
  
    zLay0[0][0] = 7856.44;
    zLay1[0][0] = 7910.96;
    zLay2[0][0] = 7976.46;
    zLay3[0][0] = 8030.94;

    zLay0[0][1] = 7865.16;
    zLay1[0][1] = 7919.64;
    zLay2[0][1] = 7985.14;
    zLay3[0][1] = 8039.66;

    zLay0[0][2] = 7670.24;
    zLay1[0][2] = 7686.56;
    zLay2[0][2] = 7703.44;
    zLay3[0][2] = 7719.71;

    zLay0[0][3] = 7670.29;
    zLay1[0][3] = 7686.56;
    zLay2[0][3] = 7703.44;
    zLay3[0][3] = 7719.76;

    zLay0[0][4] = 7752.79;
    zLay1[0][4] = 7768.99;
    zLay2[0][4] = 7785.92;
    zLay3[0][4] = 7802.15;

    zLay0[0][5] = 7753.84;
    zLay1[0][5] = 7770.07;
    zLay2[0][5] = 7787;
    zLay3[0][5] = 7803.2;

    zLay0[1][0] = 8538.45;
    zLay1[1][0] = 8592.96;
    zLay2[1][0] = 8658.46;
    zLay3[1][0] = 8712.95;

    zLay0[1][1] = 8547.16;
    zLay1[1][1] = 8601.65;
    zLay2[1][1] = 8667.15;
    zLay3[1][1] = 8721.67;

    zLay0[1][2] = 8352.24;
    zLay1[1][2] = 8368.56;
    zLay2[1][2] = 8385.44;
    zLay3[1][2] = 8401.71;

    zLay0[1][3] = 8352.29;
    zLay1[1][3] = 8368.56;
    zLay2[1][3] = 8385.44;
    zLay3[1][3] = 8401.76;

    zLay0[1][4] = 8434.76;
    zLay1[1][4] = 8450.96;
    zLay2[1][4] = 8467.87;
    zLay3[1][4] = 8484.13;

    zLay0[1][5] = 8435.87;
    zLay1[1][5] = 8452.09;
    zLay2[1][5] = 8469.03;
    zLay3[1][5] = 8485.23;

    zLay0[2][0] = 9223.44;
    zLay1[2][0] = 9277.95;
    zLay2[2][0] = 9343.46;
    zLay3[2][0] = 9397.94;

    zLay0[2][1] = 9232.18;
    zLay1[2][1] = 9286.66;
    zLay2[2][1] = 9352.17;
    zLay3[2][1] = 9406.68;

    zLay0[2][2] = 9037.24;
    zLay1[2][2] = 9053.56;
    zLay2[2][2] = 9070.44;
    zLay3[2][2] = 9086.71;

    zLay0[2][3] = 9037.29;
    zLay1[2][3] = 9053.56;
    zLay2[2][3] = 9070.44;
    zLay3[2][3] = 9086.76;

    zLay0[2][4] = 9119.74;
    zLay1[2][4] = 9135.94;
    zLay2[2][4] = 9152.87;
    zLay3[2][4] = 9169.1;

    zLay0[2][5] = 9120.89;  
    zLay1[2][5] = 9137.12;
    zLay2[2][5] = 9154.02;
    zLay3[2][5] = 9170.28;
  }
};
#endif
