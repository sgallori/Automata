#ifndef PATFWDHITS_CONTAINER_CUH
#define PATFWDHITS_CONTAINER_CUH 1

#include "AutomataDef.cuh" // Include constants

class FwdHit {
  private:
    __device__ unsigned set(bool b, unsigned mask, unsigned val) { 
      return ( val & ~mask ) | ( unsigned(-b) & mask ) ; 
    }
    __device__ void set(bool b, unsigned mask) { 
      m_flags = ( m_flags & ~mask ) | ( unsigned(-b) & mask ) ; 
    }

  public:

    // Default constructor
    __device__ FwdHit() :
      m_pos(-1),
      m_driftDistance(0),
      m_x(0),
      m_z(0),
      m_weight(0),
      m_projection(99999),
      m_flags(0u)
    {};
    
    __device__ FwdHit( int idx, float dd, float x, float z, float w, int s, int l, int r ) :
      m_driftDistance(dd),
      m_x(x),
      m_z(z),
      m_weight(w),
      m_projection(99999)
     { m_flags = 4 * s + l; m_pos = 100*idx+10*l+r; }  
    
    __device__ FwdHit& operator=(const FwdHit& v) {
      this->m_pos = v.m_pos;
      this->m_x = v.m_x;
      this->m_z = v.m_z;
      this->m_weight = v.m_weight;
      this->m_driftDistance = v.m_driftDistance;
      this->m_projection = v.m_projection;
      this->m_flags = v.m_flags;
      
      return *this;
    }

    /// Desctructor
    __device__ ~FwdHit() { }

    __device__ void init(int idx, float dd, float x, float z, float w, int s, int l, int r ) {
      m_driftDistance = dd;
      m_x = x; 
      m_z = z;
      m_weight = w;
      m_flags = 4 * s + l;
      m_pos = 100*idx+10*l+r;
    }  

    // Accessors
    __device__ int   index()          const { return m_pos/100; }
    __device__ float driftDistance()  const { return m_driftDistance; }
    __device__ float x()              const { return m_x; }
    __device__ float z()              const { return m_z; }
    __device__ float weight()         const { return m_weight; }
    __device__ float projection()     const { return m_projection; }
    __device__ int region()           const { return m_pos%10; }
    __device__ int layer()            const { return (m_pos/10)%10; }

    __device__ int  rlAmb()           const { return ( m_flags & 0x4000u ) ? -1  :
                                        ( m_flags & 0x2000u ) ? +1  : 0 ; }
    __device__ bool hasPrevious()     const { return   m_flags & 0x1000u; }
    __device__ bool hasNext()         const { return   m_flags & 0x0800u; }
    __device__ bool isIgnored()       const { return   m_flags & 0x0400u; }
    __device__ bool isUsed()          const { return   m_flags & 0x0200u; }
    __device__ int  planeCode()       const { return   m_flags & 0x00ffu; }

    // Setters
    __device__ void setIndex( int idx )                        { m_pos = 100*idx + m_pos%100; }  
    __device__ void setDriftDistance( float driftDistance )    { m_driftDistance = driftDistance; }
    __device__ void setX( float x )                            { m_x = x; }
    __device__ void setZ( float z )                            { m_z = z; }
    __device__ void setWeight( float w )                       { m_weight = w; }  
    __device__ void setProjection( float proj )                { m_projection = proj; }

    __device__ void setRlAmb( int rl )                       { m_flags = set( rl != 0, 0x2000u,
                                                                set( rl < 0, 0x4000u, m_flags)); }
    __device__ void setHasPrevious( bool hasPrevious )       { set(hasPrevious, 0x1000u ); }
    __device__ void setHasNext( bool hasNext )               { set(hasNext,     0x0800u ); }
    __device__ void setIgnored( bool isIgnored )             { set(isIgnored,   0x0400u ); }
    __device__ void setIsUsed(bool isUsed)                   { set(isUsed,      0x0200u ); }

    __device__ bool isOT() const { return ( region() < 2 ); }
    __device__ bool isX() const { return ( layer() == 0 || layer() == 3 ); }
    
    __device__ float dxDy( ) const { 
      return ( layer() == 1 ) ? c_dxDy : ( layer() == 2 ) ? -c_dxDy : 0; 
    }

    __device__ float dzDy( ) const { return c_dzDy; }

    __device__ float sinT() const { 
      return ( layer() == 1 ) ? -c_sinT : ( layer() == 2 ) ? c_sinT : 0; 
    }

    __device__ float cosT() const { return sqrt(1 - sinT() * sinT()); }
    

    __device__ void debug(int event, int vt) {
      printf("[%d, %d] DEBUG  [%d] Z %f Xp %f X %f W %f L %d R %d Prev %d Next %d Drift %f isused %d\n",
            event, vt, index(), m_z, m_projection, m_x, m_weight, layer(), region(), hasPrevious(), 
            hasNext(), m_driftDistance, isUsed());  
    }

  private:
    // m_pos is 100*idx + 10 * lay + reg (idx refers to PatFwdHitsContainer)
    int    m_pos;
    float  m_driftDistance; 
    float  m_x; 
    float  m_z; 
    float  m_projection;   
    float  m_weight;
    // m_flag contains: RlAmb, HasPrev and HasNext, 
    // Ignored, IsUsed and planeCode
    unsigned m_flags;

  
};

//========================================
// SoA container for PatFwdHits 
//========================================
class PatFwdHitsContainer
{  
  private:
    // given global index g returns (s, l, r, i) coords
    __device__ void getCoords(int g, int &s, int &l, int &r, int &i) {
      i = g % MAX_HITS_PER_REG;
      int tmp = g / MAX_HITS_PER_REG ;
      r = tmp % NUM_REG;
      tmp /= NUM_REG;
      l = tmp % NUM_LAY;
      tmp /= NUM_LAY;
      s = tmp % NUM_STA;
      return;
    }


  public:
    // Constructors 
    __host__ __device__ PatFwdHitsContainer() {}

    // Destructors 
    __host__ __device__ ~PatFwdHitsContainer() {}

    // returns offset
    __host__ __device__ int offset( int sta, int lay, int reg) {
      return MAX_HITS_PER_REG * ( (sta * NUM_LAY * NUM_REG) + (lay * NUM_REG) + reg);
    };

    // do we need an atomic add?
    __device__ void addHit( int idx, int lhcbid, int cl, 
                            float x, float z, float w, 
                            float dd, float yb, float ye ) {
      m_lhcbIDs[idx] = lhcbid;
      m_clusters[idx] = cl; 
      m_xs[idx] = x;                    
      m_zs[idx] = z;                    
      m_weights[idx] = w;               
      m_driftDistances[idx] = dd;        
      m_yBegin[idx] = yb;
      m_yEnd[idx] = ye;         
      m_isUsed[idx] = 0;
    }

    // Returns chi2 of hit
    __device__ float chi2Hit( int idx, float tx, float x0, float z0 )
    {
      float xHit = m_xs[idx];
      float zHit = m_zs[idx];
      float driftDist = m_driftDistances[idx];
      float weight = m_weights[idx];

      float dist = xHit - x0 - tx * (zHit - z0);
      //if (hit->hit()->type() == Tf::RegionID::IT) return dist; //TODO: implement for IT... 
      dist *= (1. / sqrt(1. + tx * tx)); // cosine
      float dx = driftDist;
      if (fabs( dist - dx ) < fabs( dist + dx ))
        dist -= dx;
      else
        dist += dx;
  
      dist *= weight;

      return dist * dist;
    }

    // cluster utility
    // returns the number of hits that forms the cluster (0, 1, or 2)
    __host__ __device__ int isCluster( int idx ) {
      return m_clusters[idx];
    }
    __host__ __device__ int isCluster( int sta, int lay, int reg, int idx ) {
      return isCluster(offset(sta, lay, reg) + idx);
    }    
    __device__ float xCluster( int idx ) {
      if ( !m_clusters[idx] ) return 9999;
      if ( m_clusters[idx] == 1 ) return m_xs[idx];
      return 0.5 * ( m_xs[idx] + m_xs[idx+1] );
    }
    __device__ float zCluster( int idx ) {
      if ( !m_clusters[idx] ) return 9999;
      if ( m_clusters[idx] == 1 ) return m_zs[idx];
      return 0.5 * ( m_zs[idx] + m_zs[idx+1] );
    }  
   
    // getters
    __device__ int getNHits( int sta, int lay, int reg) { return m_nHits[sta][lay][reg]; }
    __device__ float getX( int idx ) { return m_xs[idx]; }
    __device__ float getZ( int idx ) { return m_zs[idx]; }
    __device__ float getWeight( int idx ) { return m_weights[idx]; }
    __device__ float getDriftDist( int idx ) { return m_driftDistances[idx]; }
    __device__ int getLhcbId( int idx ) { return m_lhcbIDs[idx]; }
    __device__ int isUsed( int idx ) { return m_isUsed[idx]; }
    
    __device__ float getYMin( int sta, int lay, int reg ) { return m_yMin[sta][lay][reg]; }
    __device__ float getYMax( int sta, int lay, int reg ) { return m_yMax[sta][lay][reg]; }   
    __device__ float zRegion( int sta, int lay, int reg ) {
      return 0.5 * ( m_zMin[sta][lay][reg] + m_zMax[sta][lay][reg] );
    }
    __device__ FwdHit getHit( int index ) {
      int sta, lay, reg, idx;
      getCoords(index, sta, lay, reg, idx);
      if ( idx < m_nHits[sta][lay][reg])
        return FwdHit(index, m_driftDistances[index], m_xs[index], m_zs[index], m_weights[index], sta, lay, reg);
      else { 
        printf("ALLERT! Hit %d (%d) on (%d, %d, %d) not exist! Max hit is %d\n", idx, index, sta, lay, reg,  m_nHits[sta][lay][reg]);
        return FwdHit();
      }  
    }
    
    __device__ void setNHits( int sta, int lay, int reg, int nhits ) {
      m_nHits[sta][lay][reg] = nhits;
    }
   
    // set how many times an hit is used by valid tracks 
    __device__ void setUsed( int idx, bool val ) { 
      if ( val ) atomicAdd(&m_isUsed[idx], 1);
      else atomicSub(&m_isUsed[idx], 1);
    } 
    
    // regions
    __device__ void setYRegion( int sta, int lay, int reg, float ymin, float ymax ){
      m_yMin[sta][lay][reg] = ymin;  
      m_yMax[sta][lay][reg] = ymax;
      return;
    }
    __device__ void setZRegion( int sta, int lay, int reg, float zmin, float zmax ){
      m_zMin[sta][lay][reg] = zmin;  
      m_zMax[sta][lay][reg] = zmax;
      return;
    }    
    

  private:
    // Data members
    // nHits it's just the difference bet. two adjacent offsets
    int m_nHits[NUM_STA][NUM_LAY][NUM_REG];   // Number of hits in (sta, lay, reg)
    int m_lhcbIDs[MAX_HITS];                  // LHCbIDs
    int m_clusters[MAX_HITS];                 // # of consecutive neighbours (plus itself) that form a cluster
    float m_xs[MAX_HITS];                     // x-pos. @y=0.    
    float m_zs[MAX_HITS];                     // z-pos. @y=0.
    float m_weights[MAX_HITS];                // weights (1./err)
    float m_driftDistances[MAX_HITS];         // drift distances (OT only!)
    float m_yBegin[MAX_HITS];
    float m_yEnd[MAX_HITS];
    int  m_isUsed[MAX_HITS];
    
    // region parameters
    float m_yMin[NUM_STA][NUM_LAY][NUM_REG]; 
    float m_yMax[NUM_STA][NUM_LAY][NUM_REG]; 
    float m_zMin[NUM_STA][NUM_LAY][NUM_REG]; 
    float m_zMax[NUM_STA][NUM_LAY][NUM_REG];
};


//=============================================================================
// Planes Counter class
//=============================================================================

class FwdCounter {
  public:

    /// Standard constructor
    
    __device__ FwdCounter(FwdHit* fhits, int start, int end ) : m_nbDifferent(0) {
      for (int i = 0; i < PLANES; ++i) 
        m_planeList[i] = 0;
        
      for (int i = start; i < end; ++i) 
        if ( fhits[i].isUsed() ) 
          if ( 0 == m_planeList[ fhits[i].planeCode() ]++ ) ++m_nbDifferent;
    }

    /// add a hit, returns # of different planes
    __device__ int addHit( const FwdHit& fhit ) {
      if ( !fhit.isUsed() ) return m_nbDifferent;
      if ( 0 == m_planeList[ fhit.planeCode() ]++ ) ++m_nbDifferent;
      return m_nbDifferent;
    }
    /// remove a hit, returns # of different planes
     __device__ int removeHit( const FwdHit& fhit ) {
      if ( !fhit.isUsed() ) return m_nbDifferent;
      if ( 0 == --m_planeList[ fhit.planeCode() ] ) --m_nbDifferent;
      return m_nbDifferent;
    } 

    /// returns number of different planes
    __device__ int nbDifferent() const { return m_nbDifferent; }

    /// returns number of hits in specified plane
    __device__ int nbInPlane( int plane ) const { return m_planeList[plane]; }

    /// returns number of stereo planes with hits
    __device__ int nbStereo() const {
      int nb = 0;
      if ( m_planeList[ 1] !=0 ) ++nb;
      if ( m_planeList[ 2] !=0 ) ++nb;
      if ( m_planeList[ 5] !=0 ) ++nb;
      if ( m_planeList[ 6] !=0 ) ++nb;
      if ( m_planeList[ 9] !=0 ) ++nb;
      if ( m_planeList[10] !=0 ) ++nb;
      return nb;
    }

    /// returns the number of plane with most hits
    /// -1 if there aren't any hits.
    __device__ int maxPlane () const {
      int max = 0;
      int imax = -1;
      for (int i = 0; i < PLANES; ++i ) {
        if ( m_planeList[i] > max ) { 
          max = m_planeList[i];
          imax = i;
        }
      }    
      return imax;
    }
    
  private:

    int m_planeList[PLANES];
    int m_nbDifferent;

};





#endif
