#ifndef TRACKLETS_CONTAINER_CUH
#define TRACKLETS_CONTAINER_CUH 1

#include <stdio.h> // C++
#include "AutomataDef.cuh" // Includes constants
#include "PatFwdHitsContainer.cuh" // Containers
#include "AutomataTool.cuh"

template < typename T >
__device__ void swap (T& x, T& y) {
  T temp = x;
  x = y;
  y = temp;
  return;
};

//========================================
// SoA container of x-tracklets
//========================================

class Tracklet {
  public:
    __host__ __device__ Tracklet() { 
      for (int i = 0; i < 4; ++i)
        m_cluster[i] = -1;
      m_ty = 0;
      m_tx = 0;
      m_x0 = 0;
      m_y0 = 0;
      m_z0 = 0;
      m_chi2 = 9999;
    }
    // Xtracklet
    __device__ Tracklet( int cl0, int cl3 ) {
      m_cluster[0] = cl0;
      m_cluster[1] = -1;
      m_cluster[2] = -1;
      m_cluster[3] = cl3;
      m_ty = 0;
      m_tx = 0;
      m_x0 = 0;
      m_y0 = 0;
      m_z0 = 0;
      m_chi2 = 9999;
    }
    // Stub
    __host__ __device__ Tracklet( int cl0, int clU, int clV, int cl3) {
      m_cluster[0] = cl0;
      m_cluster[1] = clU;
      m_cluster[2] = clV;
      m_cluster[3] = cl3;
      m_ty = 0;
      m_tx = 0;
      m_x0 = 0;
      m_y0 = 0;
      m_z0 = 0;
      m_chi2 = 9999;
    }
  
    __host__ __device__ void setChi2( float c2 ) {
      m_chi2 = c2;
    }
  
    // Xtracklet
    __device__ void setParam( float x, float z, float tx ) {  
      m_tx = tx;
      m_x0 = x;
      m_z0 = z; 
      return;
    }
    // Stub
    __host__ __device__ void setParam( float x, float y, float z, float tx, float ty ) {  
      m_tx = tx;
      m_ty = ty;
      m_x0 = x;
      m_y0 = y;
      m_z0 = z; 
      return;
    }    

    __device__ double xAtZ(double z) const { return m_x0 + m_tx * (z - m_z0); } // straight line
    __device__ double yAtZ(double z) const { return m_y0 + m_ty * (z - m_z0); } // straight line
    // Tracklet angle in X-Z plane
    __device__ double cosXZ() const { return 1.0 / sqrt(1.0 + m_tx * m_tx); }
    
    // getters
    __device__ int getCl( int idx ) { if (idx < 4 ) return m_cluster[idx]; else return -1; }
    __host__ __device__ float getTx()   { return m_tx;   } 
    __host__ __device__ float getTy()   { return m_ty;   }
    __device__ float getX()    { return m_x0;   }
    __device__ float getY()    { return m_y0;   }
    __device__ float getZ()    { return m_z0;   }  
    __host__ __device__ float getChi2() { return m_chi2; }
    
    __host__ __device__ void debug() {
      printf("--DEBUG--> Track: coords ( %f %f %f ) slopes ( %f %f ) clusters idx ( %d %d %d %d ) chi2 %g\n", 
             m_x0, m_y0, m_z0, m_tx, m_ty, m_cluster[0], m_cluster[1], m_cluster[2], m_cluster[3], m_chi2);
    }

  private:
    int   m_cluster[4];     // Cluster index 
    float m_tx;             // x-slope
    float m_ty;             // y-slope
    float m_x0;             // x-pos. @zmid (z1+z2)/2.
    float m_y0;             // y-pos. @zmid (z1+z2)/2.
    float m_z0;             // z-pos. @zmid
    float m_chi2;
  
};

class XTrackletsContainer
{
  private:
    // given global index g returns (s, l, r, i) coords
    __host__ __device__ void getCoords(int g, int &s, int &r, int &i) {
      i = g % MAX_XTRACK_PER_REG;
      int tmp = g / MAX_XTRACK_PER_REG;
      r = tmp % 2; // At moment only OT shoule be NUM_REG
      tmp /= 2;
      s = tmp % NUM_STA;
      return;
    }
    // check if an index is well formed
    __host__ __device__ bool isGood(int idx) {
      int sta, reg, tid;
      getCoords(idx, sta, reg, tid);
      return ( tid < m_nXTracklets[sta][reg] );
   }  

  public: 
    // Constructors 
    __host__ __device__ XTrackletsContainer() {}

    // Destructors 
    __host__ __device__ ~XTrackletsContainer() {}

    __device__ void clean( int sta, int reg) {
      m_nXTracklets[sta][reg] = 0;
    }

    // Returns offset for (sta, reg) 
    __host__ __device__ int offset( int sta, int reg ) {
      return ( sta * MAX_XTRACK_PER_STA + reg * MAX_XTRACK_PER_REG ); 
    }
   
    // AtomicAdd a tracklet in (sta, reg)
    __device__ void atomicaddTracklet( int sta, int reg, Tracklet tck ) { 
      if (m_nXTracklets[sta][reg] >= MAX_XTRACK_PER_REG) return;
      int iold = atomicAdd( &(m_nXTracklets[sta][reg]), 1 ); 
      if (iold >= MAX_XTRACK_PER_REG) {
        printf("Overflow detected in XTrackletsContainer (size = %i) \n", MAX_XTRACK_PER_REG);
        m_nXTracklets[sta][reg] = MAX_XTRACK_PER_REG; // reset counter
        return;
      } else {

        int idx = offset(sta, reg) + iold;

        m_idx0s[idx] = tck.getCl(0);
        m_idxUs[idx] = tck.getCl(1);
        m_idxVs[idx] = tck.getCl(2);
        m_idx3s[idx] = tck.getCl(3);
        m_txs[idx]   = tck.getTx();
        m_tys[idx]   = tck.getTy();
        m_x0s[idx]   = tck.getX();
        m_y0s[idx]   = tck.getY();
        m_z0s[idx]   = tck.getZ();
        m_chi2[idx]  = tck.getChi2();
      
        m_isUsed[idx] = false;
      
        for (int i=0; i < maxNB; ++i)
          m_neighbours[idx][i] = -1;
      
        return;
      }       
    }
    
    __device__ float getTx( int idx ) { return m_txs[idx]; }
    __device__ float getTy( int idx ) { return m_tys[idx]; }
    __device__ float getX0( int idx ) { return m_x0s[idx]; }
    __device__ float getY0( int idx ) { return m_y0s[idx]; }
    __device__ float getZ0( int idx ) { return m_z0s[idx]; }
    __device__ float getChi2( int idx ) { return m_chi2[idx]; }
    __device__ bool isUsed( int idx ) { return m_isUsed[idx]; }
    __device__ int getNB( int tk, int idx ) { if ( isGood(tk) ) return m_neighbours[tk][idx]; else return -1; }
    __host__ __device__ int getnNB( int tk ) {
      int res = 0;
      while ( ( res < maxNB ) && ( m_neighbours[tk][res] != -1 ) ) res++;
      return res;
      
    }
    
    __device__ void setUsed( int idx, bool val ) { if ( isGood(idx) ) m_isUsed[idx] = val; }
    
    __device__ void setNB( int idx, int idNB, int val ) { 
      if ( isGood(idx) && isGood(val) && idNB < maxNB ) m_neighbours[idx][idNB] = val; 
    }
   
    __host__ __device__ Tracklet getTracklet( int sta, int reg, int idx ) {
      return getTracklet( offset(sta, reg) + idx );  
    }

    __host__ __device__ Tracklet getTracklet( int idx ) {
      if ( isGood(idx) ) {
        Tracklet track(m_idx0s[idx], m_idxUs[idx], m_idxVs[idx], m_idx3s[idx]);
        track.setParam(m_x0s[idx], m_y0s[idx], m_z0s[idx], m_txs[idx], m_tys[idx]);
        track.setChi2(m_chi2[idx]);
        return track;
      } else 
        return Tracklet();
    }

    __device__ void resetCounter( int sta, int reg, int count) { m_nXTracklets[sta][reg] = count; }
    __host__ __device__ int getNumTk( int sta, int reg ) { return m_nXTracklets[sta][reg]; }

    // WARN. It works only inside the same region and we cannot maintain neighbours references
    __device__ void swaptk( int l, int r ) {
      swap(m_idx0s[l], m_idx0s[r]);
      swap(m_idxUs[l], m_idxUs[r]);
      swap(m_idxVs[l], m_idxVs[r]);
      swap(m_idx3s[l], m_idx3s[r]);
      swap(m_txs[l], m_txs[r]);
      swap(m_tys[l], m_tys[r]);
      swap(m_x0s[l], m_x0s[r]);
      swap(m_y0s[l], m_y0s[r]);
      swap(m_z0s[l], m_z0s[r]);
      swap(m_chi2[l], m_chi2[r]);
      swap(m_isUsed[l], m_isUsed[r]);
    }
    
  private:
    // Data members
    int m_nXTracklets[NUM_STA][NUM_REG]; // Number of x-tracklets in (sta, reg)
    
    bool m_isUsed[MAX_XTRACK];           // used flags
    
    int m_neighbours[MAX_XTRACK][maxNB]; // indexes of neighbours  
    
    int m_idx0s[MAX_XTRACK];             // Cluster index in lay0
    int m_idxUs[MAX_XTRACK];             // Cluster index in layU
    int m_idxVs[MAX_XTRACK];             // Cluster index in layV    
    int m_idx3s[MAX_XTRACK];             // Cluster index in lay3
    
    float m_txs[MAX_XTRACK];             // x-slope
    float m_tys[MAX_XTRACK];             // y-slope
    float m_x0s[MAX_XTRACK];             // x-pos. @zmid (z1+z2)/2.
    float m_y0s[MAX_XTRACK];             // y-pos. @zmid (z1+z2)/2.
    float m_z0s[MAX_XTRACK];             // z-pos. @zmid
    float m_chi2[MAX_XTRACK];            // chi2
     
};

//========================================
// Forward track. Basically the PatSeedTrack
//========================================

class FwdTrack {
  
  public:
    /// Standard constructor

    __device__ FwdTrack( ) :
      m_numHits(0),
      m_z0(0.0),
      m_ax(0.0), m_bx(0.0), m_cx(0.0), m_dx(0.0), 
      m_ay(0.0), m_by(0.0),
      m_cosine(1.0),
      m_chi2PerDoF(99999.0)
     { }
     
    __device__ FwdTrack( Tracklet tck, PatFwdHitsContainer* hits, double zRef, double dRatio, double arrow ) :
      m_dx(dRatio), m_ay(0.0), m_cosine(1.), 
      m_numHits(0), m_chi2PerDoF(99999.0) { 

      //== x = a + b*dz + c*dz^2 + d*dz^3
      //== d = dRation * c
      //== c = curvature * (x at z=0 )
      //== solve with c0 and c3 only

      // arrow is m_cx * dz^2 for nominal distance between stations.
      const double curvature = -arrow;
      const double dz0 = hits->getZ( tck.getCl(0) ) - zRef;
      const double curv0 = curvature * ( 1. + dRatio * dz0 ) * dz0 * dz0;
      const double a0 =  1. + curv0;
      const double b0 = dz0 - zRef * curv0;
      const double dz3 = hits->getZ( tck.getCl(3) ) - zRef;
      const double curv3 = curvature * ( 1. + dRatio * dz3 ) * dz3 * dz3;
      const double a3 =  1. + curv3;
      const double b3 = dz3 - zRef * curv3;
      const double x0 = hits->getX( tck.getCl(0) );
      const double x3 = hits->getX( tck.getCl(3) );

      m_z0 = zRef;
      m_bx = ( x0 * a3 - a0 * x3 ) / ( b0 * a3 - a0 * b3 );
      m_ax = ( x0 * b3 - b0 * x3 ) / ( a0 * b3 - b0 * a3 );
      m_cx = curvature * ( m_ax - zRef * m_bx ); 
      m_by = ( xAtZ(hits->getZ(tck.getCl(1))) - hits->getX(tck.getCl(1)) - 
                xAtZ(hits->getZ(tck.getCl(2))) + hits->getX(tck.getCl(2)) ) / ( 2 * c_dxDy * m_z0 );

      addTrackletCl(hits, tck);
    
    }  
        
    __device__ FwdTrack(const FwdTrack& f) {
      this->m_z0     = f.m_z0;
      this->m_numHits = f.m_numHits;
      for (int i=0; i < f.m_numHits; ++i)
        this->m_hits[i] = f.m_hits[i];
      this->m_ax     = f.m_ax;
      this->m_bx     = f.m_bx;
      this->m_cx     = f.m_cx;
      this->m_dx     = f.m_dx;
      this->m_ay     = f.m_ay;
      this->m_by     = f.m_by;
      this->m_cosine = f.m_cosine;
      this->m_chi2PerDoF = f.m_chi2PerDoF;
    }

    __device__ FwdTrack& operator=(const FwdTrack& f) {
      this->m_z0     = f.m_z0;
      this->m_numHits = f.m_numHits;
      for (int i=0; i < f.m_numHits; ++i)
        this->m_hits[i] = f.m_hits[i];
      this->m_ax     = f.m_ax;
      this->m_bx     = f.m_bx;
      this->m_cx     = f.m_cx;
      this->m_dx     = f.m_dx;
      this->m_ay     = f.m_ay;
      this->m_by     = f.m_by;
      this->m_cosine = f.m_cosine;
      this->m_chi2PerDoF = f.m_chi2PerDoF;
 
      return *this;
    }

    __device__ ~FwdTrack( ) {}; ///< Destructor

    __device__ int getNumHits()            const { return m_numHits; }
    __device__ FwdHit hit(int idx)               { return m_hits[idx]; }
    __device__ FwdHit* hitptr(int idx)           { return &m_hits[idx]; }
    __device__ FwdHit* hits()                    { return m_hits; }
    
    __device__ void setChi2PerDoF( double chi2 ) { m_chi2PerDoF = chi2; }
    __device__ double chi2PerDoF()         const { return m_chi2PerDoF; }

    __device__ double z0() const { return m_z0; } ///< return reference z
    __device__ double xAtZEqZ0() const { return m_ax; } ///< return x at reference z

    __device__ double xAtZ( double z ) const {
      const double dz = z - m_z0;
      return m_ax + dz * ( m_bx + dz * ( m_cx * (1. + dz * m_dx )));
    }
    ///< return slope in x at given z
    __device__ double xSlope( double z ) const {
      const double dz = z - m_z0;
      return m_bx + dz * ( 2. * m_cx * (1. + 1.5 * dz * m_dx) );
    }
    __device__ double yAtZ( double z ) const { return m_ay + m_by * z; }
    __device__ double ySlope( double ) const { return m_by; }
    __device__ double curvature() const { return m_cx; }
    /// return ratio of cubic/parabolic coefficient
    __device__ double dRatio() const { return m_dx; }
    ///< return cosine of track angle in xz projection
    __device__ double cosine() const { return m_cosine; }
    __device__ void setCosine( double cos ) { m_cosine = cos; }

    // little helper class to set ambiguities in a pair of hits
    template <bool sameSign>
    struct HitPairAmbSetter {
      /// set ambiguity of h1 to amb, h2 according to sameSign
      __device__ static inline void set(FwdHit* h1, FwdHit* h2, int amb)
      { h1->setRlAmb(amb); h2->setRlAmb(sameSign ? amb : -amb); }
    };

    // Resolve left-right ambiguities using two hits in different monolayers
    __device__ void resolveAmb(PatFwdHitsContainer* hits, FwdHit *h1, FwdHit *h2)  {
      // have two neighbouring hits, calculate pitch residual
      // work out effective slope in module frame
      double sinT = h1->sinT();
      double cosT = h1->cosT();
      double z0 = ( hits->getZ( h1->index() ) + hits->getZ( h2->index() ) ) / 2.0;
      double txeff = xSlope( z0 ) * cosT + ySlope( z0 ) * sinT;
      // get 2D wire distance vector in module coordinates
      double dx = ( hits->getX( h1->index() ) + hits->getX( h2->index() ) ) * cosT;
      double dzdy = h1->dzDy();
      double dz = ( hits->getZ( h1->index() ) - hits->getZ( h2->index() ) ) /
	                    std::sqrt(1.0 + dzdy * dzdy);
      // calculate effective pitch (i.e. pitch seen by tr)
      double scale = (dx * txeff + dz) / (1.0 + txeff * txeff);
      double eprx = dx - scale * txeff;
      double eprz = dz - scale;
      double epr = std::sqrt(eprx * eprx + eprz * eprz);
      // get radii
      double r1 = h1->driftDistance();
      double r2 = h2->driftDistance();
      // calculate pitch residual (convert back from effective pitch)
      // case 1: track passes between hits, case 2: not case 1
      double pr1 = std::abs(dx / epr) * (epr - r1 - r2);
      double pr2 = std::abs(dx / epr) * (epr - std::abs(r1 - r2));
      if (std::abs(pr1) <= std::abs(pr2)) {
	      // set ambiguities accordingly
	      HitPairAmbSetter<false>::set(h1, h2, (dx > 0) ? -1 : +1);
      } else {
	      // try to figure out ambiguity by comparing slope estimate obtained
	      // from drift radii (assuming hits do not pass in between the wires)
	      // to the (well measured) effective slope
	      //
	      // as the slopes are usually big, we have to correct for the fact that
	      // the radius is no longer in x direction (using the measured txeff)
	      double corr = std::sqrt(1.0 + txeff * txeff);
	      double dslplus = (dx + (r1 - r2) * corr) - txeff * dz;
	      double dslminus = (dx - (r1 - r2) * corr) - txeff * dz;
	      HitPairAmbSetter<true>::set(h1, h2,
	                              (std::abs(dslplus) < std::abs(dslminus)) ? +1 : -1);
      }
    }
    
    __device__ int exist( FwdHit fhit ) {
      return ( fhit.index() >=0 );
    }
      
    __device__ void addTrackletCl( PatFwdHitsContainer* hits, Tracklet tkl ) {
      for (int j=0; j<4; ++j)
        if ( tkl.getCl( j ) != -1 )
          addCluster(hits, tkl.getCl( j ));
    }
      
    __device__ void addCluster( PatFwdHitsContainer* hits, int idx ) {
      addHit( hits->getHit( idx ) );  
      if ( hits->isCluster( idx ) == 2 ) {
        addHit( hits->getHit( idx + 1 ) );
        resolveAmb(hits, &m_hits[m_numHits-2], &m_hits[m_numHits-1]);
      }
      
    }
    
    __device__ void addHit( FwdHit fhit ) { 
      if ( ( exist(fhit) ) && ( m_numHits < FWDHITS ) ) {
        fhit.setIsUsed( true );
        m_hits[m_numHits++] = fhit; 
      }
    }

    // Remove unUseed hits
    __device__ void cleanHits( ) {
      int nValidHits = 0;
      for ( int i = 0; i < m_numHits; ++i) {
        if ( m_hits[i].isUsed( ) ) {
          if ( nValidHits < i )
            m_hits[nValidHits] = m_hits[i];
          nValidHits++;
        }
      }
      m_numHits = nValidHits;
    }
    
    // Setters 
    __device__ void forcenHits( int val ) { m_numHits = val; }
    
    __device__ void updateParameters( double dax, double dbx, double dcx,
                           double day = 0., double dby = 0.  ) {                  
      m_ax += dax;
      m_bx += dbx;
      m_cx += dcx;
      m_ay += day;
      m_by += dby;
      m_z0 += day * m_hits[0].dzDy();
      m_cosine =  1. / std::sqrt( 1. +  m_bx * m_bx  );
      
    }

    __device__ int countUsed() {
      int nbUsed = 0;
      for ( int i = 0; i < m_numHits; ++i)  
        if ( m_hits[i].isUsed() )  nbUsed++;
      return nbUsed;
    }      

  protected:

  private:
    // hits in the track
    unsigned int m_numHits;
    FwdHit m_hits[FWDHITS];
    // track parameters   
    double m_z0;
    // fit parameters
    double m_ax;
    double m_bx;
    double m_cx;
    double m_dx;
    double m_ay;
    double m_by;
    double m_cosine;
    double m_chi2PerDoF;

};

typedef PVector<FwdTrack, MAX_OUTTRACK> CAtracks; 

struct Candidate {

  __device__ Candidate() {};

  __device__ Candidate(const Candidate& f) {
    for ( int i = 0; i < 3; ++i ) 
      this->x[i] = f.x[i];
  }
   __device__ Candidate& operator=(const Candidate& f) {
    for ( int i = 0; i < 3; ++i ) 
      this->x[i] = f.x[i]; 
    return *this;
  }
  
  __device__ ~Candidate( ) {}; ///< Destructor  
  
  int x[3];
};

typedef PVector<Candidate, 12*MAX_XTRACK_PER_STA> Candidates;

struct LhcbOutTrack {
// output tracks
  double x0;
  double y0;
  double tx;
  double ty;
  double curvature;
  // "dof" values
  double chi2xdof;
  // lhcbID hits
  int   HitsNum;
  int   Hits[CANDHITS]; // lhcbID

};

typedef PVector<LhcbOutTrack, MAX_LHCBTK> LhcbOutTracks;




#endif

