#include <iostream>
#include "AutomataKernels.cuh"
#include <assert.h>

/*
WARNING: Assume data packets sorted by increasing (evt, sta, lay, reg)

Event format:
HEADER: idxsta idxlay idxreg #hits
HITS:   lhcbIDs xs zs weights drifstances yBegin yEnd

*/

//=====================================================
// Unpack data buffer and load hits in container
//=====================================================
__global__ void unpack( uint8_t *buffer, PatFwdHitsContainer* chits ) 

{
  const unsigned int evt = (blockIdx.x);  // event 
  const unsigned int sta = (threadIdx.x); // station index: 0, 1, 2
  const unsigned int lay = (threadIdx.y); // layer index: 0, 1, 2, 3
  const unsigned int reg = (threadIdx.z); // region index:0, 1, 2, 3, 4, 5


  //PatFwdHitsContainer hits;
  PatFwdHitsContainer *hits = &chits[evt];
  // Shift to the begin of the event
  const uint8_t *data = buffer + evt * ( HEADER_SIZE + MAX_HITS_PER_REG * HIT_SIZE ) * ( NUM_STA * (NUM_LAY * NUM_REG));
  // Overall index to identify the first hit of every region
  int index = ( HEADER_SIZE + MAX_HITS_PER_REG * HIT_SIZE ) * ( sta * (NUM_LAY * NUM_REG) + lay * (NUM_REG) + reg );

  int nHits = *(int *)(data + index + 3 * sizeof(uint32_t));

  hits->setNHits(sta, lay, reg, nHits);

  // Inside the given region we identify the 5 quantities, skipping the header containing Sta, Lay, Reg and nHits
  int   * ids            = (int *)  (data + index + HEADER_SIZE );
  float * xs             = (float *)(data + index + HEADER_SIZE  +     MAX_HITS_PER_REG * sizeof(float));
  float * zs             = (float *)(data + index + HEADER_SIZE  + 2 * MAX_HITS_PER_REG * sizeof(float));
  float * weights        = (float *)(data + index + HEADER_SIZE  + 3 * MAX_HITS_PER_REG * sizeof(float));
  float * driftDistances = (float *)(data + index + HEADER_SIZE  + 4 * MAX_HITS_PER_REG * sizeof(float));
  float * yBegin         = (float *)(data + index + HEADER_SIZE  + 5 * MAX_HITS_PER_REG * sizeof(float));
  float * yEnd           = (float *)(data + index + HEADER_SIZE  + 6 * MAX_HITS_PER_REG * sizeof(float));
  
  //Clusterization
  int i;
  int cluster = 0;
  float ymin = 9999;
  float zmin = 9999;
  float ymax = -9999;
  float zmax = -9999;
  int off = hits->offset(sta, lay, reg);
  for (i = 0; i < nHits - 1; ++i) {
    ymin = min(ymin, min(yBegin[i], yEnd[i]));
    ymax = max(ymax, max(yBegin[i], yEnd[i]));
    zmin = min(zmin, zs[i]);
    zmax = max(zmax, zs[i]);
    if ( cluster == 2 ) { // not overlap clusters
      cluster = 0; 
    } else {
      if ( abs( xs[i] - xs[i+1] ) < 3.0 ) {
        cluster = 2;
      } else {
        cluster = 1;
      }
    }
    hits->addHit( off+i, ids[i], cluster, xs[i], zs[i], weights[i], driftDistances[i], yBegin[i], yEnd[i]);
  }

  // last hit
  if (nHits > 0) {
    ymin = min(ymin, min(yBegin[i], yEnd[i]));
    ymax = max(ymax, max(yBegin[i], yEnd[i]));
    zmin = min(zmin, zs[i]);
    zmax = max(zmax, zs[i]);    
    if ( cluster == 2 ) 
      cluster = 0;
    else
      cluster = 1;    
    hits->addHit( off+i, ids[i], cluster, xs[i], zs[i], weights[i], driftDistances[i], yBegin[i], yEnd[i]);
    hits->setYRegion(sta, lay, reg, ymin, ymax);
    hits->setZRegion(sta, lay, reg, zmin, zmax);
  }

}
