#include "Utility.cuh"

//==================================
// Wakeup device before running 
//==================================
__global__ void WakeUp() 
{
  //printf("GPU says: 'Bonjour à tout le monde!'\n");
}

//==================================
// Print GPU memory usage 
//==================================
__host__ void printMemUsage() 
{
  size_t free, total;
  cudaMemGetInfo(&free, &total);
  size_t bytes = total - free; // bytes

  std::cout << "INFO: ===================" << std::endl;
  std::cout << "INFO:  Memory usage info " << std::endl;
  std::cout << "INFO: ===================" << std::endl;
  std::cout << "INFO: PatFwdHitsContainer memory = " 
  << sizeof(PatFwdHitsContainer) * MB << " MB" << std::endl;  
  std::cout << "INFO: XTrackletsContainer memory = " 
  << sizeof(XTrackletsContainer) * MB << " MB" << std::endl;  
  std::cout << "INFO: CAtracks memory = " 
  << sizeof(CAtracks) * MB << " MB" << std::endl;  
  
  std::cout << "INFO: Total allocated memory (cudaMemGetInfo) = " 
  << bytes * MB << " MB \n" << std::endl;
}

//==================================
// Print device properties 
//==================================
__host__ void printDeviceProp() 
{
  int nDevices;

  cudaGetDeviceCount(&nDevices);

  for (int i = 0; i < nDevices; i++) {
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, i);
    std::cout << "Device Number: " << i << std::endl;
    std::cout << "  Device name: " << prop.name << std::endl;
    std::cout << "  Memory Clock Rate (KHz): " 
      << prop.memoryClockRate << std::endl;
    std::cout << "  Memory Bus Width (bits): "
      << prop.memoryBusWidth << std::endl;
    std::cout << "  Peak Memory Bandwidth (GB/s): " 
      << 2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6 
      << "\n" << std::endl;
  }
}



