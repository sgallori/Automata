#ifndef UTILITY_CUH
#define UTILITY_CUH 1

#include <iostream> // C++
#include <stdint.h>
#include <assert.h>

#include "PatFwdHitsContainer.cuh" // Containers
#include "TrackletsContainer.cuh"

#include <iostream>

#define CUDA_ERROR_CHECK

#define CudaSafeCall( err ) __cudaSafeCall( err, __FILE__, __LINE__ )
#define CudaCheckError()    __cudaCheckError( __FILE__, __LINE__ )

inline void __cudaSafeCall( cudaError err, const char *file, const int line ) {
#ifdef CUDA_ERROR_CHECK
  if ( cudaSuccess != err ) {
    fprintf( stderr, "cudaSafeCall() failed at %s:%i : %s\n",
             file, line, cudaGetErrorString( err ) );
    exit( -1 );
  }
#endif
  return;
}

inline void __cudaCheckError( const char *file, const int line ) {
#ifdef CUDA_ERROR_CHECK
  cudaError err = cudaGetLastError();
  if ( cudaSuccess != err ) {
    fprintf( stderr, "cudaCheckError() failed at %s:%i : %s\n",
             file, line, cudaGetErrorString( err ) );
    exit( -1 );
  }

  // More careful checking. However, this will affect performance.
  // Comment away if needed.
  /*
  err = cudaDeviceSynchronize();
  if( cudaSuccess != err ) {
    fprintf( stderr, "cudaCheckError() with sync failed at %s:%i : %s\n",
              file, line, cudaGetErrorString( err ) );
    exit( -1 );
  }
  */
#endif
  return;
}

// Useful constants
const float MB = 1. / 1024 / 1024;
const float GB = 1. / 1024 / 1024 / 1024;
const float sec = 1. / 1000;

/* Wakeup device before running */
__global__ void WakeUp();

/* Print device memory usage */
__host__ void printMemUsage();

/* Print device properties */
__host__ void printDeviceProp();

/* Profile cudaMemcpy */
template <typename T>
__host__ void cudaMemcpyProf( T *h_data, T *d_data, int bytes )
{
  cudaEvent_t start, stop;
  cudaEventCreate( &start );
  cudaEventCreate( &stop );

  cudaEventRecord( start, 0 );
  cudaMemcpy( d_data, h_data, bytes, cudaMemcpyHostToDevice );
  cudaEventRecord( stop, 0 );
  cudaEventSynchronize( stop );

  float milliseconds = 0;
  cudaEventElapsedTime( &milliseconds, start, stop );

  std::cout << "INFO: Transfer size (MB) = " 
            << bytes * MB << " MB" << std::endl; 
  std::cout << "INFO: Transfer time (ms) = " 
            << milliseconds << " ms" << std::endl; 
  std::cout << "INFO: Host to Device bandwidth (GB/s) = " 
            << (bytes / milliseconds) * GB / sec <<  "\n" << std::endl;

  cudaEventDestroy( start );
  cudaEventDestroy( stop );
}

                            
     
     
                           

#endif
