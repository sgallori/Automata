#ifndef DISPLAYER_H
#define DISPLAYER_H 1

#include "Event.h"

#include "TROOT.h" // ROOT
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TLine.h>
#include <TString.h>
#include <TMarker.h>

#define ZMAX +9500.0 // mm (max drawing range)
#define ZMIN +7500.0 // mm (min drawing range)

//------------------------------
// Displayer class
//------------------------------
class Displayer
{
  public:
    Displayer() {};
    ~Displayer() {};

    // Display event
    void Display(Event &event, std::string whatToShow = "XZ-tracks")
    {
      // Steer drawing options
      if (whatToShow == "nothing") return;
      else {
	DisplayEmptySpace(event);

	if (whatToShow == "hits") DisplayHits(event);
	else if (whatToShow == "tracks") DisplayTracks(event, "XZ");
	else if (whatToShow == "XZ-tracks") DisplayTracks(event, "XZ");
	else if (whatToShow == "YZ-tracks") DisplayTracks(event, "YZ");
	else if (whatToShow == "all") {
	  DisplayHits(event);
	  DisplayTracks(event, "XZ");
	}
	else std::cout << "> ERROR: Drawing option not known! \n" << endl;
      }
    }


    // Display empty space
    void DisplayEmptySpace(Event &event)
    {
      std::ostringstream os;
      os << event.number();
      TString evtNb = os.str();

      TCanvas *cViewer = new TCanvas("canv_evt" + evtNb,"Event display",831,765);

      TH2D* h = new TH2D("hEvt" + evtNb, "", 500, ZMIN, ZMAX, 500, -3500., +3500.);
      h->GetXaxis()->SetTitle("Z-axis, mm");
      h->GetYaxis()->SetTitle("X-axis, mm");
      h->SetTitle("Event number " + evtNb);
      h->Draw();

      // Draw Hough plane
      TLine *line = new TLine();
      line->SetLineColor(kBlue);
      line->SetLineWidth(2);
      line->SetLineStyle(1);
      line->DrawLine(8520.0, -3500.0, 8520.0, +3500);
    }


    // Display hits in event
    void DisplayHits(Event &event) 
    {
      // To be done... 
    }


    // Display tracks in event
    void DisplayTracks(Event &event, std::string proj = "XZ")
    {
      const double dxdy = 0.087489; // stereo angle

      TLine *line = new TLine();
      line->SetLineColor(kBlue);
      line->SetLineWidth(1);
      line->SetLineStyle(1);

      TMarker *marker = new TMarker();
      marker->SetMarkerColor(kRed);
      marker->SetMarkerStyle(20);
      marker->SetMarkerSize(0.6);

      // Sanity check
      if (proj != "XZ" && proj != "YZ") {
	std::cout << "ERROR: Drawing projection " << proj << " not supported!" << std::endl;
	return;
      }

      // Draw tracks of the event
      for (Track track : event.tracks()) {
	double tx = track.tx();
	double ty = track.ty();
	double x0 = track.x0();
	double y0 = track.y0();
	double z0 = track.z0();

	double zMin = ZMAX; // z(min,max) where to start drawing track 
	double zMax = ZMIN;

	// Draw hits on track
	for (Hit hit : track.hits()) {
	  unsigned int lay = hit.layer();
	  double xh = hit.x();
	  double zh = hit.z();

	  // Draw hits on XZ plane
	  if (proj == "XZ") { 
	    marker->DrawMarker(zh, xh);
	  }

	  // Draw hits on YZ plane
	  if (proj == "YZ") {
	    if (lay == 1 || lay == 2)  { // stereo layers
	      double dx = track.xAtZ( zh ) - xh;
	      double yh = (lay == 1) ? (dx / dxdy) : -(dx / dxdy); 
	      marker->DrawMarker(zh, yh);
	    }
	  }

	  if (zh < zMin) zMin = zh;
	  if (zh > zMax) zMax = zh;
	}//hits

	double xMin = track.xAtZ( zMin ); // track position at z(min,max)
	double xMax = track.xAtZ( zMax ); 
	double yMin = track.yAtZ( zMin );
	double yMax = track.yAtZ( zMax );

	// Draw track on XZ plane
	if (proj == "XZ") {
	  line->DrawLine(zMin, xMin, zMax, xMax);
	}

	// Draw track on YZ plane
	if (proj == "YZ") {
	  line->DrawLine(zMin, yMin, zMax, yMax);
	}
      }//tracks
    }

    // Members
};
#endif
