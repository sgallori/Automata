#ifndef EVENT_H
#define EVENT_H 1

#include <vector>
#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <cstdlib>
#include <ctime>
#include <cmath>

//------------------------------
// Hit class
//------------------------------
class Hit 
{
  public:
    Hit() {};
    ~Hit() {};
    Hit(unsigned int sta, 
	unsigned int lay, 
	unsigned int reg, 
	double x, 
	double z) 
    {
      m_station = sta;
      m_layer = lay;
      m_region = reg;
      m_x = x;
      m_z = z;
    }

    unsigned int layer() { return m_layer; }
    double x() { return m_x; }
    double z() { return m_z; }

    // Members
    unsigned int m_station;
    unsigned int m_layer;
    unsigned int m_region;
    unsigned int m_module;
    unsigned int m_monolayerB;
    unsigned int m_straw;
    double m_x;
    double m_z;

  private:
};
typedef std::vector<Hit> Hits;


//------------------------------
// Track class
//------------------------------
class Track 
{
  public:
    Track() {};
    ~Track() {};
    Track(double x0,
	  double y0,
	  double z0,
	  double tx,
	  double ty)
    {
      m_x0 = x0;
      m_y0 = y0;
      m_z0 = z0;
      m_tx = tx;
      m_ty = ty;
    }

    Hits hits() { return m_hits; }
    void addHit(Hit hit) { m_hits.push_back(hit); }
    double x0() { return m_x0; }
    double y0() { return m_y0; }
    double z0() { return m_z0; }
    double tx() { return m_tx; }
    double ty() { return m_ty; }
    double xAtZ(double z) { return m_x0 + m_tx * (z - m_z0); }
    double yAtZ(double z) { return m_y0 + m_ty * (z - m_z0); }

    // Members
    double m_x0;
    double m_y0;
    double m_z0;
    double m_tx;
    double m_ty;
    Hits m_hits;

  private:
};
typedef std::vector<Track> Tracks;


//------------------------------
// Event class
//------------------------------
class Event
{
  public:
    Event() {};
    ~Event() {};
    Event(unsigned int number, 
	  unsigned int nHits, 
	  unsigned int nTracks) 
    {
      m_nHits = nHits;
      m_nTracks = nTracks;
      m_number = number;
      m_tracks.reserve(nTracks);
    };

    unsigned int nHits() { return m_nHits; }
    unsigned int nTracks() { return m_nTracks; }
    unsigned int number() { return m_number; }
    Hits hits(unsigned int sta, 
	      unsigned int lay, 
	      unsigned int reg) 
    { return m_hits[sta][lay][reg]; }
    Tracks tracks() { return m_tracks; }
    
    // Print stats
    void PrintStats() 
    {
      std::cout << std::endl;
      std::cout << "> INFO: Processed event " << m_number << " has:" << std::endl;
      std::cout << "        " << m_nHits << " T-stations hits." << std::endl;
      std::cout << "        " << m_nTracks << " reconstructed tracks." << std::endl;
    }


    // Unapck event
    void Unpack(ifstream& file) 
    { // BEWARE: DATA SHOULD BE READ IN THIS ORDER!!! 
      UnpackHits(file);
      UnpackTracks(file);
    }


    // Load all T-stations hits
    void UnpackHits (ifstream& file)
    { // INFO:        
      //  VeloR=0,    ///< Velo R sensor
      //  VeloPhi=1,  ///< Velo Phi sensor
      //  TT=2,       ///< TT sensor
      //  IT=3,       ///< IT sensor
      //  OT=4,       ///< OT detector
      //  UT=5,       ///< UT sensor
      //  UNKNOWN=6   ///< Unknown type
      unsigned int station, layer, region, detType;
      unsigned int module, monolayerB, straw;
      double xh, zh;

      // Clear container
      for (unsigned int sta = 0; sta < 3; sta++) 
	for (unsigned int lay = 0; lay < 4; lay++) 
	  for (unsigned int reg = 0; reg < 6; reg++) 
	    m_hits[sta][lay][reg].clear();

      // Load hits
      for (unsigned int ih = 0; ih < m_nHits; ih++) {
	if (file.eof()) break;
	file >> station >> layer >> region >> xh >> zh; 
	//file >> station >> layer >> detType >> detRegion >> module >> monolayerB >> straw >> xh >> zh; 
	//std::cout << station << " " <<  layer << " " << region << " " << xh << " " << zh; 
	
	Hit hit(station, layer, region, xh, zh);
	m_hits[station][layer][region].push_back( hit );
      }//hits
    }


    // Load all recostructed tracks
    void UnpackTracks (ifstream& file)
    {
      unsigned int nHitsOnTrack;
      unsigned int station, layer, region, detType;
      unsigned int module, monolayerB, straw;
      double x0, y0, z0, tx, ty;
      double xh, zh;

      // Clear container 
      m_tracks.clear();

      // Load reconstructed tracks
      for (unsigned int itr = 0; itr < m_nTracks; itr++) {
	if (file.eof()) break;
	file >> nHitsOnTrack >> x0 >> y0 >> z0 >> tx >> ty;
	Track track(x0, y0, z0, tx, ty);

	// Load track hits
	for (unsigned int ih = 0; ih < nHitsOnTrack; ih++) {
	  file >> station >> layer >> region >> xh >> zh;
	  //file >> station >> layer >> detType >> detRegion >> module >> monolayerB >> straw >> xh >> zh;
	  Hit hit(station, layer, region, xh, zh);
	  track.addHit(hit);
	}//hits

	m_tracks.push_back( track );
      }//tracks
    }
    

    // Members
    unsigned int m_number;
    unsigned int m_nHits;
    unsigned int m_nTracks;
    Hits m_hits[3][4][6];
    Tracks m_tracks;
};
typedef std::vector<Event> Events;
#endif

