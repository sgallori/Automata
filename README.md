**How to install the algorithm (CMAKE version)**

*Install Brunel* 

(Use default package version if not specified)

    lb-dev Brunel v48r0
    SetupBrunel v48r0 
    cd BrunelDev_v48r0
    getpack -p anonymous Tf/PatAlgorithms v4r39
    getpack -p anonymous Tr/PatFitParams
    getpack -p anonymous Tf/TrackSys 
    cp -r ~/Automata/src/* Tf/PatAlgorithms/src
    cp ~/Automata/PatChecking.py Tf/TrackSys/python/TrackSys/
    cp ~/Automata/AutomataFitParams* Tr/PatFitParams/src
    cp ~/Automata/CMakeLists.txt Tf/PatAlgorithms/

*Install GPUManager*

    cd ..
    git clone https://gitlab.cern.ch/dcampora/lhcb-gpu-manager.git
    cd BrunelDev_v48r0
    cp -r ../lhcb-gpu-manager/GpuManager/GpuManager . 
    cp -r ../lhcb-gpu-manager/GpuManager/cmake .

*Add the line include(cmake/CUDA.cmake) in CMakeLists.txt*

*Install kernels*

    cp -r ~/Automata/AutomataCuda Tf/PatAlgorithms

*Compile all*

    make -j
    SetupBrunel v48r0 

*Option file is in scripts/checkerMC.py*

--------------------------------------------------

**How to run AutomataViewer**

    Unflag DUMP_INFO in AutomataTool.h (enable some outputfiles to be written during exec)
    Files placed in directory steered by DumpFolder property
    Data file containing events info (tracks, hits) is named TStationsEvents.dat
    exec viewer: root -l; .L EventDisplay.C+; EventDisplay(...)

