#!/usr/bin/env gaudirun.py
from Gaudi.Configuration import *
from Configurables import Brunel, LHCbApp, L0Conf, GaudiSequencer
from GaudiConf import IOHelper

from Configurables import NTupleSvc
from Configurables import TrackIPResolutionChecker
from Configurables import TrackIPResolutionCheckerNT
from Configurables import (FastVeloTracking)
from Configurables import (PatForward, PatForwardTool, PatFwdTool)
from Configurables import (PatSeeding, PatSeedingTool, PatSeedTool)
from Configurables import (GalloSeeding, AutomataTool)
from Configurables import (GaudiSequencer, FlagHitsForPatternReco)
from Configurables import AutomataFitParams

# Input file
#IOHelper('ROOT').inputFiles(['/afs/cern.ch/work/s/sgallori/DataOnDisk/mc2012/5120events_bsphiphi_MC2012_uncompressed.digi']) #2012
IOHelper('ROOT').inputFiles(['/afs/cern.ch/work/s/sgallori/DataOnDisk/mc2015/B02Kstmumu-Beam6500GeV-RunII-MagUp-Nu16-25ns_1.xdigi']) #2015
#importOptions("Data/B02Kstmumu-Beam6500GeV-RunII-MagUp-Nu26-25ns-Pythia8Sim08e-XDIGI.py") # Nu = 2.6, 25ns
#importOptions("Data/B02Kstmumu-Beam6500GeV-RunII-MagUp-Nu16-25ns-Pythia8Sim08f-XDIGI.py")  # Nu = 1.6, 25ns 
#importOptions("Data/B02Kstmumu-Beam6500GeV-RunII-MagUp-Nu16-50ns-Pythia8Sim08f-XDIGI.py") # Nu = 1.6, 50ns 
# Max events to process
Brunel().EvtMax = 50
# Are MC events?
Brunel().Simulation = True
# Data format
Brunel().InputType = "DIGI"
Brunel().WriteFSR = False
# Data type
Brunel().DataType = "2012"
# Match the DDDB and CondBD tags to the events
#Brunel().DDDBtag = "Sim08-20130503-1" # 2012
#Brunel().CondDBtag = "Sim08-20130503-1-vc-md100"
Brunel().DDDBtag = "dddb-20130929-1" # 2015
Brunel().CondDBtag = "sim-20131023-vc-mu100"
# Use MC truth info?
Brunel().WithMC = True
# L0 config
L0Conf().EnsureKnownTCK = False
# Context
Brunel().Context = "Offline"
# Output level
Brunel().OutputLevel = 3


# FastVelo options
FastVeloTracking().HLT1Only = False
FastVeloTracking().TimingMeasurement= True

# PatForward options
PatForward("PatForward").TimingMeasurement= True
#PatForward("PatForward").OutputLevel= DEBUG

# PatSeeding options
PatSeeding('PatSeeding').TimingMeasurement= True
PatSeeding('PatSeeding').addTool(PatSeedingTool('PatSeedingTool'), name = 'PatSeedingTool') 
PatSeeding('PatSeeding').PatSeedingTool.MeasureTime = True
#PatSeeding("PatSeeding").OutputLevel = DEBUG

# GalloSeeding options
GalloSeeding('GalloSeeding').TimingMeasurement= True
GalloSeeding('GalloSeeding').addTool(AutomataTool('AutomataTool'), name = 'AutomataTool') 
GalloSeeding('GalloSeeding').AutomataTool.MeasureTime = True
GalloSeeding('GalloSeeding').AutomataTool.DumpFolder = "./"
GalloSeeding('GalloSeeding').AutomataTool.MaxOTHits = 15000

# Add GalloSeeding to PatSeed sequence
GaudiSequencer("TrackSeedPatSeq").Members += [ GalloSeeding('GalloSeeding') ]

# Run MCLinksSeq before TrackSeedSeq (to allow running MC association in Seeding)
SeqSeeding = GaudiSequencer("TrackSeedPatSeq")
SeqMCLinks = GaudiSequencer("MCLinksUnpackSeq")
SeqSeeding.Members = [ SeqMCLinks ] + SeqSeeding.Members

# Tool for resolution checks
TrackIPResolutionChecker().TrackContainer = FastVeloTracking().getProp("OutputTracksName")
def ipcheck():
    GaudiSequencer("CheckPatSeq").Members += [ TrackIPResolutionChecker() ]
appendPostConfigAction(ipcheck)

# Tool for switching off/reduce efficiency of T-stations
def patch_earlyDecoding(): 
    GaudiSequencer("RecoDecodingSeq").Members += [ GaudiSequencer("RecoHitEffPatchingSeq") ] 
appendPostConfigAction(patch_earlyDecoding)

hiteffpatchSeq = GaudiSequencer("RecoHitEffPatchingSeq") 
hiteffpatcher = FlagHitsForPatternReco("FlagHitsForPatternReco") 
hiteffpatchSeq.Members = [ hiteffpatcher ]
hiteffpatcher.EfficiencyCorrections += [
                                        # IT
                                        "ITS0E0.0",
                                        "ITS1E0.0",
                                        "ITS2E0.0" 
                                       ]

# Tool for fitting parameters of the seeding
NTupleSvc().Output += [ "FILE1 DATAFILE='AutomataFitParams.root' TYP='POOL_ROOTTREE' OPT='NEW'" ]
def addAutomataFitParams():
    GaudiSequencer('CheckPatSeq').Members += [ AutomataFitParams('AutomataFitParams') ]
appendPostConfigAction( addAutomataFitParams )

