#ifndef AUTOMATA_H
#define AUTOMATA_H 1

//=========================================================================
// Cellular Automata namespace
//=========================================================================
namespace Automata 
{
  const static double BigDouble = +999.0;
  const static int BigInt = +999;
  const static double zMagnet = 5383.17; //mm
  const static double magKick = 1.1821;  //GeV
 
  // Max. num of hits on cluster
  const static unsigned int MAX_HITS_ON_CLUS = 3; 

  // Max. clusters on tracklet 
  const static unsigned int MAX_CLUS_ON_STUB = 4;

  // Max. hits on tracklet 
  const static unsigned int MAX_HITS_ON_STUB = 
                            MAX_HITS_ON_CLUS * MAX_CLUS_ON_STUB;

  // Max. neighbours per tracklet
  const static unsigned int MAX_NB_PER_STUB = 5;

  // Max. num of hits on track
  const static unsigned int MAX_HITS_ON_TRACK = 36; 
}
#endif

