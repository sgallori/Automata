#include <cmath>
#include <algorithm>
#include <functional>
#include <array>

// from Gaudi
#include "GaudiKernel/ToolFactory.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/SystemOfUnits.h"

// from Event
#include "Event/StateParameters.h"
#include "Event/ProcStatus.h"

// from ROOT
#include <TMath.h>

// local
#include "AutomataTool.h"
#include "TfKernel/RegionID.h"

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( AutomataTool )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
AutomataTool::AutomataTool( const std::string& type,
                            const std::string& name,
                            const IInterface* parent)
#if defined DEBUG_AUTOMATA || defined FINAL_PLOTS 
  : GaudiTupleTool ( type, name , parent )
#else
  : GaudiTool ( type, name , parent )
#endif
{
  declareInterface<AutomataTool>(this);
  //declareInterface<IPatSeedingTool>(this);
  //declareInterface<ITracksFromTrack>(this);

  // NOTE: some properties are deliberately set to an unreasonable value
  // (-1e42) because we want to supply defaults later in initialize
  // if the user did not specify the values via job options
  // we have to wait until initialize because we need the field service
  // in an initialized state to decide which defaults to fill in
  //------------------------------------------------------------------------
  // hit level stuff
  //------------------------------------------------------------------------
  declareProperty( "reusePatSeeding",		m_reusePatSeeding	= true );
  declareProperty( "UseForward",		m_useForward		= false       );
  declareProperty( "OnlyGood",			m_onlyGood		= false  );
  declareProperty( "DiscardChi2",		m_discardChi2		= 1.5      ); 
  declareProperty( "InputTracksName",		m_inputTracksName	= LHCb::TrackLocation::Forward  );
  declareProperty( "DriftRadiusRange",		m_driftRadiusRange	= {-0.6 * Gaudi::Units::mm,2.8 * Gaudi::Units::mm});

  //------------------------------------------------------------------------
  // track model
  //------------------------------------------------------------------------
  declareProperty( "dRatio",			m_dRatio		= -1e42);
  declareProperty( "InitialArrow",		m_initialArrow		= -1e42);
  declareProperty( "MomentumScale",		m_momentumScale		= -1e42);
  declareProperty( "zReference",		m_zReference		=  StateParameters::ZMidT);

  //------------------------------------------------------------------------
  // options for track search in xz projection
  //------------------------------------------------------------------------
  // curvature cut
  declareProperty( "MinMomentum",		m_minMomentum		=  500. * Gaudi::Units::MeV );
  declareProperty( "CurveTol",			m_curveTol		=    3. * Gaudi::Units::mm );
  // center of magnet compatibility
  declareProperty( "zMagnet",			m_zMagnet		= -1e42 );
  declareProperty( "xMagTol",			m_xMagTol		= 2000. * Gaudi::Units::mm );
  // window to collect hits from
  declareProperty( "TolCollectOT",		m_tolCollectOT		=    3. * Gaudi::Units::mm );
  declareProperty( "TolCollectIT",		m_tolCollectIT		=    .3 * Gaudi::Units::mm );
  declareProperty( "TolCollectITOT",		m_tolCollectITOT	=    .6 * Gaudi::Units::mm );
  // clone killing in xz projection
  declareProperty( "MinXPlanes",		m_minXPlanes		=    5                     );
  declareProperty( "CommonXFraction",		m_commonXFraction	=    0.7                   );
  declareProperty( "QualityWeights",		m_qualityWeights	= { 1.0,-0.2});
  // demands that NDblOTHitsInXSearch out of three layers used for the initial
  // parabola have a hits in both monolayers
  declareProperty( "NDblOTHitsInXSearch",	m_nDblOTHitsInXSearch	= 0 );

  //------------------------------------------------------------------------
  // options for track search in stereo layers
  //------------------------------------------------------------------------
  // projection plane for stereo hits
  declareProperty( "zForYMatch",		m_zForYMatch		= 9000. * Gaudi::Units::mm );
  // spread of run of stereo hits in projection plane
  declareProperty( "MaxRangeOT",		m_maxRangeOT		=  150. * Gaudi::Units::mm );
  declareProperty( "MaxRangeIT",		m_maxRangeIT		=   15. * Gaudi::Units::mm );
  // pointing criterium in y
  declareProperty( "yCorrection",		m_yCorrection		= -1e42);
  declareProperty( "MaxYAtOrigin",		m_maxYAtOrigin		=  400. * Gaudi::Units::mm );
  declareProperty( "MaxYAtOriginLowQual",	m_maxYAtOriginLowQual	= 1500. * Gaudi::Units::mm );
  // tolerance to check if a hit is within the sensitive y area of straw/sensor
  declareProperty( "yTolSensArea",		m_yTolSensArea		=   40. * Gaudi::Units::mm );
  // y hit requirements during per-region stereo search
  declareProperty( "MinStHitsPerStaOT",		m_minStHitsPerStaOT	= 1			   );
  declareProperty( "MinTotStHitsOT",		m_minTotStHitsOT	= 6			   );

  //------------------------------------------------------------------------
  // track search starting with quartett in IT
  //------------------------------------------------------------------------
  declareProperty( "TolExtrapolate",		m_tolExtrapolate	=    4. * Gaudi::Units::mm );

  //------------------------------------------------------------------------
  // internal fit, outlier removal, minimum number of planes
  //------------------------------------------------------------------------
  declareProperty( "MaxChi2HitOT",		m_maxChi2HitOT		=   30.                    );
  declareProperty( "MaxChi2HitIT",		m_maxChi2HitIT		=   10.                    );
  declareProperty( "MaxTrackChi2",		m_maxTrackChi2     	=   15.                    );
  declareProperty( "MaxFinalChi2",		m_maxFinalChi2     	=   12.25                  );
  declareProperty( "MaxFinalTrackChi2",		m_maxFinalTrackChi2	=    9.                    );
  declareProperty( "MaxTrackChi2LowMult",	m_maxTrackChi2LowMult	=    6.			   );
  declareProperty( "MinTotalPlanes",		m_minTotalPlanes	=    9			   );
  declareProperty( "MaxMisses",			m_maxMisses		=    1			   );
  declareProperty( "OTNHitsLowThresh",		m_otNHitsLowThresh	=   17			   );

  declareProperty( "MinPlanesPerStation",	m_minPlanesPerStation	=    1			   );
  declareProperty( "MaxHoles",			m_maxHoles		=    2			   );
  declareProperty( "ITStubLooseChi2",		m_itStubLooseChi2	= 1500.			   );
  declareProperty( "ITStubTightChi2",		m_itStubTightChi2	=   80.			   );

  //------------------------------------------------------------------------
  // final track selection
  //------------------------------------------------------------------------
  // maximum fraction of used hits
  declareProperty( "MaxUsedFractPerRegion",	m_maxUsedFractPerRegion	=  0.30			   );
  declareProperty( "MaxUsedFractITOT",		m_maxUsedFractITOT	=  0.15			   );
  declareProperty( "MaxUsedFractLowQual",	m_maxUsedFractLowQual	=  0.05   		   );

  //------------------------------------------------------------------------
  // reference states for subsequent refitting using Kalman filter
  //------------------------------------------------------------------------
  declareProperty( "StateErrorX2",		m_stateErrorX2		=   4. );
  declareProperty( "StateErrorY2",		m_stateErrorY2		= 400. );
  declareProperty( "StateErrorTX2",		m_stateErrorTX2		= 6e-5 );
  declareProperty( "StateErrorTY2",		m_stateErrorTY2		= 1e-4 );
  declareProperty( "MomentumToolName",	        m_momentumToolName	= "FastMomentumEstimate" );
  declareProperty( "ZOutput",			m_zOutputs		= {StateParameters::ZBegT,StateParameters::ZMidT,StateParameters::ZEndT});
  
  //------------------------------------------------------------------------
  // options concerning copying the T station part of forward tracks
  //------------------------------------------------------------------------
  // should we copy T station part of forward tracks (default: no)
  declareProperty( "UseForwardTracks",		m_useForwardTracks	= false       );
  // maximal difference in track parameters for two tracks to be considered clones
  declareProperty( "ForwardCloneMaxXDist",	m_forwardCloneMaxXDist	=   10.0 * Gaudi::Units::mm );
  declareProperty( "ForwardCloneMaxYDist",	m_forwardCloneMaxYDist	=   50.0 * Gaudi::Units::mm );
  declareProperty( "ForwardCloneMaxTXDist",	m_forwardCloneMaxTXDist	=    0.005 );
  // if clones defined in the sense above share more than a fraction of
  // ForwardCloneMaxSharedof hits, only the longer track survives
  declareProperty( "ForwardCloneMaxShared",	m_forwardCloneMaxShared = 0.3);
  // if clones defined in the sense above share less than above fraction
  // of hits, this property determines if they will be merged into a single
  // T station track (as PatForward is tuned for efficiency, T station track
  // segments contain about 5% clones which are due to a T station track
  // being "split" to match several Velo tracks)
  declareProperty( "ForwardCloneMergeSeg",	m_forwardCloneMergeSeg	= true );

  //------------------------------------------------------------------------
  // anything that doesn't fit above
  //------------------------------------------------------------------------
  // per-stage timing measurements
  declareProperty( "MeasureTime",		m_measureTime		= false       );
  // special cuts for magnetic field off
  declareProperty( "FieldOff",			m_fieldOff		= false	      );
  // special isolation cuts for low ghost fraction (efficiency drops!)
  declareProperty( "EnforceIsolation",		m_enforceIsolation	= false );
  declareProperty( "ITIsolation",		m_ITIsolation		=   15. * Gaudi::Units::mm );
  declareProperty( "OTIsolation",		m_OTIsolation		=   20. * Gaudi::Units::mm );
  declareProperty( "Cosmics",			m_cosmics		= false );
  // maximal occupancy
  declareProperty( "MaxITHits",			   m_maxITHits        = 3000 );
  declareProperty( "MaxOTHits",			   m_maxOTHits        = 10000 );
  declareProperty( "AbortOnVeloAbort", m_abortOnVeloAbort = true );

  declareProperty( "ActivateHltPropertyHack", m_activateHLTPropertyHack = true ); 

  // Where to save debugging files 
  declareProperty( "DumpFolder", m_dumpFolder = "."); 
 
  // Offlad algorithm to an accelarator device
  declareProperty( "Offload", m_offload = false); 
}


//=============================================================================
// Destructor
//=============================================================================
AutomataTool::~AutomataTool() {}


//=============================================================================
// Initialization
//=============================================================================
StatusCode AutomataTool::initialize() 
{
  // Initialize server
// only if you use client-server
  if (m_offload) {
    info() << "--- Initializing GpuService" << endmsg;
    m_gpuService = svc<IGpuService>("GpuService", true);
  }

  StatusCode sc = GaudiTool::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiTool

  m_tHitManager = tool<Tf::TStationHitManager<PatForwardHit> >("PatTStationHitManager");

  m_magFieldSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );

  m_online = false;
  if (context() == "HLT" || context() == "Hlt") {
    if (msgLevel(MSG::DEBUG)) {
      debug() << "Running in HLT context: Online = true" << endmsg;
    }
    m_online = true;
  } else if (context() != "" && context() != "Offline") {
    Warning( "Unexpected context '" + context() +
	"'. Assuming offline mode, please check !" ).ignore();
  }

  const bool activateHLTPropertyHack = m_online && m_activateHLTPropertyHack;
  if (activateHLTPropertyHack) {
    char buf[8192];
    debug() << "HLT property hack about to be activated" << endmsg <<
      "dumping values of potentially affected properties before hack:" << endmsg;
    std::snprintf(buf, 8191, "\nInitialArrow % 24.17e\n"
	"MomentumScale % 24.17e\n"
	"dRatio % 24.17e\n"
	"zMagnet % 24.17e\n"
	"yCorrection % 24.17e\n",
	m_initialArrow, m_momentumScale, m_dRatio, m_zMagnet, m_yCorrection);
    buf[8191] = 0;
    debug() << buf << endmsg << "applying HLT property hack" << endmsg;
  }
  if (m_magFieldSvc->useRealMap()) {
    if (activateHLTPropertyHack || -1e42 == m_initialArrow)
      m_initialArrow  = 4.25307e-09;
    if (activateHLTPropertyHack || -1e42 == m_momentumScale)
      m_momentumScale = 35.31328;
    if (activateHLTPropertyHack || -1e42 == m_dRatio)
      m_dRatio = -3.2265e-4;
    if (activateHLTPropertyHack || -1e42 == m_zMagnet)
      m_zMagnet = 5383.17 * Gaudi::Units::mm;
    if (activateHLTPropertyHack || -1e42 == m_yCorrection)
      m_yCorrection = 4.73385e-15;
  } else {
    if (activateHLTPropertyHack || -1e42 == m_initialArrow)
      m_initialArrow = 4.21826e-09;
    if (activateHLTPropertyHack || -1e42 == m_momentumScale)
      m_momentumScale = 40.3751;
    if (activateHLTPropertyHack || -1e42 == m_dRatio)
      m_dRatio = -3.81831e-4;
    if (activateHLTPropertyHack || -1e42 == m_zMagnet)
      m_zMagnet = 5372.1 * Gaudi::Units::mm;
    if (activateHLTPropertyHack || -1e42 == m_yCorrection)
      m_yCorrection = 8.6746e-15;
  }
  if (activateHLTPropertyHack) {
    char buf[8192];
    debug() << "HLT property hack has been activated" << endmsg <<
      "dumping values of potentially affected properties after hack:" << endmsg;
    std::snprintf(buf, 8191, "\nInitialArrow % 24.17e\n"
	"MomentumScale % 24.17e\n"
	"dRatio % 24.17e\n"
	"zMagnet % 24.17e\n"
	"yCorrection % 24.17e\n",
	m_initialArrow, m_momentumScale, m_dRatio, m_zMagnet, m_yCorrection);
    buf[8191] = 0;
    debug() << buf << endmsg << "end of HLT property hack handling" << endmsg;
  }

  m_seedTool = tool<PatSeedTool>( "PatSeedTool" );
  m_momentumTool = tool<ITrackMomentumEstimate>( m_momentumToolName );

  //== Max impact: first term is due to arrow ->curvature by / (mm)^2 then momentum to impact at z=0
  //== second term is decay of Ks: 210 MeV Pt, 2000 mm decay distance.
  // protect against division by near-zero
  if (1e-42 > fabs(m_minMomentum) || 1e-42 > fabs(m_momentumScale) ||
      1e-42 > fabs(m_initialArrow)) m_maxImpact = HUGE_VAL;
  else m_maxImpact =
    1. / ( m_momentumScale * m_initialArrow * m_minMomentum ) +
    2000. * 210. / m_minMomentum;

  // timing counters
  if ( m_measureTime ) {
    m_timer = tool<ISequencerTimerTool>( "SequencerTimerTool", this );
    // add a dummy timer entry
    m_timer->addTimer(name());
    //m_timer->addTimer("Cellular Automata");
    m_timer->increaseIndent();
    m_timeInit = m_timer->addTimer( "Prepare hits" );
    m_timeInitAutomata = m_timer->addTimer( "Initialization" );
    m_timeClustering = m_timer->addTimer( "Make clusters" );
    m_timeMakeXTracklets = m_timer->addTimer( "Make x-tracklets" );
    m_timeAddStereo = m_timer->addTimer( "Add stereo hits" );
    m_timeFitStubs = m_timer->addTimer( "Fit stubs" );
    m_timeFindNeighbours = m_timer->addTimer( "Find neighbours" );
    m_timeEvolve = m_timer->addTimer( "Evolve tracklets" );
    m_timeLink = m_timer->addTimer( "Link tracklets" );
    m_timeSelectLong = m_timer->addTimer( "Select long tracks" );
    m_timeSelectShort = m_timer->addTimer( "Select short tracks" );
    m_timeKillLongClones = m_timer->addTimer( "Kill long clones" );
    m_timeKillShortClones = m_timer->addTimer( "Kill short clones" );
    m_timeSerializer = m_timer->addTimer( "Hits serialization" );
    m_timeOffloading = m_timer->addTimer( "Offloading" );
    m_timer->decreaseIndent();
  }

  if (m_qualityWeights.size() < 2)
    return Error ( "Not enough qualityWeights values" );
  if (m_driftRadiusRange.size() < 2)
    return Error ( "Not enough DriftRadiusRange values" );
  if (m_driftRadiusRange[0] > m_driftRadiusRange[1]) {
    warning() << "swapping values in DriftRadiusRange such that "
      "lower limit <= upper limit" << endmsg;
    std::swap(m_driftRadiusRange[0], m_driftRadiusRange[1]);
  }

  if (m_nDblOTHitsInXSearch > 3)
    return Error("nDoubleHitsOT must be at most 3!");

  // Stefano
#ifdef DEBUG_AUTOMATA
  setHistoTopDir("Stefano/");
  setHistoDir("Debug/");
#endif

#ifdef FINAL_PLOTS
  setHistoTopDir("Stefano/");
  setHistoDir("Final/");
#endif

#ifdef DEBUG_AUTOMATA
   m_dumpFile = fopen( (m_dumpFolder + "/TStationsEvents.dat").c_str(), "w" ); 
   m_mvaSignalLongFile = fopen( (m_dumpFolder +"/mvaSignalLong.dat").c_str(), "w" ); 
   m_mvaGhostsLongFile = fopen( (m_dumpFolder +"/mvaGhostsLong.dat").c_str(), "w" ); 
   m_mvaSignalShortFile = fopen( (m_dumpFolder +"/mvaSignalShort.dat").c_str(), "w" ); 
   m_mvaGhostsShortFile = fopen( (m_dumpFolder +"/mvaGhostsShort.dat").c_str(), "w" ); 
#endif

  return StatusCode::SUCCESS;
}


//=============================================================================
// Finalization
//=============================================================================
StatusCode AutomataTool::finalize() 
{
  if (m_measureTime) {
    info() << name() << " finalizing now..." << endmsg;
  }

  return GaudiTool::finalize();
}


//=========================================================================
// Prepare hits for tracking
//=========================================================================
unsigned AutomataTool::prepareHits()
{
  m_printing = msgLevel( MSG::DEBUG );

  m_tHitManager->prepareHits();

  if ( m_measureTime ) m_timer->start( m_timeInit );

  // mark hits used if:
  // - used by a different algorithm (and user asked us to ignore them)
  // - drift radius out of range
  const PatRange driftRadRange(m_driftRadiusRange[0], m_driftRadiusRange[1]);
  HitRange range = hits();
  for( PatFwdHit* hit: range ) {
    hit->setSelected( true );
    hit->setIgnored( false );
    if (hit->hit()->ignore()){
      hit->setIsUsed( true );
      continue;
    }
   
    if ( m_useForward )
      if ( hit->hit()->testStatus(Tf::HitBase::UsedByPatForward) ) {
        hit->setIsUsed(true);
        continue;
      }
    if ( !m_reusePatSeeding )
      if ( hit->hit()->testStatus(Tf::HitBase::UsedByPatSeeding) ) {
        hit->setIsUsed(true);
        continue;
      }
    const Tf::OTHit* otHit = hit->hit()->othit();
    if ( otHit ) {
      const double driftRad = otHit->untruncatedDriftDistance();
      if ( !driftRadRange.isInside(driftRad) ) {
        hit->setIsUsed( true );
        continue;
      }
    }
    hit->setIsUsed(false);
  }

  // alternatively mark only those PatFwd hits
  // which come from tracks with bad chi2>m_discardChi2
  if (m_onlyGood) {
    LHCb::Tracks* fwdTracks  = get<LHCb::Tracks>( m_inputTracksName );
    for( const LHCb::Track* tF: *fwdTracks) {
      // check for good PatFwd tracks
      // from PatForward fit
      if (tF->fitStatus() == LHCb::Track::Fitted) {
        // if fitted - used chi2pdf from fit
        if (tF->chi2PerDoF() < m_discardChi2) continue;
      }
      //else {
      // otherwise use PatQuality from patreco
      //  if (tF->info(LHCb::Track::PatQuality, -1.) < m_discardPatQual) continue;
      //}
      if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) 
        debug() << "*** Found bad PatFwd track, re-marking hits unused. ";
      const std::vector<LHCb::LHCbID>& ids = tF->lhcbIDs();
      int nHits = 0;
      for(const LHCb::LHCbID id: ids) {
        for( PatFwdHit* hit: range ) {
          if (id == hit->hit()->lhcbID()) {
            nHits++;
            // mark hit as unused and exit this loop
            hit->setIsUsed(false);
            break;
          }
        }
      }
      if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) 
        debug() << nHits << " hits marked." << endmsg;
    }
  }

  // update region cache: we need some information not directly available
  // from the Tf::-framework
  for (unsigned sta = 0; sta < m_nSta; ++sta) {
    for (unsigned lay = 0; lay < m_nLay; ++lay) {
      for (unsigned reg = 0; reg < m_nReg; ++reg) {
	unsigned idx = (sta * m_nLay + lay) * m_nReg + reg;
	m_RCtanT[idx] = region(sta,lay,reg)->sinT() /
	  region(sta,lay,reg)->cosT();
	const Gaudi::XYZVector& vy(region(sta, lay, reg)->vy());
	m_RCdzdy[idx] = vy.z() / vy.y();
	const Gaudi::XYZVector& vx(region(sta, lay, reg)->vx());
	m_RCdzdx[idx] = vx.z() / vx.x();
	// return zmid at x = 0, y = 0 (instead of x = xmid, y = ymid)
	m_RCz0[idx] = region(sta,lay,reg)->z() -
	  region(sta,lay,reg)->y() * m_RCdzdy[idx] -
	  region(sta,lay,reg)->x() * m_RCdzdx[idx];
      }
    }
  }

  // if someone wants a printout, do so now
  if ( msgLevel( MSG::VERBOSE ) ) {
    for (unsigned reg = 0; reg < m_nReg; ++reg) {
      for (unsigned sta = 0; sta < m_nSta; ++sta) {
        for (unsigned lay = 0; lay < m_nLay; ++lay) {
          HitRange range1 = hits(sta, lay, reg);
          for( const PatFwdHit* hit: range1 ) {
            if ( ! hit->isUsed() ) continue;
            debugFwdHit( hit, verbose() );
          }
        }
      }
    }
  }

  if ( m_measureTime ) m_timer->stop( m_timeInit );

  return hits().size();
}


//=========================================================================
// Cellular Automata routine called by PatSeeding
//=========================================================================
StatusCode AutomataTool::cellularAutomata( LHCb::Tracks* outputTracks )
{ 
  StatusCode sc = cellularAutomata();

  // copy tracks from outvec to output container
  if (sc.isSuccess() && !m_lhcbTracks.empty()) {
    outputTracks->reserve(m_lhcbTracks.size());
    for (LHCb::Track* track : m_lhcbTracks) 
      outputTracks->insert(track);
  }

  return sc;
}


//=========================================================================
// Cellular Automata main routine
//=========================================================================
StatusCode AutomataTool::cellularAutomata()
{
  /* TODO: - Now Tracklet and CATrack have a PatSeedTrack(*) inside for an easy use of the fitters.
   *         The drawback is that you now have twice the same info (hits are stored in CATrack and in PatSeedTracks).
   *         * One option is to work from the beginning with PatSeedTrack but I would like to use the ~same classes also in GPU 
   *           for an easy poting,
   *         * The other is to try to embedd the fitters in my home-made classes
   *
   */

  // Protect against very hot events
  if( m_maxITHits < 999999 || m_maxOTHits < 999999 ) {
    m_nHitsOT = m_nHitsIT = 0;

    for (unsigned int sta = 0; sta < m_nSta; ++sta) {
      for (unsigned int lay = 0; lay < m_nLay; ++lay) {
        for (unsigned int reg = 0; reg < m_nReg; ++reg) {
          // keep track of number of hits
          if (isRegionOT(reg)) m_nHitsOT += hits(sta, lay, reg).size();
          else m_nHitsIT += hits(sta, lay, reg).size();
        }
      }
    }

    if (m_nHitsIT > m_maxITHits || m_nHitsOT > m_maxOTHits) {
      Warning("Skipping very hot event!", StatusCode::SUCCESS, 1).ignore();

      if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) 
        debug() << "Skipping very hot event! (" << m_nHitsIT << " IT hits "
                << m_nHitsOT << " OT hits)" << endmsg;
      
      return StatusCode::SUCCESS;
    }
  }

  initAutomata();
  if (m_offload) {
    offload();
    
  } else {
    makeClusters();
    makeXTracklets();
    addStereoHits();
    fitStubs();
    //collectTracklets(); 
    findNeighbours();
    evolve();
    linkTracklets();
    selectLongTracks();
    killLongClones();
    selectShortTracks();
    killShortClones();  
    collectTracks(); 
  }

// make same plots
#ifdef FINAL_PLOTS
	for ( LHCb::Track* track : m_lhcbTracks ) {
  
    const LHCb::State * state = track->stateAt (	LHCb::State::AtT );

    plot(state->tx(), "Final_tx", "tx of tracks", -2.0, +2.0, 100);
    plot(state->ty(), "Final_ty", "ty of tracks", -0.5, +0.5, 100);
    plot(state->x(), "Final_x0", "dx0 of tracks", -800.0, +800.0, 100);
	  plot(state->y(), "Final_y0", "dy0 of tracks", -600.0, +600.0, 100);
    plot(track->chi2PerDoF(), "Final_chi2PerDoF", "chi2PerDoF of tracks", -1.0, +5.0, 100);
    plot(track->chi2(), "Final_chi2", "chi2 of tracks", -1.0, +100.0, 100);
    plot(track->nLHCbIDs(), "Final_nHits", "Number of hits on tracks", 0.0, +36.0, 36);
  }
  
#endif

#ifdef DEBUG_AUTOMATA
  dumpEvent();
#endif

  return StatusCode::SUCCESS;
}


//=========================================================================
// Initialize all containers for Cellular Automata
//=========================================================================
StatusCode AutomataTool::initAutomata() 
{
  if ( m_measureTime ) m_timer->start( m_timeInitAutomata );

  m_lhcbTracks.clear();
  m_lhcbTracks.reserve(1024);

  // Loop on regions
  for (unsigned int reg = 0; reg < m_nReg; reg++) {
    m_outputTracks[reg].clear();
    m_outputTracks[reg].reserve(256);

    // Loop on stations
    for (unsigned int sta = 0; sta < m_nSta; sta++) {
      m_xTracklets[sta][reg].clear();
      m_xTracklets[sta][reg].reserve(1024);
      m_trackStubs[sta][reg].clear();
      m_trackStubs[sta][reg].reserve(1024);
      m_candTracks[sta][reg].clear();
      m_candTracks[sta][reg].reserve(256);

      // Loop on layers
      for (unsigned int lay = 0; lay < m_nLay; lay++) {
	m_clusters[sta][lay][reg].clear();
	m_clusters[sta][lay][reg].reserve(128);
      }//lay

    }//sta
  }//reg

#ifdef DEBUG_AUTOMATA
  // Init MC linkers
  m_otlinker = new Linker( evtSvc(), msgSvc(), LHCb::OTTimeLocation::Default );
  m_itlinker = new Linker( evtSvc(), msgSvc(), LHCb::STClusterLocation::ITClusters );
#endif

  if ( m_measureTime ) m_timer->stop( m_timeInitAutomata );

  return StatusCode::SUCCESS;
}


//=========================================================================
// Delete all containers
//=========================================================================
StatusCode AutomataTool::clearEvent() 
{
  // Loop on stations
  for (unsigned int sta = 0; sta < m_nSta; sta++) {
    // Loop on regions
    for (unsigned int reg = 0; reg < m_nReg; reg++) {
      // Loop on layers
      for (unsigned int lay = 0; lay < m_nLay; lay++) {
	std::for_each(m_clusters[sta][lay][reg].begin(), 
	              m_clusters[sta][lay][reg].end(), 
	              Cluster::deleteCluster);
      }
    }
  }
  
  return StatusCode::SUCCESS;
}


//=========================================================================
// Make clusters
//=========================================================================
StatusCode AutomataTool::makeClusters() 
{
  unsigned int nClustersOT = 0;

  if ( m_measureTime ) m_timer->start( m_timeClustering );

  // Loop over stations
  for (unsigned int sta = 0; sta < m_nSta; sta++) {
    // Loop over layers
    for (unsigned int lay = 0; lay < m_nLay; lay++) {
      // Loop over regions
      for (unsigned int reg = 0; reg < m_nReg; reg++) {
	// OT only!
	if (reg > 1) continue;

	// Get all hits
	HitRange range = hits(sta, lay, reg);

	// Clusterize hits
	makeClusters(range, m_clusters[sta][lay][reg]);

#ifdef DEBUG_AUTOMATA
	if (reg < 2) 
	  nClustersOT += m_clusters[sta][lay][reg].size();

	int nHits = range.size();
	int nClus = m_clusters[sta][lay][reg].size();

	plot(nHits, "nHitsOTPerReg", "Num. number of OT hits in sta, lay, reg", 0.0, 1000, 100);
	plot(nClus, "nClusOTPerReg", "Num. number of OT clusters in sta, lay, reg", 0.0, 1000, 100);
	//std::cout << "sta = " << sta << ", lay = " << lay << ", reg = " << reg << std::endl;
	//std::cout << "nHits = " << nHits << ", nClus = " << nClus << std::endl;
#endif
      }//reg
    }//lay
  }//sta

#ifdef DEBUG_AUTOMATA
  plot(m_nHitsOT, "nHitsOT", "Total number of OT hits in T-stations", 0.0, m_maxOTHits, 100);
  plot(m_nHitsIT, "nHitsIT", "Total number of IT hits in T-stations", 0.0, m_maxITHits, 100);
  plot(nClustersOT, "nClustersOT", "Total number of OT clusters", 0.0, m_maxOTHits, 100);
#endif

  if ( m_measureTime ) m_timer->stop( m_timeClustering );

  return StatusCode::SUCCESS;
}


//=========================================================================
// Make X-tracklets
//=========================================================================
StatusCode AutomataTool::makeXTracklets()
{
  /* Constants */
  // Search window for hits in layer3
  const double maxDx = 150.0; //mm
  // Max. dx/dz slope
  const double maxSlope = 1.5; //rad
  // Max. tracklet chi2
  const double maxChi2 = 300.0; 
  // Max. ellipse in x1/(z1-zMag) vs tx plane
  const double maxEllipse = 1.0; 
  // Ellipse constants
  const double sigma = 0.5;
  const double rho = 0.95;

  if ( m_measureTime ) m_timer->start( m_timeMakeXTracklets );

  // Loop over stations
  for (unsigned int sta = 0; sta < m_nSta; sta++) {
    unsigned int nXTracklets = 0;

    // Loop over regions
    for (unsigned int reg = 0; reg < m_nReg; reg++) {
      // OT only...
      if (reg > 1) continue;

      // Reference z-pos. of layers
      double zLay0 = region(sta, 0, reg)->z();
      double zLay3 = region(sta, 3, reg)->z();
      double zRatio = zLay3 / zLay0;
      //double zMid  = (zLay0 + zLay3) / 2.0;

      Clusters clustersX1 = clustersInRegion(sta, 0, reg);
      Clusters clustersX2 = clustersInRegion(sta, 3, reg);

      // Loop over hits on X1
      for (Clusters::iterator itX1 = clustersX1.begin(); 
	   itX1 != clustersX1.end(); itX1++) {

	Cluster* clX1 = *(itX1);

	double x1 = clX1->x();
	double z1 = clX1->z();

	// Find X1-neighbour
	bool hasNeighbourX1 = hasNeighbour(itX1, clustersX1);
	if (hasNeighbourX1) {
	  x1 += (*(itX1+1))->x();
	  z1 += (*(itX1+1))->z();
	  x1 /= 2.0;
	  z1 /= 2.0;
	}
	
	// Loop over hits on X2
	for (Clusters::iterator itX2 = clustersX2.begin(); 
	     itX2 != clustersX2.end(); itX2++) {

	  Cluster* clX2 = *(itX2);
	
	  double x2 = clX2->x();
	  double z2 = clX2->z();
	  
	  // Find X2-neighbour
	  bool hasNeighbourX2 = hasNeighbour(itX2, clustersX2);
	  if (hasNeighbourX2) {
	    x2 += (*(itX2+1))->x();
	    z2 += (*(itX2+1))->z();
	    x2 /= 2.0;
	    z2 /= 2.0;
	  }

	  double tx = (x2 - x1) / (z2 - z1);
	  double x2Pred = x1 * zRatio; // assume track points to the origin

	  // Search inside a window around predicted x2-position
	  if (x2 - x2Pred < -maxDx) continue;
	  if (x2 - x2Pred > +maxDx) break;

	  // Cut on slope
	  // TODO: use break!!!
	  if (std::abs(tx) > maxSlope) continue;

	  // Make tracklet!
          Tracklet xTracklet(clX1, clX2);

	  // Add neighbouring hits
	  if (hasNeighbourX1) xTracklet.addCluster( *(itX1 + 1) );
	  if (hasNeighbourX2) xTracklet.addCluster( *(itX2 + 1) );

	  // Set rough track parameters
	  xTracklet.setStation( sta );
	  xTracklet.setZ0( (z1 + z2) / 2.0 );
	  xTracklet.setX0( (x1 + x2) / 2.0 );
	  xTracklet.setTx( tx );
	 
	  // Cut on chi2nDoF 
	  unsigned int nDoF = xTracklet.nHits() - 2;
	  double chi2nDoF = 0.0;
	  for (unsigned int i = 0; i < xTracklet.nHits(); i++) 
	    chi2nDoF += xTracklet.chi2Hit( xTracklet.hit(i) );
	  chi2nDoF = nDoF > 0 ? chi2nDoF / double(nDoF) : -Automata::BigDouble;
	  xTracklet.setChi2nDoF( chi2nDoF );
	  if (chi2nDoF > maxChi2) continue;

	  // Elliptic cut on x1/(z1-zMag) vs tx
	  double ell = pow((x1 / (z1 - m_zMagnet)) / sigma, 2) + pow(tx / sigma, 2)
   	               -2.0 * rho * ((x1 / (z1 - m_zMagnet)) / sigma) * (tx / sigma);
	  if (ell > maxEllipse) continue;

	  // Store tracklet!
	  m_xTracklets[sta][reg].push_back( xTracklet );
	  nXTracklets++;

#ifdef DEBUG_AUTOMATA
	// Associate to MC
	//LHCb::MCParticle *part = mcParticle( xTracklet.lhcbIDs(), m_otlinker );
#endif

	  if (hasNeighbourX2) itX2++;
	}//hitX2
	
	if (hasNeighbourX1) itX1++;
      }//hitX1
    }//reg

#ifdef DEBUG_AUTOMATA
    plot(nXTracklets, "nXTrackletsPerSta", "Num. x-tracklets per station", 0.0, 6000.0, 200);
#endif
  }//sta

  if ( m_measureTime ) m_timer->stop( m_timeMakeXTracklets );

  return StatusCode::SUCCESS;
}


//=========================================================================
// Add stereo hits to X-tracklets
//=========================================================================
StatusCode AutomataTool::addStereoHits() 
{
  /* Constants */
  // Stereo angle
  const double dxdy = 0.087489; //rad
  // Search window for xv
  const double maxDistFromPred = 6.0; //mm
  // Max. dy/dz slope
  const double maxSlopeY = 0.5; //rad
  // Max. chi2
  const double maxChi2 = 300.0;

  if ( m_measureTime ) m_timer->start( m_timeAddStereo );

  // Loop over stations
  for (unsigned int sta = 0; sta < m_nSta; sta++) {
    unsigned int nTracklets = 0;

    // Loop over regions
    for (unsigned int reg = 0; reg < m_nReg; reg++) {
      // OT only...
      if (reg > 1) break;

      // Reference z-pos. of layers
      //double zLay1  = region(sta,1,reg)->z();
      //double zLay2  = region(sta,2,reg)->z();
      //double zRatio = zLay2 / zLay1;

      // U range
      PatRange rangeU = regionY(sta, 1, reg);
      rangeU *= regionTanT(sta, 1, reg);
      const double dUMin = fabs(rangeU.min()); 
      const double dUMax = fabs(rangeU.max()); 

      // V range
      PatRange rangeV = regionY(sta, 2, reg);
      rangeV *= regionTanT(sta, 2, reg);
      const double dVMin = fabs(rangeV.min()); 
      const double dVMax = fabs(rangeV.max()); 

      Clusters clustersU = clustersInRegion(sta, 1, reg);
      Clusters clustersV = clustersInRegion(sta, 2, reg);


      // Loop over X-tracklets
      for (Tracklet& xTracklet : m_xTracklets[sta][reg]) {
	Cluster** clX = xTracklet.clusters();


	// Loop over hits in u-layer
	for (Clusters::iterator itU = clustersU.begin(); 
	     itU != clustersU.end(); itU++) {

	  Cluster *clU = *(itU);
	  double xu = clU->x();
	  double zu = clU->z();

	  // Find U-neighbour
	  bool hasNeighbourU = hasNeighbour(itU, clustersU);
	  if (hasNeighbourU) {
	    xu += (*(itU+1))->x();
	    zu += (*(itU+1))->z();
	    xu /= 2.0;
	    zu /= 2.0;
	  }

	  // Hits must be inside sensitive area 
	  double xAtU = xTracklet.xAtZ( zu );
	  if (xu < xAtU - dUMin) continue; 
	  if (xu > xAtU + dUMax) break; 

	  double yu = (xAtU - xu) / (+dxdy); //(hitU.FwdHit()->hit()->dxDy());


	  // Loop over hits in v-layer
	  for (Clusters::iterator itV = clustersV.begin(); 
	       itV != clustersV.end(); itV++) {

	    Cluster *clV = *(itV);
	    double xv = clV->x();
	    double zv = clV->z();

	    // Find V-neighbour
	    bool hasNeighbourV = hasNeighbour(itV, clustersV);
	    if (hasNeighbourV) {
	      xv += (*(itV+1))->x();
	      zv += (*(itV+1))->z();
	      xv /= 2.0;
	      zv /= 2.0;
	    }

	    // Hits must be inside sensitive area 
	    double xAtV = xTracklet.xAtZ( zv );
	    if (xv < xAtV - dVMin) continue; 
	    if (xv > xAtV + dVMax) break; 

	    // Cut on hit - track position at v-layer
	    double yAtV = yu * (zv/zu); // assume track points to the origin
	    double yv = (xAtV - xv) / (-dxdy); 
	    double dxvFromPred = xv + (-dxdy) * yAtV - xAtV; // update hit pos. 
	    if (std::abs( dxvFromPred ) > maxDistFromPred) continue; 
	    
	    // Cut on dy/dz slope
	    double ty = (yu/zu + yv/zv) / 2.0;
	    if (std::abs(ty) > maxSlopeY) continue;

	    // We can make a stub!
	    Tracklet stub( clX[0], clU, clV, clX[1] );
	    for (unsigned int i = 2; i < xTracklet.nClusters(); i++) 
	      stub.addCluster( clX[i] ); 

	    // Add neighbours
	    if (hasNeighbourU) stub.addCluster( *(itU + 1) );
	    if (hasNeighbourV) stub.addCluster( *(itV + 1) );

	    // Set rough track parameters
	    stub.setStation( sta );
	    stub.setZ0( xTracklet.z0() );
	    stub.setX0( xTracklet.x0() );
	    stub.setTx( xTracklet.tx() );
	    stub.setTy( ty );
	    stub.setY0( (yu + yv) / 2.0 ); // NOTE: if add neighbours param. have to be updated!!!
	    //stub.setY0( yu + ty*(zMid - zu) );
	    
	    // Cut on chi2nDoF
	    double chi2nDoF = 0.0;
	    for (unsigned int i = 0; i < stub.nHits(); i++)
	      chi2nDoF += stub.chi2Hit( stub.hit(i) );
	    chi2nDoF /= double(stub.nHits() - 3);
	    stub.setChi2nDoF( chi2nDoF );
	    if (chi2nDoF > maxChi2) continue;

	    // Store it!
	    m_trackStubs[sta][reg].push_back( stub );
	    nTracklets++;

#ifdef DEBUG_AUTOMATA
	    // Associate to MC
	    LHCb::MCParticle *part = mcParticle( stub.lhcbIDs(), m_otlinker );

	    // Compute pitch residual
	    double pitchResSum = 0.0;
	    for (unsigned int i = 0; i < stub.nClusters(); i++) {
	      Cluster* cl = stub.cluster(i);
	      double res = stub.pitchResidual(cl);
	      pitchResSum += res;
	    }

	    // Look at good tracklets!
	    if (part != nullptr) {
	      plot(stub.nHits(), "nHitsMatched", "nHits on stub", 2.0, +14.0, 12);
	      plot(xu - xv, "dxStereoMatched", "xu-xv for tracklets", -450.0, +450.0, 100);
	      plot(yu - yv, "dyStereoMatched", "yu-yv for MC particles", -500.0, +500.0, 100);
	      plot(xAtU - xu, "dxuPredMatched", "xAtU - xu for tracklets", -300.0, +300.0, 100);
	      plot(xAtV - xv, "dxvPredMatched", "xAtV - xv for tracklets", -300.0, +300.0, 100);
	      plot(chi2nDoF, "chi2nDoFMatched", "Raw chi2/nDoF for MC particles", 0.0, +250.0, 100);
	      plot(dxvFromPred, "dxvFromPredMatched", "xv(y) - xAtV for MC particles", -20.0, +20.0, 100);
	      plot(pitchResSum, "pitchResidualSumMatched", "Pitch residuals for MC part", -10.0, +3.0, 100);
	      plot2D(dxvFromPred, m_nHitsOT, "dvFromPredNHitsCorrMatched", -10.0, +10.0, 0.0, m_maxOTHits, 100, 100);
	      plot2D(xAtU - xu, m_nHitsOT, "dxuNHitsCorrMatched", -300.0, +300.0, 0.0, m_maxOTHits, 100, 100);
	    }

	    // Here they are our beloved ghosts!
	    else {
	      plot(stub.nHits(), "nHitsGhosts", "nHits on stub", 2.0, +14.0, 12);
	      plot(xu - xv, "dxStereoGhosts", "xu-xv for tracklets", -450.0, +450.0, 100);
	      plot(yu - yv, "dyStereoGhosts", "yu-yv for MC particles", -500.0, +500.0, 100);
	      plot(xAtU - xu, "dxuPredGhosts", "xAtU - xu for tracklets", -300.0, +300.0, 100);
	      plot(xAtV - xv, "dxvPredGhosts", "xAtV - xv for tracklets", -300.0, +300.0, 100);
	      plot(chi2nDoF, "chi2nDoFGhosts", "Raw chi2/nDoF for MC particles", 0.0, +250.0, 100);
	      plot(dxvFromPred, "dxvFromPredGhosts", "xv(y) - xAtV for MC particles", -20.0, +20.0, 100);
	      plot(pitchResSum, "pitchResidualSumGhosts", "Pitch residuals for MC part", -10.0, +3.0, 100);
	      plot2D(dxvFromPred, m_nHitsOT, "dvFromPredNHitsCorrGhosts", -10.0, +10.0, 0.0, m_maxOTHits, 100, 100);
	      plot2D(xAtU - xu, m_nHitsOT, "dxuNHitsCorrGhosts", -300.0, +300.0, 0.0, m_maxOTHits, 100, 100);
	    }
#endif

	    if (hasNeighbourV) itV++;
	  }//hitV
	  
	  if (hasNeighbourU) itU++;
	}//hitU
	    
      }//xTracklet
    }//reg

#ifdef DEBUG_AUTOMATA
    plot(nTracklets, "nTrackletsPerSta", "#Tracklets per stations", 0.0, 50000.0, 200);
#endif
  }//sta
  
  if ( m_measureTime ) m_timer->stop( m_timeAddStereo );

  return StatusCode::SUCCESS;
}


//=========================================================================
// Fit stubs 
//=========================================================================
StatusCode AutomataTool::fitStubs() 
{
  /* Constants */
  // Min. num. hits on tracklet
  unsigned int minHits = 4; // 4
  // Min. num. layers on tracklet
  unsigned int minPlanes = 3; // 3
  // Max. chi2 of worst hit
  const double worstChi2Max = 200.0; // 200.0;
  // Max. chi2/nDoF after fit
  const double maxChi2 = 100.0; //100.0;
  // Max. num. of outliers
  const unsigned int maxOutliers = 3; // 3

  Tracklets goodStubs;
  goodStubs.clear();
  goodStubs.reserve(256);

  if ( m_measureTime ) m_timer->start( m_timeFitStubs );

  // Loop over stations
  for (unsigned int sta = 0; sta < m_nSta; sta++) {
    // Loop over regions
    for (unsigned int reg = 0; reg < m_nReg; reg++) {
      // OT only...
      if (reg > 1) continue;

      int nFitted = 0; 
      goodStubs.clear();

      // Loop over tracklets
      for (Tracklet& stub : m_trackStubs[sta][reg]) {

#ifdef DEBUG_AUTOMATA
	// Associate to MC
	LHCb::MCParticle *part = mcParticle( stub.lhcbIDs(), m_otlinker );

	double txOld = stub.tx();
	double tyOld = stub.ty();
	double x0Old = stub.x0();
	double y0Old = stub.y0();
#endif

	// Set FwdTrack
	stub.setFwdTrack(m_zReference, m_dRatio, m_initialArrow);
	PatSeedTrack *track = stub.fwdTrack();

	// Fit!
	track->updateHits(); 
	bool fitOk = stub.fit(m_seedTool, m_initialArrow);
	
	// Reject absurd tracklets
	if (!fitOk) {
	  //info() << "Fit to tracklet not converged!!!" << endmsg;
	  resetHits( stub );
	  continue;
	}

	// Remove outliers
	bool refitOk = true;
	PatFwdHits::iterator worst = track->coordBegin();
	double worstChi2 = 0; 
	unsigned int nOutliers = 0;

	while ((track->nCoords() >= minHits) && (track->nPlanes() >= minPlanes)) {
	  // Remove outlier...
	  worst = stub.worstHit();
	  worstChi2 = track->chi2Hit( *worst ); 

	  if (worstChi2 < worstChi2Max) break; 
	  track->removeCoord( worst );
	  nOutliers++;

	  // Refit tracklet...
	  refitOk = stub.fit(m_seedTool, m_initialArrow);
	  if (!refitOk) break;
	}
	  
	// Reject if not converged or too few hits/planes
	if (!refitOk ||
	    (track->nCoords() < minHits) || 
	    (track->nPlanes() < minPlanes) ||
	    nOutliers > maxOutliers) {
	  resetHits( stub );
	  continue;
	}

        // Cut on chi2/nDoF after fit	
	double chi2nDoF = stub.chi2nDoF();
	if (chi2nDoF > maxChi2) { 
	  resetHits( stub ); 
	  continue; 
	}

	// Reset hits (next tracklet can have hits in common...)
	resetHits( stub ); 

	// Set hits on tracklet after outlier removal
	stub.setHits(track->nCoords(), track->coords());

	// Stub is good, save it!
	goodStubs.push_back( stub );
	nFitted++;

#ifdef DEBUG_AUTOMATA
	unsigned int nHitsX = 0, nHitsStereo = 0;
	for (PatFwdHit *hit : track->coords()) {
	  if(hit->hit()->isX()) nHitsX++;
	  else nHitsStereo++;
	}

	plot(nOutliers, "nOutliers", "Number of refits", 0.0, +10.0, 10);
	plot(nHitsX, "nHitsXOnStub", "Number of X hits on tracklet", 0.0, +10.0, 10);
	plot(nHitsStereo, "nHitsStereoOnStub", "Number of stereo hits on tracklet", 0.0, +10.0, 10);
	plot(track->nPlanes(), "nPlanesOnStub", "Number of planes on tracklet", 0.0, +6.0, 6);
	plot(track->nCoords(), "nHitsOnStubFit", "#Hits on stub after fit", 0.0, 20.0, 20);

	plot(chi2nDoF, "chi2ndof_final", "fit chi2/ndof final for tracklets", 0.0, +50.0, 100);
	plot(stub.tx(), "txFitted", "tx for tracklets after fit", -2.0, +2.0, 100);
	plot(stub.ty(), "tyFitted", "ty for tracklets after fit", -0.5, +0.5, 100);
	plot(track->xAtZ(0.0), "xAtZ0_final", "Stub X-pos. at Z=0 after fit", -300.0, +300.0, 100);

	// Matched!
	if (part != nullptr) {
	  // True values
	  double tyMC = part->momentum().py() / part->momentum().pz();

	  plot(nOutliers, "nOutliersMatched", "Number of refits", 0.0, +10.0, 10);
	  plot(nHitsX, "nHitsXOnStubMatched", "Number of X hits on tracklet", 0.0, +10.0, 10);
	  plot(nHitsStereo, "nHitsStereoOnStubMatched", "Number of stereo hits on tracklet", 0.0, +10.0, 10);
	  plot(track->nCoords(), "nHitsOnStubFitMatched", "#Hits on stub after fit", 0.0, 20.0, 20);
	  plot(chi2nDoF, "chi2nDoFFitMatched", "Raw chi2/nDoF for MC part", 0.0, maxChi2, 100);
	  plot(worstChi2, "worstChi2FitMatched", "Worst chi2 for MC part", 0.0, +200.0, 150);
	  plot(track->nPlanes(), "nPlanesOnStubFitMatched", "Number of planes on tracklet", 0.0, +6.0, 6);
	  plot(stub.ty() - tyMC, "dtyTrueMatched", "tyPred - tyMC for tracklets", -0.01, +0.01, 100);
	  plot2D(chi2nDoF, stub.tx(), "chi2txCorrelFitMatched", "chi2/nDoF vs tx for tracklets", 0.0, +50.0, -1.5, +1.5, 100, 100);
	  plot(stub.tx() - txOld, "dtxFittedMatched", "dtx for tracklets after fit", -0.1, +0.1, 100);
	  plot(stub.ty() - tyOld, "dtyFittedMatched", "dtx for tracklets after fit", -0.05, +0.05, 100);
	  plot(stub.x0() - x0Old, "dx0FittedMatched", "dx0 for tracklets after fit", -30.0, +30.0, 100);
	  plot(stub.y0() - y0Old, "dy0FittedMatched", "dy0 for tracklets after fit", -60.0, +60.0, 100);
	}

	// It's a fucking ghost!
	else {
	  plot(nOutliers, "nOutliersGhosts", "Number of refits", 0.0, +10.0, 10);
	  plot(nHitsX, "nHitsXOnStubGhosts", "Number of X hits on tracklet", 0.0, +10.0, 10);
	  plot(nHitsStereo, "nHitsStereoOnStubGhosts", "Number of stereo hits on tracklet", 0.0, +10.0, 10);
	  plot(track->nCoords(), "nHitsOnStubFitGhosts", "#Hits on stub after fit", 0.0, 20.0, 20);
	  plot(chi2nDoF, "chi2nDoFFitGhosts", "Raw chi2/nDoF for MC part", 0.0, maxChi2, 100);
	  plot(worstChi2, "worstChi2FitGhosts", "Worst chi2 for MC part", 0.0, +200.0, 150);
	  plot(track->nPlanes(), "nPlanesOnStubFitGhosts", "Number of planes on tracklet", 0.0, +6.0, 6);
	  plot2D(chi2nDoF, stub.tx(), "chi2txCorrelFitGhosts", "chi2/nDoF vs tx for tracklets", 0.0, +50.0, -1.5, +1.5, 100, 100);
	  plot(stub.tx() - txOld, "dtxFittedGhosts", "dtx for tracklets after fit", -0.1, +0.1, 100);
	  plot(stub.ty() - tyOld, "dtyFittedGhosts", "dtx for tracklets after fit", -0.05, +0.05, 100);
	  plot(stub.x0() - x0Old, "dx0FittedGhosts", "dx0 for tracklets after fit", -30.0, +30.0, 100);
	  plot(stub.y0() - y0Old, "dy0FittedGhosts", "dy0 for tracklets after fit", -60.0, +60.0, 100);
	}
#endif
      }//stubs

      // Store only good stubs
      m_trackStubs[sta][reg].clear();
      m_trackStubs[sta][reg].insert(m_trackStubs[sta][reg].begin(), goodStubs.begin(), goodStubs.end());
      
#ifdef DEBUG_AUTOMATA
      plot(nFitted, "nFitted", "#Tracklets per region after fit", 0.0, 1000.0, 100);
#endif
    }//reg
  }//sta
  

  if ( m_measureTime ) m_timer->stop( m_timeFitStubs );
  
  return StatusCode::SUCCESS;
}


//=========================================================================
// Find neighbours
//=========================================================================
StatusCode AutomataTool::findNeighbours() 
{
  /* Constants */
  // Max. neighbours per tracklet
  unsigned int maxNbNeighbours = 4; //4
  // Search window in x
  const double dxPredMax = 30.0; //30.0 mm
  // Max. kink in x
  const double dtxMax = 0.05; //0.05 rad
  // Search window in y
  const double dyPredMax = 60; //60.0 mm
  // Max. kink in y
  const double dtyMax = 0.01; //0.01 rad

  if ( m_measureTime ) m_timer->start( m_timeFindNeighbours );

  //PatSeedTrack *track;

  // Loop on stations
  for (int sta = (m_nSta - 2); sta >= 0; --sta) {
    unsigned int nextSta = sta + 1;

    // Loop on regions
    for (unsigned int reg = 0; reg < m_nReg; reg++) {
      // only OT hits...
      if (reg > 1) continue;

      // Middle Z-pos. @next-station
      double zNext = 0.5 * (region(nextSta,0,reg)->z() 
                  	  + region(nextSta,3,reg)->z());

      // Sort tracklets by x(zNext)
      std::sort( m_trackStubs[sta][reg].begin(), m_trackStubs[sta][reg].end(), 
          [zNext] (const Tracklet& lhs, const Tracklet& rhs)
          { return lhs.x(zNext) < rhs.x(zNext); } );

      std::sort( m_trackStubs[nextSta][reg].begin(), m_trackStubs[nextSta][reg].end(),
          [zNext] (const Tracklet& lhs, const Tracklet& rhs)
          { return lhs.x(zNext) < rhs.x(zNext); } );

      // Find first stub in next station compatible with first stub
      auto beg = std::lower_bound (
          m_trackStubs[nextSta][reg].begin(), m_trackStubs[nextSta][reg].end(),
          m_trackStubs[sta][reg].front().x(zNext) - dxPredMax,
          [zNext] (const Tracklet& tl, double val) 
          { return tl.x(zNext) < val; });


      // Loop over station stubs
      for (Tracklet& stub : m_trackStubs[sta][reg]) {
	double xStub = stub.x(zNext);
	double yStub = stub.y(zNext);
	double txStub = stub.tx();
	double tyStub = stub.ty();

#ifdef DEBUG_AUTOMATA
	// Associate to MC
	LHCb::MCParticle *part = mcParticle( stub.lhcbIDs(), m_otlinker );
#endif

	// Find first compatible stub in station 
	while ((m_trackStubs[nextSta][reg].end() != beg) &&
	       (beg->x(zNext) < xStub - dxPredMax)) ++beg;


	// Loop over next-station stubs
	for (auto itN = beg; (m_trackStubs[nextSta][reg].end() != itN) &&
 	     (itN->x(zNext) < xStub + dxPredMax); ++itN) {

	  // Stop search, enough neighbours!
	  if (stub.nNeighbours() >= maxNbNeighbours) break;

	  Tracklet &next = (*itN);

	  // Open search window around pred y
	  double yNext = next.y(zNext);
	  if (std::abs(yNext - yStub) > dyPredMax) continue; 

	  // Require small kink angles between them
	  double txNext = next.tx(); 
	  if (std::abs(txNext - txStub) > dtxMax) continue; 

	  double tyNext = next.ty();  
	  if (std::abs(tyNext - tyStub) > dtyMax) continue; 

	  // Add neighbour
	  stub.addNeighbour( &next );

#ifdef DEBUG_AUTOMATA
	  double xNext = next.x(zNext);
	  plot(xNext - xStub, "dxNb", "xNext - xStub for tracklets", -80, +80, 100);
	  plot(yNext - yStub, "dyNb", "yNext - yStub for tracklets", -70, +70, 100);
	  plot(txNext - txStub, "dtxNb", "txNext - txStub for tracklets", -0.06, +0.06, 100);
	  plot(tyNext - tyStub, "dtyNb", "tyNext - tyStub for tracklets", -0.01, +0.01, 100);

	  // Match!
	  if ((part != nullptr)) {
	    plot(xNext - xStub, "dxNbMatched", "xNext - xStub for tracklets", -100, +100, 100);
	    plot(yNext - yStub, "dyNbMatched", "yNext - yStub for tracklets", -100, +100, 100);
	    plot(txNext - txStub, "dtxNbMatched", "txNext - txStub for tracklets", -0.1, +0.1, 100);
	    plot(tyNext - tyStub, "dtyNbMatched", "tyNext - tyStub for tracklets", -0.01, +0.01, 100);
	    plot2D(xNext - xStub, m_nHitsOT, "dxNbNHitsCorrMatched", -100.0, +100.0, 0.0, m_maxOTHits, 100, 100);
	  }

	  // Ghost!
	  else {
	    plot(txNext - txStub, "dtxNbGhosts", "txNext - txStub for tracklets", -0.1, +0.1, 100);
	    plot(tyNext - tyStub, "dtyNbGhosts", "tyNext - tyStub for tracklets", -0.01, +0.01, 100);
	    plot(xNext - xStub, "dxNbGhosts", "xNext - xStub for tracklets", -100, +100, 100);
	    plot(yNext - yStub, "dyNbGhosts", "yNext - yStub for tracklets", -100, +100, 100);
	    plot2D(xNext - xStub, m_nHitsOT, "dxNbNHitsCorrGhosts", -100.0, +100.0, 0.0, m_maxOTHits, 100, 100);
	  }
#endif

	}//next

#ifdef DEBUG_AUTOMATA
	plot(stub.nNeighbours(), "nNb", "Number of neighbours per stub", 0.0, +10.0, 11);
	if (part != nullptr) plot(stub.nNeighbours(), "nNbMatched", "Number of neighbours per stub", 0.0, +10.0, 11);
	else plot(stub.nNeighbours(), "nNbGhosts", "Number of neighbours per stub", 0.0, +10.0, 11);
#endif
      }//stub
    }//reg
  }//sta

  if ( m_measureTime ) m_timer->stop( m_timeFindNeighbours );

  return StatusCode::SUCCESS;
}


//=========================================================================
// Evolve tracklets
//=========================================================================
StatusCode AutomataTool::evolve()
{
  /* Constants */
  // Number of evolution steps
  const unsigned int maxSteps = 2;

  if ( m_measureTime ) m_timer->start( m_timeEvolve );

  // Evolve tracklets
  for (unsigned int nStep = 0; nStep < maxSteps; nStep++) {
    evolutionStep();
  }

  if ( m_measureTime ) m_timer->stop( m_timeEvolve );

  return StatusCode::SUCCESS;
}


//=========================================================================
// Do the evolution step 
//=========================================================================
 StatusCode AutomataTool::evolutionStep()
{ // NOTE:
  // Loop over neighbours and update counter of current tracklet
  // THREAD SAFE? you don't know which station is being processed!!! 
  // should use a "hasToBeIncremented" flag and than update all counters
  
  // Loop on stations
  for (unsigned int sta = 0; sta < (m_nSta - 1); sta++) {
    // Loop on regions
    for (unsigned int reg = 0; reg < m_nReg; reg++) {
      // only OT hits...
      if (reg > 1) continue;

      // Loop on tracklets
      for (Tracklet& stub : m_trackStubs[sta][reg]) {
	unsigned int counter = stub.counter();

	// Loop over neighbours
	for (unsigned int i = 0; i < stub.nNeighbours(); i++) {
	  unsigned int counterNb = stub.neighbour(i)->counter();
	  if (counterNb >= counter) { stub.incrementCounter(); break; }
	}//nb
      }//stubs
    }//reg
  }//sta

  return StatusCode::SUCCESS;
}


//=========================================================================
// Link tracklets and make candidate tracks
//=========================================================================
StatusCode AutomataTool::linkTracklets()
{
  if ( m_measureTime ) m_timer->start( m_timeLink );

  // Loop on stations
  for (unsigned int sta = 0; sta < (m_nSta - 1); sta++) {
    // Loop on regions
    for (unsigned int reg = 0; reg < m_nReg; reg++) {
      // only OT hits...
      if (reg > 1) continue;

      // Loop on tracklets in first station (aka layer in CA language)
      for (Tracklet& stub0 : m_trackStubs[sta][reg]) {
	unsigned int counter0 = stub0.counter();
	unsigned int numNb0 = stub0.nNeighbours(); 

	// It's a dead end, make a track from one stub!
	if (numNb0 == 0) {
	  CATrack track( &stub0 );
	  m_candTracks[0][reg].push_back( track );
	}

	// Loop on its neighbours
	for (unsigned int iNb0 = 0; iNb0 < numNb0; iNb0++) {
	  Tracklet *stub1 = stub0.neighbour(iNb0);
	  unsigned int counter1 = stub1->counter();
	  unsigned int numNb1 = stub1->nNeighbours();

	  // It's a dead end, make a track from two stubs!
	  if (numNb1 == 0) {
	    CATrack track( &stub0 );
	    track.addTracklet( stub1 ); 
	    m_candTracks[1][reg].push_back( track );
	  }

	  // Cannot be linked
	  if (counter1 != counter0 - 1) continue;

	  // Loop on its neighbours
	  for (unsigned int iNb1 = 0; iNb1 < numNb1; iNb1++) {
	    Tracklet *stub2 = stub1->neighbour(iNb1);
	    
	    // Cannot be linked
	    if (stub2->counter() != counter1 - 1) continue;

	    // All stubs collected, make a candidate!
	    CATrack track( &stub0 );
	    track.addTracklet( stub1 ); 
	    track.addTracklet( stub2 ); 
	    m_candTracks[2][reg].push_back( track );
	  }//nb1

	}//nb0
      }//stub0
    }//reg
  }//sta

#ifdef DEBUG_AUTOMATA
  for (unsigned int len = 0; len < m_nSta; len++) {  
    for (unsigned int reg = 0; reg < m_nReg; reg++) {  
      if (reg > 1) continue;

      for (CATrack &cand :  m_candTracks[len][reg]) {
	plot(cand.nHits(), "nHitsOnCand", "Number of hits on candidate tracks", 4.0, 30.0, 26);
	plot(cand.nTracklets(), "nTrackletsOnCand", "Number of tracklets on candidate tracks", 0.0, +5.0, 5);
      }//cand

    }//reg
  }//len
#endif

  if ( m_measureTime ) m_timer->stop( m_timeLink );

  return StatusCode::SUCCESS;
}


//=========================================================================
// Fit long tracks and set hits as used
// - NOTE:
// - most of exec. time taken by this function (fitTrack)
//=========================================================================
StatusCode AutomataTool::selectLongTracks()
{
  /* Constants */
  // Max. chi2 contribution of an hit
  const double maxHitChi2 = 6.0; //6.0
  // Min. number of layers a tracks should have
  const unsigned int minNbPlanes = 10; //10
  // Max. number of outliers
  const unsigned int maxNbOutliers = 15; //15
  // Max. track chi2
  const double maxTrackChi2 = 6.0; //6.0

  if ( m_measureTime ) m_timer->start( m_timeSelectLong );

  PatSeedTrack fwdTrack; 

  // Loop over regions
  for (unsigned int reg = 0; reg < m_nReg; reg++) {
    // OT only...
    if (reg > 1) continue;
   
    // Loop over long tracks
    for (CATrack &cand : m_candTracks[m_nSta - 1][reg]) {

#ifdef DEBUG_AUTOMATA
      // Associate to MC
      LHCb::MCParticle *part = mcParticle( cand.lhcbIDs(), m_otlinker );
#endif
     
      // Raw chi2/nDoF
      // Apply a cut on chi2 before fitting?? (chi2nDoF < 80?)
      double chi2nDoF_Raw = cand.tracklet(0)->chi2nDoF() + 
	                    cand.tracklet(1)->chi2nDoF() +
			    cand.tracklet(2)->chi2nDoF();
      chi2nDoF_Raw /= 3.0; 

      // Fit track!
      fwdTrack = *(cand.tracklet(0)->fwdTrack());
      for (unsigned int n = 1; n < cand.nTracklets(); n++) { 
	Tracklet* stub = cand.tracklet(n);
	for (unsigned int i = 0; i < stub->nHits(); i++) 
	  fwdTrack.addCoord( stub->hit(i) );
      }

      bool fitOk = m_seedTool->fitTrack( fwdTrack, maxHitChi2, minNbPlanes, false, false );
      if (!fitOk) {
        //info() << "Fit of long track failed!!!" << endmsg;
	resetHits( cand );
        continue;
      }

      // Cut on chi2/nDoF
      double chi2nDoF = 0.0;
      for (const PatFwdHit* hit : fwdTrack.coords()) 
	chi2nDoF += fwdTrack.chi2Hit(hit);
      chi2nDoF /= double(fwdTrack.nCoords() - 5);
      if (chi2nDoF > maxTrackChi2) {
	resetHits( cand );
	continue;
      }

      // Num. of outliers
      unsigned int nOutliers = cand.nHits() - fwdTrack.nCoords();
      if (nOutliers > maxNbOutliers) {
	resetHits( cand );
	continue;
      }

      // Track quality
      const double w0 = 0.0930419521624; 
      const double w1 = -0.208728666352; 
      const double w2 = -0.124468775361; 
      const double w3 = 0.17558325965; 
      const double w4 = -0.167157852572; 
      double quality = w0 + 
  	               w1 * chi2nDoF + 
 		       w2 * nOutliers + 
 		       w3 * fwdTrack.nCoords() + 
 		       w4 * fwdTrack.nPlanes();

      // Reset hits for next candidate
      resetHits( cand );

      // Update hits and parameters
      cand.setHits( fwdTrack.nCoords(), fwdTrack.coords() );
      cand.setX0( fwdTrack.xAtZ(m_zReference) );
      cand.setY0( fwdTrack.yAtZ(m_zReference) );
      cand.setZ0( m_zReference );
      cand.setTx( fwdTrack.xSlope(m_zReference) );
      cand.setTy( fwdTrack.ySlope(m_zReference) );
      cand.setChi2nDoF( chi2nDoF );
      cand.setQuality( quality );
      cand.setIsValid( true );

      // It's good! Store it!
      //if (part != nullptr) 
      //m_outputTracks[reg].push_back( cand );

#ifdef DEBUG_AUTOMATA
      // Matching!
      if (part != nullptr) {
	// Dump signal variables for MVA analysis
	fprintf(m_mvaSignalLongFile, "%f %i %i %i \n", 
	    chi2nDoF,  nOutliers, fwdTrack.nCoords(), fwdTrack.nPlanes() ); 

	// True quantities
	double p_MC = part->p() / 1000.0;

	plot(chi2nDoF, "chi2nDoFLongMatched", "fit chi2/ndof for long track", 0.0, +10.0, 100);
	plot(chi2nDoF_Raw, "chi2nDoFLongRawMatched", "fit chi2/ndof for long track", 0.0, +100.0, 100);
	plot(fwdTrack.nCoords(), "nHitsLongMatched", "Number of hits on long tracks", 6.0, 40.0, 34);
	plot(fwdTrack.nPlanes(), "nPlanesLongMatched", "Number of planes on long tracks", 9.0, 14.0, 5);
	plot(cand.tracklet(0)->chi2nDoF(), "chi2nDoFLong0Matched", "fit chi2/ndof for long track", 0.0, +10.0, 100);
	plot(cand.tracklet(1)->chi2nDoF(), "chi2nDoFLong1Matched", "fit chi2/ndof for long track", 0.0, +10.0, 100);
	plot(cand.tracklet(2)->chi2nDoF(), "chi2nDoFLong2Matched", "fit chi2/ndof for long track", 0.0, +10.0, 100);
	plot2D(chi2nDoF, fwdTrack.nPlanes(), "chi2nPlanesMatched", "chi2(0) vs nPlanes for long tracks", 0.0, +10.0, 9.0, +13.0, 100, 4);
	plot(quality, "qualityLongMatched", "Fisher discriminant for MC part", -2.0, +2.0, 100);
	plot(p_MC, "mcMomentumLongMatched", "MCParticle momentum for long track", 0.0, +50.0, 100);
	plot(nOutliers, "nOutliersLongMatched", "Number of outliers on long tracks", 0.0, 20.0, 20);
	plot2D(chi2nDoF, nOutliers, "nOutliersChi2CorrelLongMatched", "nOutliers vs chi2nDoF for long tracks", 0.0, 12.0, 0.0, 15.0, 100, 15);
      }

      // Ghosts...
      else {
	// Dump ghosts variables for MVA analysis
	fprintf(m_mvaGhostsLongFile, "%f %i %i %i \n", 
	    chi2nDoF,  nOutliers, fwdTrack.nCoords(), fwdTrack.nPlanes() ); 
	
	plot(chi2nDoF, "chi2nDoFLongGhosts", "fit chi2/ndof for long track", 0.0, +10.0, 100);
	plot(chi2nDoF_Raw, "chi2nDoFLongRawGhosts", "fit chi2/ndof for long track", 0.0, +100.0, 100);
	plot(fwdTrack.nCoords(), "nHitsLongGhosts", "Number of hits on long tracks", 6.0, 40.0, 34);
	plot(fwdTrack.nPlanes(), "nPlanesLongGhosts", "Number of planes on long tracks", 9.0, 14.0, 5);
	plot(cand.tracklet(0)->chi2nDoF(), "chi2nDoFLong0Ghosts", "fit chi2/ndof for long track", 0.0, +10.0, 100);
	plot(cand.tracklet(1)->chi2nDoF(), "chi2nDoFLong1Ghosts", "fit chi2/ndof for long track", 0.0, +10.0, 100);
	plot(cand.tracklet(2)->chi2nDoF(), "chi2nDoFLong2Ghosts", "fit chi2/ndof for long track", 0.0, +10.0, 100);
	plot2D(chi2nDoF, fwdTrack.nPlanes(), "chi2nPlanesGhosts", "chi2(0) vs nPlanes for long tracks", 0.0, +10.0, 9.0, +13.0, 100, 4);
	plot(quality, "qualityLongGhosts", "Fisher discriminant for MC part", -2.0, +2.0, 100);
	plot(nOutliers, "nOutliersLongGhosts", "Number of outliers on long tracks", 0.0, 20.0, 20);
	plot2D(chi2nDoF, nOutliers, "nOutliersChi2CorrelLongGhosts", "nOutliers vs chi2nDoF for long tracks", 0.0, 12.0, 0.0, 15.0, 100, 15);
      }
#endif
 
    }//tracks
  }//reg

  if ( m_measureTime ) m_timer->stop( m_timeSelectLong );

  return StatusCode::SUCCESS;
}


//================================================================================
// Kill clone tracks
//================================================================================
StatusCode AutomataTool::killLongClones() 
{
  /* Constants */
  // Max. nUsed/nHits on track
  const double maxFracUsed = 0.30; //0.30
  // Min. value for track quality 
  const double minQuality = -0.5; //-0.5; 

  if ( m_measureTime ) m_timer->start( m_timeKillLongClones );

  // Loop over regions
  for (unsigned int reg = 0; reg < m_nReg; reg++) {
    // OT only...
    if (reg > 1) continue;
    
    // Sort tracks by decreasing nHits (if equal sort by chi2nDoF)
    std::sort( m_candTracks[m_nSta - 1][reg].begin(), m_candTracks[m_nSta - 1][reg].end(), 
	[] (const CATrack& first, const CATrack& second) {
	if (first.nHits() > second.nHits()) return true;
	if (first.nHits() < second.nHits()) return false;
	return first.chi2nDoF() < second.chi2nDoF();
	} );

    // Loop over tracks
    for (CATrack &cand : m_candTracks[m_nSta - 1][reg]) {

#ifdef DEBUG_AUTOMATA
      // Associate to MC
      LHCb::MCParticle *part = mcParticle( cand.lhcbIDs(), m_otlinker );
#endif

      // Reject bad candidates
      if (!cand.isValid()) continue;

      // Kill clones
      if (cand.fracUsedHits() > maxFracUsed) continue;

      // Cut on track quality
      if (cand.quality() < minQuality) continue;
 
      // Tag tracklets as used
      for (unsigned int i = 0; i < cand.nTracklets(); i++) {
	cand.tracklet(i)->setIsUsed( true );
      }

      // Tag hits as used
      for (unsigned int i = 0; i < cand.nHits(); i++) {
	cand.hit(i)->setIsUsed( true );
      }
      
      // It's good! Store it!
      m_outputTracks[reg].push_back( cand );

#ifdef DEBUG_AUTOMATA
      // Match to MCParticle
      if (part != nullptr) {
	plot(cand.chi2nDoF(), "chi2nDoFLongFinalMatched", "fit chi2/ndof for long track", 0.0, +10.0, 100);
	plot(cand.nHits(), "nHitsLongFinalMatched", "Number of hits on long tracks", 6.0, 40.0, 34);
	plot(cand.quality(), "qualityLongFinalMatched", "Fisher discriminant for MC part", -2.0, +2.0, 100);
      }

      // It's ghost...
      else {
	plot(cand.chi2nDoF(), "chi2nDoFLongFinalGhosts", "fit chi2/ndof for long track", 0.0, +10.0, 100);
	plot(cand.nHits(), "nHitsLongFinalGhosts", "Number of hits on long tracks", 6.0, 40.0, 34);
	plot(cand.quality(), "qualityLongFinalGhosts", "Fisher discriminant for MC part", -2.0, +2.0, 100);
      }
#endif

    }//tracks
 
  }//reg

  if ( m_measureTime ) m_timer->stop( m_timeKillLongClones );

  return StatusCode::SUCCESS;
}


//================================================================================
// Extend short tracks by adding missing layers, fit and set hits as used
//================================================================================
StatusCode AutomataTool::selectShortTracks() 
{
  /* Constants */
  // Max. hit chi2 
  const double maxHitChi2 = 5.0; //5.0
  // Min. number of layers on track
  const unsigned int minNbPlanes = 7; //7
  // Max. number of outliers
  const unsigned int maxNbOutliers = 2; //2 
  // Max. track chi2
  const double maxTrackChi2 = 5.0; //5.0 
  // Max. nUsed/nHits on track
  const double maxFracUsed = 0.30; //0.30

  if ( m_measureTime ) m_timer->start( m_timeSelectShort );
  
  PatSeedTrack fwdTrack;

  // Loop on regions
  for (unsigned int reg = 0; reg < m_nReg; reg++) {
    // only OT hits...
    if (reg > 1) continue;

    // Loop on short candidates
    for (CATrack &cand : m_candTracks[m_nSta - 2][reg]) {

#ifdef DEBUG_AUTOMATA
      // Associate to MC
      LHCb::MCParticle *part = mcParticle( cand.lhcbIDs(), m_otlinker );
#endif

      // Skip track candidates with too many used hits
      double fracUsed = cand.fracUsedHits();
      if (fracUsed > maxFracUsed) {
        resetHits( cand );
        continue;
      }

      // Fit track
      fwdTrack = *(cand.tracklet(0)->fwdTrack());
      for (unsigned int n = 1; n < cand.nTracklets(); n++) { 
	Tracklet* stub = cand.tracklet(n);
	for (unsigned int i = 0; i < stub->nHits(); i++) 
	  fwdTrack.addCoord( stub->hit(i) );
      }

      bool okFit = m_seedTool->fitTrack( fwdTrack, maxHitChi2, minNbPlanes, false, false );
      if(!okFit) {
	//info() << "Fit of short track failed!!!" << endmsg;
	resetHits( cand );
	continue;
      }

      // Cut on chi2/nDOF
      double chi2nDoF = 0.0;
      for (const PatFwdHit* hit : fwdTrack.coords()) 
	chi2nDoF += fwdTrack.chi2Hit( hit );
      chi2nDoF /= double(fwdTrack.nCoords() - 5);
      if (chi2nDoF > maxTrackChi2) {
	resetHits( cand );
	continue;
      }

      // Num. of outliers
      unsigned int nOutliers = cand.nHits() - fwdTrack.nCoords();
      if (nOutliers > maxNbOutliers) {
	resetHits( cand );
	continue;
      }

      // Track quality
      const double w0 = -1.15104136013;  
      const double w1 = -0.126391514861;  
      const double w2 = -0.140524353725;  
      const double w3 = 0.154541380645; 
      const double w4 = -0.0097035742262; 
      double quality = w0 + 
  	               w1 * chi2nDoF + 
 		       w2 * nOutliers + 
 		       w3 * fwdTrack.nCoords() + 
 		       w4 * fwdTrack.nPlanes();

      // Reset hits for next candidates
      resetHits( cand );
      
      // Update hits and parameters
      cand.setHits( fwdTrack.nCoords(), fwdTrack.coords() );
      cand.setX0( fwdTrack.xAtZ(m_zReference) );
      cand.setY0( fwdTrack.yAtZ(m_zReference) );
      cand.setZ0( m_zReference );
      cand.setTx( fwdTrack.xSlope(m_zReference) );
      cand.setTy( fwdTrack.ySlope(m_zReference) );
      cand.setChi2nDoF( chi2nDoF );
      cand.setQuality( quality );
      cand.setIsValid( true );

      // Candidate is good! Store it!
      //m_outputTracks[reg].push_back( cand );

#ifdef DEBUG_AUTOMATA
      plot(chi2nDoF, "chi2nDoFShort", "fit chi2/ndof final for short tracks", 0.0, +30.0, 100);
      plot(cand.firstStation(), "firstStaShort", "First station on short tracks", 0.0, +5.0, 5);
      plot(cand.lastStation(), "lastStaShort", "Last station on short tracks", 0.0, +5.0, 5);
      
      // Match to MCParticles
      if (part != nullptr) {
	// Dump signal variables for MVA analysis
	fprintf(m_mvaSignalShortFile, "%f %i %i %i \n", 
	    chi2nDoF,  nOutliers, fwdTrack.nCoords(), fwdTrack.nPlanes() ); 

	plot(chi2nDoF, "chi2nDoFShortMatched", "fit chi2/ndof for short track", 0.0, +20.0, 100);
	plot(fwdTrack.nCoords(), "nHitsShortMatched", "Number of hits on long tracks", 6.0, 40.0, 34);
	plot(fwdTrack.nPlanes(), "nPlanesShortMatched", "Number of planes on long tracks", 2.0, 10.0, 8);
	plot(nOutliers, "nOutliersShortMatched", "Number of outliers on short tracks", 0.0, 15.0, 15);
	plot2D(chi2nDoF, nOutliers, "nOutliersChi2CorrelShortMatched", "nOutliers vs chi2nDoF for short tracks", 0.0, 12.0, 0.0, 10.0, 100, 10);
	plot2D(chi2nDoF, fwdTrack.nPlanes(), "nPlanesChi2CorrelShortMatched", "nPlanes vs chi2nDoF for short tracks", 0.0, 12.0, 0.0, 10.0, 100, 10);
	plot2D(nOutliers, fwdTrack.nPlanes(), "nPlanesNOutliersCorrelShortMatched", "nPlanes vs nOutliers for short tracks", 0.0, 10.0, 0.0, 10.0, 10, 10);
	plot(fracUsed, "fracUsedShortMatched", "Fraction of used hits for short track", 0.0, +1.0, 50);
      }

      // Ghosts...
      else {
	// Dump ghosts variables for MVA analysis
	fprintf(m_mvaGhostsShortFile, "%f %i %i %i \n", 
	    chi2nDoF,  nOutliers, fwdTrack.nCoords(), fwdTrack.nPlanes() ); 
	
	plot(chi2nDoF, "chi2nDoFShortGhosts", "fit chi2/ndof for short track", 0.0, +20.0, 100);
	plot(fwdTrack.nCoords(), "nHitsShortGhosts", "Number of hits on long tracks", 6.0, 40.0, 34);
	plot(fwdTrack.nPlanes(), "nPlanesShortGhosts", "Number of planes on long tracks", 2.0, 10.0, 8);
	plot(nOutliers, "nOutliersShortGhosts", "Number of outliers on short tracks", 0.0, 15.0, 15);
	plot2D(chi2nDoF, nOutliers, "nOutliersChi2CorrelShortGhosts", "nOutliers vs chi2nDoF for short tracks", 0.0, 12.0, 0.0, 10.0, 100, 10);
	plot2D(chi2nDoF, fwdTrack.nPlanes(), "nPlanesChi2CorrelShortGhosts", "nPlanes vs chi2nDoF for short tracks", 0.0, 12.0, 0.0, 10.0, 100, 10);
	plot2D(nOutliers, fwdTrack.nPlanes(), "nPlanesNOutliersCorrelShortGhosts", "nPlanes vs nOutliers for short tracks", 0.0, 10.0, 0.0, 10.0, 10, 10);
	plot(fracUsed, "fracUsedShortGhosts", "Fraction of used hits for short track", 0.0, +1.0, 50);
      }
#endif

      // Refit only if hits are added...
      //if (nHitsAfter > nHitsBefore) {
      //  // Re-fit
      //  okFit = m_seedTool->fitTrack( fwdTrack, maxHitChi2 + 2.0, minNbPlanes + 1, false, false );
      //  if(!okFit) {
      //    //info() << "Fit of short track failed!!!" << endmsg;
      //    resetHits( cand );
      //    continue;
      //  }

      //  // Cut on chi2/nDOF
      //  chi2nDoF = 0.0;
      //  worstChi2 = -Automata::BigDouble;
      //  for (const PatFwdHit* hit : fwdTrack.coords()) {
      //    double chi2 = fwdTrack.chi2Hit( hit );
      //    chi2nDoF += chi2;
      //    if (chi2 > worstChi2) worstChi2 = chi2;
      //  }
      //  chi2nDoF /= double(fwdTrack.nCoords() - 5);
      //  if (chi2nDoF > maxTrackChi2 + 2.0) {
      //    resetHits( cand );
      //    continue;
      //  }
      //}
      
      // Reset hits for next candidates
      //resetHits( cand );

      // Update hits and parameters
      //cand.setHits( fwdTrack.nCoords(), fwdTrack.coords() );
      //cand.setX0( fwdTrack.xAtZ(m_zReference) );
      //cand.setY0( fwdTrack.yAtZ(m_zReference) );
      //cand.setZ0( m_zReference );
      //cand.setTx( fwdTrack.xSlope(m_zReference) );
      //cand.setTy( fwdTrack.ySlope(m_zReference) );
      //cand.setChi2nDoF( chi2nDoF );

      // Candidate is good! Store it!
      //m_outputTracks[reg].push_back( cand );

      // Tag hits as used
      //for (unsigned int i = 0; i < cand.nHits(); i++) 
      //  cand.hit(i)->setIsUsed( true );

      // Tag stubs as used
      //for (unsigned int i = 0; i < cand.nTracklets(); i++) 
      //  cand.tracklet(i)->setIsUsed( true );

    }//track
  }//reg

  if ( m_measureTime ) m_timer->stop( m_timeSelectShort );

  return StatusCode::SUCCESS;
}


//================================================================================
// Kill short clones
//================================================================================
StatusCode AutomataTool::killShortClones() 
{
  /* Constants */
  // Max. nUsed/nHits on track
  const double maxFracUsed = 0.30; //0.30
  // Min. value for track quality 
  const double minQuality = 0.0; //0.0

  if ( m_measureTime ) m_timer->start( m_timeKillShortClones );

  // Loop over regions
  for (unsigned int reg = 0; reg < m_nReg; reg++) {
    // OT only...
    if (reg > 1) continue;
    
    // Sort tracks by decreasing nHits (if equal sort by chi2nDoF)
    std::sort( m_candTracks[m_nSta - 2][reg].begin(), m_candTracks[m_nSta - 2][reg].end(), 
	[] (const CATrack& first, const CATrack& second) {
	if (first.nHits() > second.nHits()) return true;
	if (first.nHits() < second.nHits()) return false;
	return first.chi2nDoF() < second.chi2nDoF();
	} );

    // Loop over tracks
    for (CATrack &cand : m_candTracks[m_nSta - 2][reg]) {

#ifdef DEBUG_AUTOMATA
      // Associate to MC
      LHCb::MCParticle *part = mcParticle( cand.lhcbIDs(), m_otlinker );
#endif

      // Reject bad candidates
      if (!cand.isValid()) continue;

      // Kill clones
      if (cand.fracUsedHits() > maxFracUsed) continue;

      // Cut on track quality
      if (cand.quality() < minQuality) continue;

      // Tag tracklets as used
      for (unsigned int i = 0; i < cand.nTracklets(); i++) {
	cand.tracklet(i)->setIsUsed( true );
      }

      // Tag hits as used
      for (unsigned int i = 0; i < cand.nHits(); i++) {
	cand.hit(i)->setIsUsed( true );
      }
      
      // It's good! Store it!
      m_outputTracks[reg].push_back( cand );

#ifdef DEBUG_AUTOMATA
      // Match to MCParticle
      if (part != nullptr) {
	plot(cand.chi2nDoF(), "chi2nDoFShortFinalMatched", "fit chi2/ndof for short track", 0.0, +10.0, 100);
	plot(cand.nHits(), "nHitsShortFinalMatched", "Number of hits on short tracks", 6.0, 40.0, 34);
	plot(cand.quality(), "qualityShortFinalMatched", "Fisher discriminant for short tracks", -1.0, +1.0, 100);
      }

      // It's ghost...
      else {
	plot(cand.chi2nDoF(), "chi2nDoFShortFinalGhosts", "fit chi2/ndof for short track", 0.0, +10.0, 100);
	plot(cand.nHits(), "nHitsShortFinalGhosts", "Number of hits on short tracks", 6.0, 40.0, 34);
	plot(cand.quality(), "qualityShortFinalGhosts", "Fisher discriminant for short tracks", -1.0, +1.0, 100);
      }
#endif

    }//tracks
  }//reg

  if ( m_measureTime ) m_timer->stop( m_timeKillShortClones );

  return StatusCode::SUCCESS;
}


//=========================================================================
// Collect all tracks found by Cellular Automata
//=========================================================================
StatusCode AutomataTool::collectTracks() 
{
  // Loop over regions
  for (unsigned int reg = 0; reg < m_nReg; reg++) {
    if (reg > 1) continue;

    // Convert CATracks to LHCb::Tracks and store
    for (CATrack& track : m_outputTracks[reg]) {
      LHCb::Track *out = new LHCb::Track();
      makeLHCbTrack(track, out); 
      m_lhcbTracks.push_back( out );
    }//tracks
  
  }//reg
 
  return StatusCode::SUCCESS;
}


//=========================================================================
// Collect all tracklets found by Cellular Automata
//=========================================================================
StatusCode AutomataTool::collectTracklets() 
{
  // Loop over stations
  for (unsigned int sta = 0; sta < m_nSta; sta++) {
    // Loop over regions
    for (unsigned int reg = 0; reg < m_nReg; reg++) {
      if (reg > 1) continue;
      // Loop over tracklets
      for (Tracklet& stub : m_trackStubs[sta][reg]) {
	// Convert to LHCb::Track and store
	LHCb::Track *out = new LHCb::Track();
	CATrack track(&stub);
	makeLHCbTrack(track, out); 
	m_outputTracks[reg].push_back( track );
	m_lhcbTracks.push_back( out );
      }//stubs
    }//reg
  }//sta
 
  return StatusCode::SUCCESS;
}


//=========================================================================
// Callback function used with GpuService::submitData 
//=========================================================================
// allocTracks takes the size of received data and a pointer to a GpuTrack
// vector. The received data is assumed to consist of an array of GpuTrack
// objects. allocTracks reserves enough space to store the received tracks and
// returns a pointer to the start of that memory.
void * allocTracks (size_t size, void * param) 
{ 
  typedef std::vector<uint8_t> Data;
  Data & tracks = *reinterpret_cast<Data*>(param);
  tracks.resize(size);
  return &tracks[0]; 
}


//=========================================================================
//   algorithm on the accelerator device and get results back
//=========================================================================
StatusCode AutomataTool::offload() 
{
  if ( m_measureTime ) m_timer->start( m_timeOffloading );

  // Serialize hits in a buffer to ship to the accelerator 
  if ( m_measureTime ) m_timer->start( m_timeSerializer );

  m_serializer.reset();
  Data serializedEvent;
  Data serializedTracks;

  try {
    for (unsigned int sta = 0; sta < m_nSta; ++sta) {
      for (unsigned int lay = 0; lay < m_nLay; ++lay) {
	for (unsigned int reg = 0; reg < m_nReg; ++reg) {
	  HitRange range = hits(sta, lay, reg);
	  m_serializer.serialize(sta, lay, reg, range);
	}//reg
      }//lay
    }//sta

    serializedEvent = m_serializer.buffer();
  } 
  catch (const std::exception & e) {
    error() << "serialization failed; " << e.what() << endmsg; 
  }

  if (serializedEvent.empty())
    info() << "--- Serialized event is empty! This should not happen!" << endmsg;

  if ( m_measureTime ) m_timer->stop( m_timeSerializer );

  //info() << "--- Submitting data to gpuService" << endmsg;
  //info() << "--- serializedEvent: 0x" << std::hex << (long long int)&serializedEvent[0] <<
  //          std::dec << ", size " << serializedEvent.size() << endmsg;

  // Offload the algorithm on the accelerator
  try {
 
    m_gpuService->submitData( "AutomataCudaHandler",
        serializedEvent.data(),
        serializedEvent.size(),
        allocTracks,
        &serializedTracks);
/*
     // Do not use GpuManager
     std::vector<Data> trackCollection;
     const Data* in[1] = { &serializedEvent }; 
     const Batch input(in, in+1);
   
     Automata(input, trackCollection);
*/
    try {
      debug() << "--- Deserializing tracks" << endmsg;
      deserialize( serializedTracks,  &m_lhcbTracks );
    } 
    catch (const std::exception & e) {
      error() << "deserialization failed; " << e.what() << endmsg;
    } 
    catch (...) {
      error() << "deserialization failed; reason unknown" << endmsg;
    }
  } 
  catch (const std::exception & e) {
    error() << "submission failed; " << e.what() << endmsg;
  } 
  catch (...) {
    error() << "submission failed; reason unknown" << endmsg;
  }
  
  if ( m_measureTime ) m_timer->stop( m_timeOffloading );
  
  return StatusCode::SUCCESS;
}

//=========================================================================
// Reset hits
//=========================================================================
inline void AutomataTool::resetHit (PatFwdHit* hit)
{
  hit->setX( hit->hit()->xAtYEq0() );
  hit->setZ( hit->hit()->zAtYEq0() );
  //hit->setIsUsed( false );
  //hit->setIgnored( false );
  //hit->setSelected( true );
  hit->setRlAmb( 0 );
  const Tf::OTHit* otHit = hit->hit()->othit();
  if (otHit)
    hit->setDriftDistance( otHit->untruncatedDriftDistance(0) );
}


//=========================================================================
// Reset hits for Tracklet
//=========================================================================
inline void AutomataTool::resetHits(Tracklet &tracklet) 
{
  for (unsigned int i = 0; i < tracklet.nHits(); i++) {
    PatFwdHit *hit = tracklet.hit(i);
    resetHit( hit );
  }
}


//=========================================================================
// Reset hits for CATrack
//=========================================================================
inline void AutomataTool::resetHits(CATrack &track) 
{
  for (unsigned int i = 0; i < track.nHits(); i++) {
    PatFwdHit *hit = track.hit(i);
    resetHit( hit );
  }
}


//=========================================================================
// Decide if a cluster has a neighbour
//=========================================================================
inline bool AutomataTool::hasNeighbour(Clusters::iterator itCl, Clusters &clusters) 
{
  // Max. distance to collect 
  //const double maxNbDist = 7.875; //mm (1.5 pitch)
  //const double maxNbDist1 = 5.25; //mm (1. pitch)
  //const double maxNbDist2 = 7.875; //mm

  //// Next is the end...
  //if ((itCl + 1) == clusters.end()) return false;

  //Cluster *clus = *(itCl);
  //Cluster *nextClus = *(itCl + 1);

  //// First method:
  ////if (std::abs(nextClus->x() - clus->x()) < maxNbDist) return true;

  //// Second method:
  //if (clus->isCluster() && nextClus->isCluster()) {
  //  if (std::abs(nextClus->x() - clus->x()) < maxNbDist1) return true;
  //}
  //else if (!clus->isCluster() && !nextClus->isCluster()) {
  //  if (std::abs(nextClus->x() - clus->x()) == maxNbDist1 
  //      || std::abs(nextClus->x() - clus->x()) == maxNbDist2) return true;
  //}
  //else {
  //  if (std::abs(nextClus->x() - clus->x()) > maxNbDist1 
  //      && std::abs(nextClus->x() - clus->x()) < maxNbDist2) return true;
  //}

  return false;
}


//=========================================================================
// Check if two PatFwdHits make a cluster
//=========================================================================
bool AutomataTool::isCluster(PatFwdHit *hit1, PatFwdHit *hit2) 
{ 
  Tf::OTHit const *othit1 = hit1->hit()->othit();
  Tf::OTHit const *othit2 = hit2->hit()->othit();
  //const LHCb::OTChannelID otID1( hit1->hit()->lhcbID().otID() );
  //const LHCb::OTChannelID otID2( hit2->hit()->lhcbID().otID() );

  // Hits are OT hits (Tf::RegionID = 4)
  if (othit1 == 0 || othit2 == 0) return false;
    
  // Get hit1 info
  double x1 = hit1->hit()->xAtYEq0();
  //double z1 = hit1->hit()->zAtYEq0();
  //int quarter1 = othit1->module().quarterID();
  //int module1 = othit1->module().moduleID();
  //int mono1 = 1 - othit1->monolayer();
  //int nStraws = othit1->module().nChannels()/2;
  //int straw1 = othit1->straw() - (1-mono1)*nStraws;

  // Get hit2 info
  double x2 = hit2->hit()->xAtYEq0();
  //double z2 = hit2->hit()->zAtYEq0();
  //int quarter2 = othit2->module().quarterID();
  //int module2 = othit2->module().moduleID();
  //int mono2 = 1 - othit2->monolayer();
  //nStraws = othit2->module().nChannels()/2;
  //int straw2 = othit2->straw() - (1-mono2)*nStraws;

  // Condition for making a cluster
  bool verdict = (std::abs(x2 - x1) < 3.0);
  //bool verdict = hit1->hasNext();

  //bool verdict =  
  //  (quarter1 == quarter2) 
  //  && (module1 == module2)  
  //  && (mono1 != mono2) 
  //  && (abs(straw1 - straw2) < 2);

  //==============================================================================================
  // Check...
  //==============================================================================================
  //if (verdict && (otID2.uniqueModule() != otID1.uniqueModule())) { 
  //  info() << "dx " << x2 - x1 << " dz " << z2 - z1 
  //         << " dmodule " << module2 - module1 << " dquarter " << quarter2 - quarter1 << " dmono " << mono2 - mono1 << " dstraw(shifted) " << straw2 - straw1 
  //         << " dchannels " << othit2->module().nChannels() -  othit1->module().nChannels() << " straw1 " <<  othit1->straw() << " straw2 " <<  othit2->straw() 
  //         << " duniqueModule " << otID2.uniqueModule() - otID1.uniqueModule() << endmsg;
  //}
  //==============================================================================================

  return verdict;
}


//=========================================================================
// Make clusters from PatFwdHits
//=========================================================================
void AutomataTool::makeClusters(PatFwdHits &hits, Clusters &clusters) 
{
  // Reset hits before clustering...
  for (PatFwdHit *hit : hits) resetHit( hit );

  //=============================
  // Standard clustering
  //=============================
  //int status;
  //bool isNotClusterised = true;
  //size_t nHits = hits.size();

  // nHits should be > 2
  //if (nHits == 0) return; 
  //if (nHits == 1) {
  //  PatFwdHit *h = hits[0];
  //  if (h->hit()->othit() != 0) clusters.push_back( new Cluster(h) );
  //  return;
  //}

  // Loop on hits
  //for (PatFwdHits::const_iterator it = hits.begin(); hits.end() - 1 != it; ++it) {
  //  PatFwdHit *hit = *it;
  //  PatFwdHit *next = *(it + 1);

  //  // Check if they form a cluster
  //  bool ok = isCluster(hit, next, status);
  //  if (status == -1) {
  //    info() << "WARNING IN MakeClusters! LOOK AT CLUSTER STATUS!!!" << endmsg;
  //    continue;
  //  }

  //  if (ok) clusters.push_back( new Cluster(hit, next) );

  //  else {
  //    if (isNotClusterised) clusters.push_back( new Cluster(hit) );
  //    if (it == hits.end() - 1) clusters.push_back( new Cluster(next) );
  //  }

  //  // Set hit status for next iteration
  //  isNotClusterised = !ok;
  //}//hits


  //=======================================
  // New version
  //=======================================
  // Loop on hits
  for (PatFwdHits::const_iterator itH = hits.begin(); hits.end() != itH; ++itH) {
    PatFwdHits::const_iterator itH2 = itH + 1;
    PatFwdHits::const_iterator itH3 = itH + 2;

    // Last hit...
    if (itH2 == hits.end()) {
      clusters.push_back( new Cluster(*itH) );
      break;
    }

    // Check if they form a cluster
    if (isCluster(*itH, *itH2)) {
      Cluster* clus = new Cluster(*itH, *itH2);
      // Merge next cluster, if it exists...
      if (itH3 != hits.end() && isCluster(*itH2, *itH3)) {
        clus->addHit( *itH3 );
      }
      clusters.push_back( clus );
      itH++;
    }
    
    else {
      clusters.push_back( new Cluster(*itH) );
    }

    //if (itH3 != hits.end() && 
    //    std::abs((*(itH))->hit()->xAtYEq0() - (*(itH3))->hit()->xAtYEq0()) <= 1.5*pitch) {
    //  Cluster* clus = new Cluster();
    //  clus->addHit( *itH );
    //  clus->addHit( *itH2 );
    //  clus->addHit( *itH3 );
    //  clusters.push_back( clus );
    //  itH++;
    //}

    //else if (std::abs((*(itH))->hit()->xAtYEq0() - (*(itH2))->hit()->xAtYEq0()) <= 1.0*pitch) {
    //  Cluster* clus = new Cluster();
    //  clus->addHit( *itH );
    //  clus->addHit( *itH2 );
    //  clusters.push_back( clus );
    //  itH++;
    //}

    //else {
    //  clusters.push_back( new Cluster(*itH) );
    //}
  }//hits
}


//=========================================================================
// Wrapper for using HitRange instead of PatFwdHits
//=========================================================================
void AutomataTool::makeClusters(HitRange &range, Clusters &clusters) 
{
  PatFwdHits hits;
  hits.reserve(100);

  for (PatFwdHit *hit : range) 
    hits.push_back(hit);

  makeClusters(hits, clusters);
}


//=========================================================================
// Convert Tracklet to LHCb::Track
//=========================================================================
void AutomataTool::makeLHCbTrack( const CATrack& track, LHCb::Track *out ) 
{
  // Set properties
  out->setType( LHCb::Track::Ttrack );
  out->setHistory( LHCb::Track::PatSeeding );
  out->setPatRecStatus( LHCb::Track::PatRecIDs );

  // Loop on hits
  for (unsigned int i = 0; i < track.nHits(); i++) {
    PatFwdHit *hit = track.hit(i);
    // add lhcbIDs
    out->addToLhcbIDs( hit->hit()->lhcbID() );
    // set status
    hit->hit()->setStatus(Tf::HitBase::UsedByPatSeeding);
    // tag hit as used 
    hit->setIsUsed(true);
  }

  // Add track state
  Gaudi::TrackVector vect(track.xAtZ(m_zReference),
                          track.yAtZ(m_zReference),
                          track.tx(),
                          track.ty(),
                          0.0);

  LHCb::State temp(vect, m_zReference, LHCb::State::AtT);

  out->addToStates( temp );
  // put the chi^2 and NDF of our track fit onto the track (e.g. for use in
  // the trigger before the track is fitted with the Kalman filter)
  // we save chi^2/ndf on our track, so compensate
  unsigned ndf = track.nHits() - 5;
  out->setChi2AndDoF(double(ndf) * track.chi2nDoF(), ndf);

}

//=========================================================================
// Deserialize and convert GPU tracks
//=========================================================================
void AutomataTool::deserialize( const Data& trackData, std::vector<LHCb::Track*>* outputTracks) {

  if (trackData.empty())
    throw std::runtime_error("Empty track data.");
  if (trackData.size() % sizeof(GpuOutTrack) != 0) {
    std::string msg = "Invalid track data size: " + std::to_string(trackData.size()) + " - " + std::to_string(sizeof(GpuOutTrack));
    throw std::runtime_error(msg);
  }
  
  const GpuOutTrack * outTracks = reinterpret_cast<const GpuOutTrack*>(trackData.data());

  const size_t count = trackData.size() / sizeof(GpuOutTrack);

  outputTracks->reserve(outputTracks->size()+count);

  for (size_t i = 0; i != count; ++i) {
    LHCb::Track* finalTk = new LHCb::Track();
    convert(outTracks[i], finalTk);
    outputTracks->push_back( finalTk );
  }

}

void AutomataTool::convert( const GpuOutTrack& gtk, LHCb::Track *out ) {
  // Set properties
  out->setType( LHCb::Track::Ttrack );
  out->setHistory( LHCb::Track::PatSeeding );
  out->setPatRecStatus( LHCb::Track::PatRecIDs );

  // Loop on hits
  for (int i = 0; i < gtk.nHits; i++) {
    // add lhcbIDs
    out->addToLhcbIDs( (LHCb::LHCbID)gtk.hit[i] );
    // set status TODO
    //hit->hit()->setStatus(Tf::HitBase::UsedByPatSeeding);
    // tag hit as used TODO
    //hit->setIsUsed(true);
  }

  // Add track state
  Gaudi::TrackVector vect(gtk.x0,
                          gtk.y0,
                          gtk.tx,
                          gtk.ty,
                          0.0);

  LHCb::State temp(vect, StateParameters::ZMidT, LHCb::State::AtT);
  
  double qOverP, sigmaQOverP;
  if (m_momentumTool->calculate(&temp, qOverP, sigmaQOverP, true).isFailure()) {
    // if our momentum tool doesn't succeed, we have to try ourselves
    // so get q/p from curvature
    const double scaleFactor = m_magFieldSvc->signedRelativeCurrent();
    if (std::abs(scaleFactor) < 1e-4) {
      // return a track of 1 GeV, with a momentum uncertainty which makes it
      // compatible with a 1 MeV track
      qOverP = ((gtk.curvature < 0) ? -1. : 1.) *
        ((scaleFactor < 0.) ? -1. : 1.) / Gaudi::Units::GeV;
      sigmaQOverP = 1. / Gaudi::Units::MeV;
    } else {
      // get qOverP from curvature
      qOverP = gtk.curvature * m_momentumScale / (-1*scaleFactor);
      sigmaQOverP = 0.5 * qOverP; // be conservative
    }
  }
  temp.setQOverP(qOverP);
  
  out->addToStates( temp );
  
  // put the chi^2 and NDF of our track fit onto the track (e.g. for use in
  // the trigger before the track is fitted with the Kalman filter)
  // we save chi^2/ndf on our track, so compensate
  unsigned ndf = gtk.nHits - 5;
  out->setChi2AndDoF(double(ndf) * gtk.chi2xdof, ndf);

}

//=========================================================================
// Associate reconstructed track to MCParticle
//=========================================================================
LHCb::MCParticle* AutomataTool::mcParticle( const std::vector<LHCb::LHCbID>& ids,
                  	                    Linker* linker,
					    double minWeight ) 
{
  // Min value of weight for matching
  //const double minWeight = 0.40;

  // Match track to MC particles based on LHCbIDs
  std::vector<std::pair<LHCb::MCParticle*, float> > tmp = mcMatch(ids, linker);
  
  // Associate track to MC particle with the highest weight 
  if (!tmp.empty()) {

#ifdef DEBUG_AUTOMATA
    plot(tmp.front().second, "mcweight", "Weight associated to track", 0.0, +1.0, 60);
#endif
    
    if (tmp.front().second >= minWeight) // sorted by weight!
      return tmp.front().first;
  }

  // It's a ghost...
  return nullptr;
}


//=========================================================================
// Match vector of LHCbIDs to MC particles
//=========================================================================
std::vector<std::pair<LHCb::MCParticle*, float> > 
AutomataTool::mcMatch( const std::vector<LHCb::LHCbID>& ids, Linker* linker ) const
{
  unsigned int nHits = ids.size();
  unsigned int nParts = 0;
  std::vector<std::pair<LHCb::MCParticle*, float> > assoc; // (particle, its weight)
  assoc.reserve( nHits );

  // Loop over track ids
  for (LHCb::LHCbID id : ids) {
    LHCb::OTChannelID otID = id.otID();
    LHCb::MCParticle* part = linker->first( otID );

    // Loop over MCParticles associated to the given id
    while (0 != part) {
      ++nParts;
      bool found = false;

      // If we already have part in the list, increase its weight
      for (unsigned int i = 0; i < assoc.size(); ++i) {
        if (part == assoc[i].first) {
          assoc[i].second += 1.f;
          found = true;
          break;
        }
      }

      // Otherwise, we put it in
      if (!found) assoc.push_back( std::make_pair(part, 1.f) );

      part = linker->next();
    }//part
  }//ids

  // Normalize weights
  const float scale = 1.f / float(nHits); // USE nParts INSTEAD ???
  for (unsigned int i = 0; i < assoc.size(); ++i) 
    assoc[i].second *= scale;

  // Sort by weight
  if (2 <= assoc.size())
    std::sort(assoc.begin(), assoc.end(), 
	[] (std::pair<LHCb::MCParticle*, float> p1, std::pair<LHCb::MCParticle*, float> p2) 
	{ return p1.second > p2.second; });

  return assoc;
}


//=========================================================================
// Match an LHCbID to a MCParticle
//=========================================================================
bool AutomataTool::mcMatch (const LHCb::LHCbID& id, 
                            LHCb::MCParticle *part,
                      	    Linker* linker) const
{
  // No MCParticle...
  if (part == nullptr) return false;

  // Get all MCParticles associated with id
  std::vector<LHCb::LHCbID> ids(1, id);
  std::vector<std::pair<LHCb::MCParticle*, float> > tmp = mcMatch(ids, linker);

  // Loop over associated MCParticles
  for (std::pair<LHCb::MCParticle*, float> pair : tmp) {
    // We got it!
    if (pair.first == part) return true;
  }

  return false;
}


//=========================================================================
// Wrapper for PatFwdHit
//=========================================================================
bool AutomataTool::mcMatch (PatFwdHit *hit, 
                            LHCb::MCParticle *part,
                      	    Linker* linker) const
{ 
  return mcMatch(hit->hit()->lhcbID(), part, linker); 
}


//=========================================================================
// Wrapper for Cluster
//=========================================================================
bool AutomataTool::mcMatch (Cluster *clus, 
                            LHCb::MCParticle *part,
                      	    Linker* linker) const
{ 
  // One hit is sufficient
  if (clus->isCluster()) {
   return (mcMatch(clus->hit(0), part, linker) || 
           mcMatch(clus->hit(1), part, linker)); 
  }
  else return mcMatch(clus->hit(0), part, linker);
}


//=========================================================================
//  Debug one coordinate
//=========================================================================
void AutomataTool::debugFwdHit ( const PatFwdHit* coord, MsgStream& msg ) const
{
  msg << format( " Z %10.2f Xp %10.2f X%10.2f  St%2d lay%2d typ%2d used%2d",
                 coord->z(),
                 coord->projection(),
                 coord->x(),
                 coord->hit()->station(),
                 coord->hit()->layer(),
                 coord->hit()->region(),
                 coord->isUsed() );
  if ( coord->hit()->type() == Tf::RegionID::OT ) {
    msg << format( " P%2d N%2d RL%2d Drift %7.3f",
                   coord->hasPrevious(),
                   coord->hasNext(),
                   coord->rlAmb(),
                   coord->driftDistance() );
  }
  //else {
  //msg << format( " High Threshold %2d         ", coord->highThreshold() );
  //}

  //for ( std::vector<int>::const_iterator itM = coord->MCKeys().begin();
  //    coord->MCKeys().end() != itM; ++itM ) {
  //msg << " " << *itM;
  //}
  msg << endmsg;
}


//=========================================================================
//  Dump one coordinate on file
//=========================================================================
void AutomataTool::dumpFwdHit ( const PatFwdHit* hit ) const
{
  fprintf(m_dumpFile, "%i %i %i %f %f \n", 
      hit->hit()->station(), hit->hit()->layer(), hit->hit()->region(), 
      hit->hit()->xAtYEq0(), hit->hit()->zAtYEq0() );
}


//=========================================================================
//  Dump one coordinate on file
//=========================================================================
void AutomataTool::dumpCATrack ( const CATrack &track ) const
{
  fprintf(m_dumpFile, "%i %f %f %f %f %f \n", 
      track.nHits(), track.x0(), track.y0(), track.z0(), track.tx(), track.ty() ); 

  for (unsigned int i = 0; i < track.nHits(); i++) {
    PatFwdHit *hit = track.hit(i);
    dumpFwdHit( hit );
  }
}


//=========================================================================
//  Dump event on file
//=========================================================================
void AutomataTool::dumpEvent() const
{
  static unsigned int eventNum = 0; 
  unsigned int nHits = 0, nTracks = 0;

  // Dump global info
  HitRange range = hits();
  nHits = range.size();
  for (unsigned int reg = 0; reg < m_nReg; reg++)
    nTracks += m_outputTracks[reg].size(); 
  fprintf(m_dumpFile, "%i %i %i \n", eventNum, nHits, nTracks); 

  // Dump T-stations hits
  for (PatFwdHit* hit: range) 
    dumpFwdHit( hit );

  // Dump tracks
  for (unsigned int reg = 0; reg < m_nReg; reg++)
    for (unsigned int i = 0; i < m_outputTracks[reg].size(); i++) 
      dumpCATrack( m_outputTracks[reg][i] );

  eventNum++;
}

