#ifndef AUTOMATATOOL_H
#define AUTOMATATOOL_H 1

// For debugging...
//#define DEBUG_AUTOMATA 1  // Enable debugging (histos, ...)
#define FINAL_PLOTS 1  // Enable debugging (histos, ...)

// Include files
#include <cmath>
#include <string>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <fstream>
#include <vector>

// From Gaudi
#if defined DEBUG_AUTOMATA || defined FINAL_PLOTS
#include "GaudiAlg/GaudiTupleTool.h"
#else
#include "GaudiAlg/GaudiTool.h"
#endif

#include "GaudiAlg/ISequencerTimerTool.h"
//#include "TrackInterfaces/IPatSeedingTool.h"
//#include "TrackInterfaces/ITracksFromTrack.h"
#include "TrackInterfaces/ITrackMomentumEstimate.h"
#include "Kernel/ILHCbMagnetSvc.h"

#include "Event/Track.h"

#include "TfKernel/RegionID.h"
#include "TfKernel/RecoFuncs.h"
#include "TfKernel/TStationHitManager.h"

#include "PatKernel/PatForwardHit.h"
#include "PatSeedTool.h"
#include "PatSeedTrack.h"
#include "PatRange.h"

// linking MC particles
#include "Linker/LinkedTo.h"
#include "Linker/LinkerWithKey.h"
#include "Event/MCParticle.h"
#include "Event/MCHit.h"
#include "Kernel/LHCbID.h"
#include "Event/OTTime.h"
#include "Event/STCluster.h"

// Cellular Automata
#include "Automata.h"
#include "Cluster.h"
#include "Tracklet.h"
#include "CATrack.h"

// GpuService
#include "GpuService/IGpuService.h"

// Serializer 
#include "EventSerializer.h"


//=========================================================================
// Namespaces
//=========================================================================
namespace LHCb {
  class State;
}


//=========================================================================
// Class definition
//=========================================================================
static const InterfaceID IID_AutomataTool ( "AutomataTool", 1, 0 );

#if defined DEBUG_AUTOMATA || defined FINAL_PLOTS
class AutomataTool : public GaudiTupleTool
#else
class AutomataTool : public GaudiTool
#endif
{
  public:
    typedef Tf::TStationHitManager<PatForwardHit>::HitRange HitRange;
    typedef std::vector<PatSeedTrack> PatSeedTracks;
    typedef LinkedTo<LHCb::MCParticle> Linker;
    typedef EventSerializer::Data Data;

    /* Return the interface ID */
    static const InterfaceID& interfaceID() { return IID_AutomataTool; }

    /* Standard constructor */
    AutomataTool( const std::string& type,
	const std::string& name,
	const IInterface* parent);
 
    /* Destructor */
    ~AutomataTool( );

    // initialize the tool for subsequent use
    StatusCode initialize();

    /* finalize the tool */
    StatusCode finalize();

    /* Main Cellular Automata routines */
    StatusCode cellularAutomata( LHCb::Tracks* output );
    StatusCode cellularAutomata();
    
    /* Init Cellular Automata containers */
    StatusCode initAutomata();
    
    /* Delete all containers */
    StatusCode clearEvent();
    
    /* Make clusters */
    StatusCode makeClusters();
    
    /* Make x-tracklets */
    StatusCode makeXTracklets(); 
    
    /* Add stereo hits*/
    StatusCode addStereoHits();
    
    /* Fit stubs */
    StatusCode fitStubs(); 
    
    /* Find neighbours */
    StatusCode findNeighbours(); 
    
    /* Evolve tracklets */
    StatusCode evolutionStep();
    StatusCode evolve();

    /* Link tracklets and build tracks */
    StatusCode linkTracklets();

    /* Select long tracks */
    StatusCode selectLongTracks();

    /* Select shorter tracks */
    StatusCode selectShortTracks();
    
    /* Kill clones */
    StatusCode killLongClones();
    StatusCode killShortClones();

    /* Collect tracks found by Cellular Automata */
    StatusCode collectTracks();
    
    /* Collect tracklets found by Cellular Automata */
    StatusCode collectTracklets();
    
    /* Offload algorithm on the accelerator device */
    StatusCode offload();

    /* Prepare hits in IT/OT for use with the tool
       return number of hits in IT and OT */
    unsigned prepareHits();

    /* Helper for debugging printouts */
    void debugFwdHit ( const PatFwdHit* coord, MsgStream& msg ) const;

    /* Dump event to file */
    void dumpEvent() const;

    /* Dump FwdHit to file */
    void dumpFwdHit (const PatFwdHit*) const;

    /* Dump CATrack to file */
    void dumpCATrack (const CATrack&) const;

    /* Match ids to MC particles */
    std::vector<std::pair<LHCb::MCParticle*, float> > mcMatch( const std::vector<LHCb::LHCbID>&, Linker* ) const;

    /* Match id to a MC particle */
    bool mcMatch( const LHCb::LHCbID&, LHCb::MCParticle*, Linker* ) const;

    /* Wrapper for PatFwdHit */
    bool mcMatch( PatFwdHit*, LHCb::MCParticle*, Linker* ) const;
    
    /* Wrapper for Cluster */
    bool mcMatch( Cluster*, LHCb::MCParticle*, Linker* ) const;

    /* Returns MC particle associated to track */
    LHCb::MCParticle* mcParticle( const std::vector<LHCb::LHCbID>&, Linker*, double  w = 0.70);


  private:
    /* Function members */

    /* returns reference to m_clusters in (sta, lay, reg) */
    Clusters& clustersInRegion (unsigned int sta, unsigned int lay, unsigned int reg) 
    { return m_clusters[sta][lay][reg]; }

    /* true if region reg is OT region */
    inline bool isRegionOT(const unsigned reg) const
    { return reg < Tf::RegionID::OTIndex::kNRegions; }

    /*true if region reg is IT region */
    inline bool isRegionIT(const unsigned reg) const
    { return reg >= Tf::RegionID::OTIndex::kNRegions; }

    /* provide convenient access to regions via hit manager */
    inline const Tf::EnvelopeBase* region(const int sta, const int lay, const int reg) const
    { return m_tHitManager->region(sta, lay, reg); }

    /* provide convenient access to hits via hit manager */
    inline HitRange hits() const { return m_tHitManager->hits(); }

    /* provide convenient access to hits via hit manager */
    inline HitRange hits(const int sta, const int lay, const int reg) const
    { return m_tHitManager->hits(sta, lay, reg); }

    /* provide convenient access to hits with minimum x via hit manager */
    inline HitRange hitsWithMinX(const double xmin, const int sta,
	const int lay, const int reg) const
    { return m_tHitManager->hitsWithMinX(xmin, sta, lay, reg); }

    /* provide convenient access to hits in range (x given at y = 0) */
    inline HitRange hitsInRange( const PatRange& xrange, const unsigned sta,
	const unsigned lay, const unsigned reg) const;

    /* estimate dz/dy of a region */
    inline double regionDzDy(const int sta, const int lay,
	const int reg) const
    { return m_RCdzdy[(sta * m_nLay + lay) * m_nReg + reg]; }

    /* estimate dz/dx of a region */
    inline double regionDzDx(const int sta, const int lay,
	const int reg) const
    { return m_RCdzdx[(sta * m_nLay + lay) * m_nReg + reg]; }

    /* estimate z at x = 0, y = 0 of a region */
    inline double regionZ0(const int sta, const int lay,
	const int reg) const
    { return m_RCz0[(sta * m_nLay + lay) * m_nReg + reg]; }

    /* shortcut for a region's sinT/cosT */
    inline double regionTanT(const int sta, const int lay,
	const int reg) const
    { return m_RCtanT[(sta * m_nLay + lay) * m_nReg + reg]; }

    /* restore coordinates and drift time of a hit at y=0 */
    inline void restoreCoordinate(PatFwdHit* hit) const;

    struct isTStation : std::unary_function<const LHCb::LHCbID, bool> {
      bool operator()(const LHCb::LHCbID id) const
      { return id.isIT() || id.isOT(); }
    };

    /* return extension of a region in x */
    inline PatRange regionX(const unsigned sta, const unsigned lay,
	const unsigned reg, const double tol = 0.) const
    {
      return PatRange(region(sta,lay,reg)->xmin() - tol,
	  region(sta,lay,reg)->xmax() + tol);
    }

    /* return extension of a region in y */
    inline PatRange regionY(const unsigned sta, const unsigned lay,
	const unsigned reg, const double tol = 0.) const
    {
      return PatRange(region(sta,lay,reg)->ymin() - tol,
	  region(sta,lay,reg)->ymax() + tol);
    }

    /* return extension of a region in z */
    inline PatRange regionZ(const unsigned sta, const unsigned lay,
	const unsigned reg, const double tol = 0.) const
    {
      return PatRange(region(sta,lay,reg)->zmin() - tol,
	  region(sta,lay,reg)->zmax() + tol);
    }

    /* Reset the hit to its initial status pre-tracking */
    inline void resetHit(PatFwdHit* hit);

    /* Reset hits for a Tracklet */
    inline void resetHits(Tracklet& tracklet);

    /* Reset hits for a CATrack */
    inline void resetHits(CATrack& track);

    /* Check whether or not two PatFwdHit form a cluster */ 
    // TODO: - try to avoid use of othit (should handle also IT hits)
    bool isCluster(PatFwdHit *hit1, PatFwdHit *hit2); 

    /* Make clusters from PatFwdHits */
    void makeClusters(PatFwdHits &hits, Clusters &clusters); 

    /* Wrapper for using HitRange instead of PatFwdHits */
    void makeClusters(HitRange &range, Clusters &clusters); 

    /* Convert Tracklet to LHCb::Track */
    void makeLHCbTrack( const CATrack& tracklet, LHCb::Track *out ); 

    /* Convert GpuTrack to LHCb::Track */    
    void deserialize( const Data& trackData, std::vector<LHCb::Track*>* outputTracks);
    void convert( const GpuOutTrack& gtk, LHCb::Track *out );
    
    /* Compare two tracklets by tx */
    bool lowerByTx(const Tracklet& lhs, const Tracklet& rhs) 
    { return (lhs.tx() < rhs.tx()); }

    /* Decide if a cluster has a neighbour */
    inline bool hasNeighbour(Clusters::iterator, Clusters&);

    /* Members */

    // useful constants
    static const unsigned int m_nSta = Tf::RegionID::OTIndex::kNStations;
    static const unsigned int m_nLay = Tf::RegionID::OTIndex::kNLayers;
    static const unsigned int m_nReg = Tf::RegionID::OTIndex::kNRegions + Tf::RegionID::ITIndex::kNRegions;
    static const unsigned int m_nOTReg = Tf::RegionID::OTIndex::kNRegions;
    static const unsigned int m_nITReg = Tf::RegionID::ITIndex::kNRegions;

    // constants roughly defining the extension of the inefficient area in
    // IT and OT due to gap between ladders/division of OT modules into
    // lower and upper half
    static const int m_centralYOT = 50; // mm
    static const int m_centralYIT = 4; // mm
   
    // cache some values for the different regions
    double m_RCdzdy[m_nSta * m_nLay * m_nReg]; ///< cache for regionDzDy
    double m_RCdzdx[m_nSta * m_nLay * m_nReg]; ///< cache for regionDzDx
    double m_RCz0[m_nSta * m_nLay * m_nReg]; ///< cache for regionZ0
    double m_RCtanT[m_nSta * m_nLay * m_nReg]; ///< cache for regionTanT

    Tf::TStationHitManager <PatForwardHit> *  m_tHitManager;    // tool to provide hits
    ILHCbMagnetSvc*  m_magFieldSvc; // tool to provide magnetic field
    PatSeedTool* m_seedTool;  // tool providing PatSeeding tracking

    std::string m_inputTracksName;
    double m_zMagnet;
    double m_xMagTol;
    double m_tolCollectOT;
    double m_tolCollectIT;
    double m_tolCollectITOT;
    double m_initialArrow;
    double m_zReference;
    std::vector<double> m_zOutputs;
    double m_dRatio;
    double m_tolExtrapolate;

    int    m_minXPlanes;
    double m_curveTol;
    double m_commonXFraction;
    double m_maxRangeOT;
    double m_maxRangeIT;
    double m_maxChi2HitOT;
    double m_maxChi2HitIT;
    int m_minTotalPlanes;
    double m_maxTrackChi2;
    double m_maxFinalChi2;
    double m_maxFinalTrackChi2;
    double m_maxYAtOrigin;
    double m_maxYAtOriginLowQual;
    double m_yCorrection;

    double m_stateErrorX2;
    double m_stateErrorY2;
    double m_stateErrorTX2;
    double m_stateErrorTY2;
    double m_momentumScale;
    double m_minMomentum;

    double m_tolCollect;
    double m_maxChi2Hit;
    double m_zForYMatch;
    double m_maxImpact;
    bool m_useForward;
    bool m_useForwardTracks;
    unsigned m_nDblOTHitsInXSearch;
    bool m_printing;

    // turns on some tuning for B field off
    bool m_fieldOff;

    // maximum distance of two tracks to be considered for pattern reco
    // clone killing
    double m_cloneMaxXDistIT;
    double m_cloneMaxXDistOT;

    // chi^2 cutoff for tracks with few OT hits
    double m_maxTrackChi2LowMult;

    // weighting for quality variable:
    // first element is for number of coordinates
    // second element is for chi^2
    std::vector<double> m_qualityWeights;

    // maximum number of missed layers during x search
    int m_maxMisses;

    // shall we enforce isolation criteria?
    bool m_enforceIsolation;
    // hits/clusters must be isolated in a window around their mean position
    // (window extends for m_[IO]TIsolation in both directions)
    double m_OTIsolation;
    double m_ITIsolation;
    // true (default) if tracks found by a previous instance of PatSeedingTool
    // should be reused by this instance
    bool m_reusePatSeeding;

    // use tool for momentum parametrisation
    std::string m_momentumToolName;
    ITrackMomentumEstimate *m_momentumTool;

    // cuts on fraction of used hits during final selection (per stage)
    double m_maxUsedFractPerRegion;
    double m_maxUsedFractITOT;
    double m_maxUsedFractLowQual;

    // cut on drift radius
    std::vector<double> m_driftRadiusRange;
    // threshold below which an OT track is considered to have few hits, thus
    // having to satisfy stricter criteria
    unsigned m_otNHitsLowThresh;

    // clone killing among forward tracks
    double m_forwardCloneMaxXDist;
    double m_forwardCloneMaxYDist;
    double m_forwardCloneMaxTXDist;
    double m_forwardCloneMaxShared;
    bool m_forwardCloneMergeSeg;

    // are we reconstructing cosmics? If so, combine upper/lower half
    bool m_cosmics;

    // maximum number of hits per subdetector
    unsigned m_maxITHits;
    unsigned m_maxOTHits;
    bool m_abortOnVeloAbort;

    // Re-using of hits on PatForward tracks
    bool m_onlyGood;
    double m_discardChi2;

    /// maximum number of holes in a track (i.e. planes without hit)
    unsigned m_maxHoles;
    /// minimum number of planes per station
    unsigned m_minPlanesPerStation;
    /// max. chi^2/ndf for IT stubs before stub refit
    double m_itStubLooseChi2;
    /// max. chi^2/ndf for IT stubs after stub refit
    double m_itStubTightChi2;

    /// tolerance to check if a hit is within the sensitive y area of straw/sensor
    double m_yTolSensArea;

    /// min. number of stereo hits per station during per-region search (OT)
    int m_minStHitsPerStaOT;
    /// min. total number of stereo hits during per-region search (OT)
    int m_minTotStHitsOT;

    /// are we running in an online (HLT) or offline context (set in initialize)
    bool m_online;
    /// are we to activate the HLT property hack?
    /** this activates a workaround for certain problematic properties which
     * get their values set wrong when being extracted from a TCK because the
     * precision to which the numerical values of these properties is saved in
     * the TCK is insufficient
     *
     * workaround is to overwrite these properties with default values if
     * PatSeedingTool is run in an HLT context
     */
    bool m_activateHLTPropertyHack;
        
    // Where to dump debugging files
    std::string m_dumpFolder;
    
    // Offload the algorithm to an accelerator
    bool m_offload;  
  

    // Timing counters
    ISequencerTimerTool* m_timer;
    bool m_measureTime;
    int m_timeInit;
    int m_timeInitAutomata;
    int m_timeClustering;
    int m_timeMakeXTracklets;
    int m_timeAddStereo;
    int m_timeFitStubs;
    int m_timeFindNeighbours;
    int m_timeEvolve;
    int m_timeLink;
    int m_timeFit;
    int m_timeSelectLong;
    int m_timeSelectShort;
    int m_timeKillLongClones;
    int m_timeKillShortClones;
    int m_timeSerializer;
    int m_timeOffloading;

    // Miscellanea
    unsigned int m_nHitsOT;
    unsigned int m_nHitsIT;

    // status codes put into ProcStatus
    enum ProcStatusCodes {
      Success = 0,
      ETooManyHits = -3
    };
    
    // Output files 
    FILE* m_dumpFile;
    FILE* m_mvaSignalLongFile;
    FILE* m_mvaGhostsLongFile;
    FILE* m_mvaSignalShortFile;
    FILE* m_mvaGhostsShortFile;

    // MC association linkers
    Linker *m_otlinker;
    Linker *m_itlinker;

    // Containers
    Clusters m_clusters[m_nSta][m_nLay][m_nReg];
    Tracklets m_xTracklets[m_nSta][m_nReg];
    Tracklets m_trackStubs[m_nSta][m_nReg];
    CATracks m_candTracks[m_nSta][m_nReg]; // in principle, should be num. layers on track, but in this case nLayers = nStations
    CATracks m_outputTracks[m_nReg];
    std::vector<LHCb::Track*> m_lhcbTracks;

    // Serializer
    EventSerializer m_serializer;
    
    // Gpu Service
    SmartIF<IGpuService> m_gpuService;
};
#endif // AUTOMATATOOL_H


// vim:shiftwidth=2:tw=78
