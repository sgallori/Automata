#ifndef CATRACK_H
#define CATRACK_H 1

#include <vector>

#include "Tracklet.h"


//=================================================
// Cellular Automata track class
//=================================================
class CATrack
{
  /* Constants */
  // Max. num of hits on track
  const static unsigned int maxHits = Automata::MAX_HITS_ON_TRACK; 

  public:
    /* Constructors */
    CATrack() {};

    // Init. with a tracklet
    CATrack (Tracklet *tracklet) 
    {
      //m_fwdTrack = tracklet->fwdTrack(); // must be set before (needed by addHits...)
      addTracklet( tracklet );
      m_x0 = tracklet->x0();
      m_y0 = tracklet->y0();
      m_z0 = tracklet->z0();
      m_tx = tracklet->tx();
      m_ty = tracklet->ty();
      m_chi2nDoF = tracklet->chi2nDoF();
    }
    ~CATrack() {};


    /* Getters */ 
    PatSeedTrack* fwdTrack() { return m_fwdTrack; }
    PatFwdHit** hits() { return m_hits; }
    PatFwdHit* hit(unsigned int idx) const { return m_hits[idx]; }
    unsigned int nHits() const { return m_nHits; }
    Tracklet** tracklets() { return m_tracklets; }
    Tracklet* tracklet(unsigned int idx) const { return m_tracklets[idx]; }
    unsigned int nTracklets() const { return m_nTracklets; }
    double x0() const { return m_x0; }
    double y0() const { return m_y0; }
    double z0() const { return m_z0; }
    double tx() const { return m_tx; }
    double ty() const { return m_ty; }
    double chi2nDoF() const { return m_chi2nDoF; }
    double quality() const { return m_quality; }
    bool isValid() const { return m_isValid; }


    /* Setters */
    void setHits (unsigned int nHits, PatFwdHits hits) 
    {
      if (nHits > maxHits) {
	std::cout << "WARNING: You are trying to set more than " << maxHits << " hits on tracklet!" << std::endl; 
	return;
      }
      m_nHits = nHits;
      for (unsigned int i = 0; i < nHits; i++)
	m_hits[i] = hits[i];
    }
    
    void addHit (PatFwdHit *hit) 
    {  
      if (m_nHits >= maxHits) {
	std::cout << "WARNING: You are trying to add more than " << maxHits << " hits on track!" << std::endl; 
	return;
      }
      m_hits[m_nHits] = hit;
      m_nHits++;
    }
    
    void addHits (unsigned int nHits, PatFwdHit** hits) 
    {
      //if (m_fwdTrack == 0) { std::cout << "WARNING: PatSeedTrack not set on track!" << std::endl; return; }
      if ((m_nHits + nHits) > maxHits) {
	std::cout << "WARNING: You are trying to add more than " << maxHits << " hits on track!" << std::endl; 
	return;
      }
      for (unsigned int i = 0; i < nHits; i++) {
	m_hits[m_nHits + i] = hits[i];
      }
      m_nHits += nHits;
    }

    void addHits (Cluster *cl) { addHits(cl->nHits(), cl->hits()); }

    void addTracklet (Tracklet *tracklet) 
    {
      if (m_nTracklets >= 3) {
	std::cout << "WARNING: You are trying to add more than " << 3 
	          << " tracklets on track!" << std::endl; 
	return;
      }
      m_tracklets[m_nTracklets] = tracklet;
      m_nTracklets++;

      // Add tracklet hits
      addHits(tracklet->nHits(), tracklet->hits());
    }

    void setFwdTrack (Tracklet *tracklet) { m_fwdTrack = tracklet->fwdTrack(); }
    void setX0 (double x0) { m_x0 = x0; }
    void setY0 (double y0) { m_y0 = y0; }
    void setZ0 (double z0) { m_z0 = z0; }
    void setTx (double tx) { m_tx = tx; }
    void setTy (double ty) { m_ty = ty; }
    void setChi2nDoF (double chi2nDoF) { m_chi2nDoF = chi2nDoF; }
    void setParameters (double x0, double y0, double z0, double tx, double ty) 
    { setX0(x0); setY0(y0); setZ0(z0); setTx(tx); setTy(ty); }
    void setQuality (double q) { m_quality = q; }
    void setIsValid (bool flag) { m_isValid = flag; }
   

    /* Member functions */
    // Track positions at Z-value 
    double xAtZ (double z) const { return m_x0 + m_tx * (z - m_z0); } // straight line model
    double yAtZ (double z) const { return m_y0 + m_ty * (z - m_z0); } // straight line model

    // Return number of used tracklets on track
    unsigned int nUsedTracklets() 
    {
      unsigned int nUsed = 0;
      for (unsigned int i = 0; i < m_nTracklets; i++)
	if (m_tracklets[i]->isUsed()) nUsed++;
      return nUsed;
    } 

    // Return the number of used hits
    unsigned int nUsedHits() 
    {
      unsigned int nUsed = 0;
      for (unsigned int i = 0; i < m_nHits; i++)
	if (m_hits[i]->isUsed()) nUsed++;
      return nUsed;
    }

    // Return the fraction of used hits wrt total 
    double fracUsedHits () 
    {
      return double(nUsedHits()) / double(m_nHits);
    }
  
    // Return first/last station on track
    unsigned int firstStation() const { return m_tracklets[0]->station(); }
    unsigned int lastStation() const { return m_tracklets[m_nTracklets - 1]->station(); }

    // Returns all LHCbIDs
    std::vector<LHCb::LHCbID> lhcbIDs() 
    { 
      std::vector<LHCb::LHCbID> tmp(m_nHits);
      for (unsigned int i = 0; i < m_nHits; i++)
	tmp[i] = m_hits[i]->hit()->lhcbID();
      return tmp;
    }


  private:
    PatSeedTrack* m_fwdTrack = 0;        // associated PatSeedTrack
    Tracklet* m_tracklets[3];            // tracklets on track  
    PatFwdHit* m_hits[maxHits] = {0};    // hits on track
    unsigned int m_nHits = 0;            // num. of hits on track
    unsigned int m_nTracklets = 0;       // num. of tracklets on track
    double m_x0, m_y0, m_z0, m_tx, m_ty; // track params.
    double m_chi2nDoF = 0.0;             // chi2/nDoF 
    double m_quality = 0.0;              // quality 
    bool m_isValid = false;              // track is valid and can be stored
};
typedef std::vector<CATrack> CATracks; 
#endif
