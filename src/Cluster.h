#ifndef CLUSTER_H
#define CLUSTER_H 1

#include <vector>

#include "PatKernel/PatForwardHit.h"

#include "Automata.h"


//=================================================
// Cluster class
//=================================================
class Cluster
{
  /* Constants */
  // Max. num of hits
  const static unsigned int maxHits = Automata::MAX_HITS_ON_CLUS; 

  public:
    // Constructors
    Cluster() {} 
    ~Cluster() {};

    // One hit
    Cluster (PatFwdHit *h1) 
    { 
      addHit( h1 ); 
    }

    // Two hits
    Cluster (PatFwdHit *h1, PatFwdHit *h2)
    {
      addHit( h1 );
      addHit( h2 );

      // Use weighted average...
      //const Tf::OTHit* otHit1 = h1->hit()->othit();
      //const Tf::OTHit* otHit2 = h2->hit()->othit();
      //if (otHit1 != 0 && otHit2 != 0) {
      //  const double rr1 = otHit1->untruncatedDriftDistance(0.);
      //  const double r1 = std::sqrt(otHit1->variance() + rr1 * rr1);
      //  const double rr2 = otHit2->untruncatedDriftDistance(0.);
      //  const double r2 = std::sqrt(otHit2->variance() + rr2 * rr2);
      //  // cluster position should be closer to the hit with the smaller drift radius
      //  m_x = (h1->hit()->xAtYEq0() * r2 + h2->hit()->xAtYEq0() * r1) / (r1 + r2);
      //  m_z = (h1->hit()->zAtYEq0() * r2 + h2->hit()->zAtYEq0() * r1) / (r1 + r2);
      //} 
    }


    /* Setters */
    void addHit (PatFwdHit *hit) 
    {  
      if (m_nHits >= maxHits) {
	std::cout << "WARNING: You are trying to add more than " << maxHits << " hits on cluster!" << std::endl; 
	return;
      }

      m_hits[m_nHits] = hit;
      m_nHits++; 
      updatePosition();
    }

    void updatePosition() 
    {
      m_x = m_z = 0.0;
      if (m_nHits == 0) return;

      for (unsigned int i = 0; i < m_nHits; i++) {
	m_x += m_hits[i]->hit()->xAtYEq0();
	m_z += m_hits[i]->hit()->zAtYEq0();
      }
      m_x /= m_nHits;
      m_z /= m_nHits;
    }

    void setX (double x) { m_x = x; }
    void setZ (double z) { m_z = z; }
    void setUsed (bool flag = true) { m_isUsed = flag; }


    /* Getters */
    PatFwdHit** hits() { return m_hits; }
    PatFwdHit* hit(unsigned int idx) { return m_hits[idx]; }
    double x() const { return m_x; }
    double z() const { return m_z; }
    bool isUsed() const { return m_isUsed; }


    /* Member functions */
    // Number of hits
    unsigned int nHits() const { return m_nHits; }

    // Tell if the cluster is made by two hits
    bool isCluster() const { return (m_nHits > 1); }

    // Station of the hit
    unsigned int station() const
    { return (m_nHits > 0) ? m_hits[0]->hit()->station() : Automata::BigInt; }     

    // Layer of the hit
    unsigned int layer() const
    { return (m_nHits > 0) ? m_hits[0]->hit()->layer() : Automata::BigInt; }     

    // Region of the hit
    unsigned int region() const
    { return (m_nHits > 0) ? m_hits[0]->hit()->region() : Automata::BigInt; }     

    // Type of the hit
    unsigned int type() const
    { return (m_nHits > 0) ? m_hits[0]->hit()->type() : Automata::BigInt; }     

    // Stereo angles (store in a CA::constant?)
    double dxDy() const 
    { return (m_nHits > 0) ? m_hits[0]->hit()->dxDy() : Automata::BigDouble; }     
    
    double dzDy() const 
    { return (m_nHits > 0) ? m_hits[0]->hit()->dzDy() : Automata::BigDouble; }     

    // LHCbIDs
    std::vector<LHCb::LHCbID> lhcbIDs() 
    { 
      std::vector<LHCb::LHCbID> tmp(m_nHits);
      for (unsigned int i = 0; i < m_nHits; i++)
	tmp[i] = m_hits[i]->hit()->lhcbID();
      return tmp;
    }

    // Compare two hits by layer
    static bool compareByLayer (Cluster *cl1, Cluster *cl2) { return (cl1->layer() < cl2->layer()); }

    // Delete cluster pointer
    static void deleteCluster (Cluster* cl) { delete cl; };

  private:
    PatFwdHit* m_hits[maxHits] = {0}; // Hits of the cluster
    unsigned int m_nHits = 0; // Actual number of hits
    double m_x = 0.0; // X pos.
    double m_z = 0.0; // Z pos.
    bool m_isUsed = false; // Used flag
};
typedef std::vector<Cluster*> Clusters;
#endif



