#include <algorithm>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <sstream>

#include "EventSerializer.h"

//===================================
// Serialize hits in (sta,lay,reg)
//===================================
void EventSerializer::serialize (uint32_t sta, uint32_t reg, uint32_t lay,
                                 HitRange &hits) 
{
  // Number of hits
  const size_t NumHits = hits.size();
  const int    pad = 1024;

  // Data buffer we transmit to server
  const size_t IdxStaSize     =  sizeof(uint32_t); // #sta idx info
  const size_t IdxRegSize     =  sizeof(uint32_t); // #reg idx info
  const size_t IdxLaySize     =  sizeof(uint32_t); // #lay idx info
  const size_t NumHitsSize    =  sizeof(uint32_t); // #clusters info
  const size_t hitIDsSize     =  pad * sizeof(uint32_t); // hits id info
  const size_t hitXsSize      =  pad * sizeof(float); // hits x info
  const size_t hitZsSize      =  pad * sizeof(float); // hit z info
  const size_t hitWeightsSize =  pad * sizeof(float); // hit weight info
  const size_t hitRadiiSize   =  pad * sizeof(float); // hit drift radius info
  const size_t hitYBegSize    =  pad * sizeof(float); // hit yBegin info (geom.)
  const size_t hitYEndSize    =  pad * sizeof(float); // hit yEnd info (geom.)

  Data buffer( IdxStaSize + IdxRegSize + IdxLaySize + NumHitsSize + 
               hitIDsSize + hitXsSize + hitZsSize + hitWeightsSize + hitRadiiSize +
               hitYBegSize + hitYEndSize);

  // Put data into the buffer
  uint8_t * dst = buffer.data();
  *(uint32_t *)dst = sta; dst += IdxStaSize;
  *(uint32_t *)dst = reg; dst += IdxRegSize;
  *(uint32_t *)dst = lay; dst += IdxLaySize;
  *(uint32_t *)dst = NumHits; dst += NumHitsSize;

  uint32_t * hitIDs      =  (uint32_t*) dst; dst += hitIDsSize;
  float   * hitXs        =  (float*)   dst; dst += hitXsSize;
  float   * hitZs        =  (float*)   dst; dst += hitZsSize;
  float   * hitWeights   =  (float*)   dst; dst += hitWeightsSize;
  float   * hitRadii     =  (float*)   dst; dst += hitRadiiSize;
  float   * hitYBeg      =  (float*)   dst; dst += hitYBegSize;
  float   * hitYEnd      =  (float*)   dst; dst += hitYEndSize;

  // Sanity check
  if (dst != buffer.data() + buffer.size())
    throw std::runtime_error("event serialization failed");

  // Fill arrays 
  for (PatFwdHit *hit : hits) {
    *hitIDs = hit->hit()->lhcbID().lhcbID(); ++hitIDs;
    *hitXs = hit->hit()->xAtYEq0(); ++hitXs;
    *hitZs = hit->hit()->zAtYEq0(); ++hitZs;
    *hitWeights = hit->hit()->weight(); ++hitWeights;
    *hitRadii = hit->driftDistance(); ++hitRadii;
    *hitYBeg = hit->hit()->yBegin(); ++hitYBeg;
    *hitYEnd = hit->hit()->yEnd(); ++hitYEnd;

  }
  // Accumulate on main buffer (by increasing sta, lay, reg)
  m_buffer.insert(m_buffer.end(), buffer.begin(), buffer.end());
}


