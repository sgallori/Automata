#ifndef TSTATIONS_SERIALIZER_H
#define TSTATIONS_SERIALIZER_H 1

#include <ostream>
#include <vector>

#include "TfKernel/TStationHitManager.h"

#include "CATrack.h"
#include "Event/Track.h"

struct GpuOutTrack {
// output tracks
  double x0;
  double y0;
  double tx;
  double ty;
  double curvature;
  // "dof" values
  double chi2xdof;
  // lhcbID hits
  int   nHits;
  int   hit[24];
};

//====================================
// Event serializer for Automata
//====================================
class EventSerializer 
{
  public:
    typedef std::vector<uint8_t> Data;
    typedef Tf::TStationHitManager<PatForwardHit>::HitRange HitRange;
 
    // Serialized track structure.
    struct GpuTrack 
    { 
      uint32_t hitsNum;
      uint32_t hits[Automata::MAX_HITS_ON_TRACK];
    };
  
  public:
    /* Constructor */
    EventSerializer() {}

    /* Serialize hits in (sta, lay, reg) */ 
    void serialize (uint32_t sta, uint32_t lay, uint32_t reg,
                    HitRange &hits);

    /* Reset event */
    void reset() {
      m_hasData = false;
      m_buffer.clear();
    };

    /* Return serialized data */
    Data buffer() { return m_buffer; }

  private:
    // Data members
    bool m_hasData = false;
    Data m_buffer; 
};
#endif
