// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "OTDet/DeOTDetector.h"
// from TrackEvent
#include "Event/StateParameters.h"
#include "Event/Track.h"

// local
#include "GalloSeeding.h"

//-----------------------------------------------------------------------------
// Implementation file for class : GalloSeeding
//
// 201-02-26 : Stefano "Callot" Gallorini
//-----------------------------------------------------------------------------


// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( GalloSeeding )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
GalloSeeding::GalloSeeding( const std::string& name,
                        ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator )
  //, m_seedingTool(NULL)
  , m_timerTool(NULL)
  , m_seedTime(0)
{
  declareProperty( "OutputTracksName",  m_outputTracksName = "Rec/Track/Automata" );
  declareProperty( "TimingMeasurement", m_doTiming = false);
}


//=============================================================================
// Destructor
//=============================================================================
GalloSeeding::~GalloSeeding() {}


//=============================================================================
// Initialization
//=============================================================================
StatusCode GalloSeeding::initialize()
{
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  
  m_automataTool = tool<AutomataTool>("AutomataTool", "AutomataTool", this);

  if ( m_doTiming) {
    // Stefano
    m_timerTool = tool<ISequencerTimerTool>( "SequencerTimerTool" , this ); // Reminder: added this 
    m_seedTime = m_timerTool->addTimer( "GalloSeeding total" ); 
  }

  return StatusCode::SUCCESS;
}


//=============================================================================
// Main execution
//=============================================================================
StatusCode GalloSeeding::execute()
{
  if ( m_doTiming ) m_timerTool->start( m_seedTime );

  LHCb::Tracks* outputTracks = new LHCb::Tracks();

  // Prepare hits
  m_automataTool->prepareHits();

  // Run GalloSeeding
  StatusCode sc = m_automataTool->cellularAutomata( outputTracks );
  put(outputTracks, m_outputTracksName);

  if ( m_doTiming ) m_timerTool->stop( m_seedTime );

  return sc;
}
   

//=============================================================================
//  Finalize
//=============================================================================
StatusCode GalloSeeding::finalize()
{
  if( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
