#ifndef GALLOSEEDING_H
#define GALLOSEEDING_H 1

// Include files
// from Gaudi
#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "AutomataTool.h"

//#include "TrackInterfaces/IPatSeedingTool.h"

/** @class GalloSeeding GalloSeeding.h
 *
 *  Gallo Seeding algorithm.
 *
 *  @author Stefano "Callot" Gallorini
 *  @date   2015-02-26 Initial version
 */

class GalloSeeding : public GaudiAlgorithm
{
  public:
    /// Standard constructor
    GalloSeeding( const std::string& name, ISvcLocator* pSvcLocator );

    virtual ~GalloSeeding( ); ///< Destructor

    virtual StatusCode initialize();    ///< Algorithm initialization
    virtual StatusCode execute   ();    ///< Algorithm execution
    virtual StatusCode finalize  ();    ///< Algorithm finalization

  private:
    std::string m_outputTracksName;
    AutomataTool* m_automataTool; 
    ISequencerTimerTool* m_timerTool;
    int  m_seedTime;
    bool m_doTiming;
};

#endif // GALLOSEEDING_H
