#ifndef TRACKLET_H
#define TRACKLET_H 1

#include <vector>

#include "PatSeedTool.h"
#include "PatSeedTrack.h"

#include "Cluster.h"


//=================================================
// Tracklet class
//=================================================
class Tracklet
{
  /* Constants */
  const static unsigned int maxClusters = Automata::MAX_CLUS_ON_STUB;   // max. num. clusters on tracklet
  const static unsigned int maxHits = Automata::MAX_HITS_ON_STUB;       // max. num. hits on tracklet
  const static unsigned int maxNeighbours = Automata::MAX_NB_PER_STUB;  // max. num. neighbours

  public:
    Tracklet() {};

    // constructor for x-tracklets
    Tracklet (Cluster *clX1, 
    	      Cluster *clX2) 
    { 
      addCluster( clX1 ); 
      addCluster( clX2 ); 
    };

    // constructor for a stub
    Tracklet (Cluster *clX1, 
	      Cluster *clU,
	      Cluster *clV,
	      Cluster *clX2)
    { 
      addCluster( clX1 );
      addCluster( clU );
      addCluster( clV );
      addCluster( clX2 );
    };

    ~Tracklet() {};


    /* Setters */
    void setNHits (unsigned int n) { m_nHits = n; }

    void setHit (unsigned int idx, PatFwdHit* hit) 
    { 
      if (idx >= m_nHits) return; 
      m_hits[idx] = hit; 
    } 

    void setHits (unsigned int nHits, PatFwdHit** hits) 
    {
      if (nHits > maxHits) {
	std::cout << "WARNING: You are trying to set more than " << maxHits << " hits on tracklet!" << std::endl; 
	return;
      }
      m_nHits = nHits;
      for (unsigned int i = 0; i < nHits; i++)
	m_hits[i] = hits[i];
    }
    
    void setHits (unsigned int nHits, PatFwdHits hits) 
    {
      if (nHits > maxHits) {
	std::cout << "WARNING: You are trying to set more than " << maxHits << " hits on tracklet!" << std::endl; 
	return;
      }
      m_nHits = nHits;
      for (unsigned int i = 0; i < nHits; i++)
	m_hits[i] = hits[i];
    }

    void addHit (PatFwdHit *hit) 
    {  
      if (m_nHits >= maxHits) return;
      m_hits[m_nHits] = hit;
      m_nHits++;
    }
    
    void addHits (unsigned int nHits, PatFwdHit** hits) 
    {
      if ((m_nHits + nHits) > maxHits) {
	std::cout << "WARNING: You are trying to add more than " << maxHits << " hits on track!" << std::endl; 
	return;
      }
      for (unsigned int i = 0; i < nHits; i++) {
	m_hits[m_nHits + i] = hits[i];
      }
      m_nHits += nHits;
    }

    void addHits (Cluster *cl) { addHits(cl->nHits(), cl->hits()); }

    void setNClusters (unsigned int n) { m_nClusters = n; }

    void addClusters (unsigned int nClusters, Cluster** clusters) 
    {  
      if ((m_nClusters + nClusters) > maxClusters) {
	std::cout << "WARNING: You are trying to set more than " << maxClusters << " clusters on tracklet!" << std::endl; 
	return;
      }
      for (unsigned int i = 0; i < nClusters; i++) {
	m_clusters[m_nClusters + i] = clusters[i];
	addHits( clusters[i] );
      }
      m_nClusters += nClusters;
    }

    void addCluster (Cluster *cl) 
    {  
      if (m_nClusters >= maxClusters) return;
      m_clusters[m_nClusters] = cl;
      m_nClusters++;
      addHits( cl );
    }
    
    void setX0 (double x0) { m_x0 = x0; }
    void setY0 (double y0) { m_y0 = y0; }
    void setZ0 (double z0) { m_z0 = z0; }
    void setTx (double tx) { m_tx = tx; }
    void setTy (double ty) { m_ty = ty; }
    void setChi2nDoF (double chi2nDoF) { m_chi2nDoF = chi2nDoF; }
    void setIsUsed (bool flag) { m_isUsed = flag; }
    void setStation (unsigned int station) { m_station = station; } 
    
    void setParameters (double x0, double y0, double z0, double tx, double ty) 
    { setX0(x0); setY0(y0); setZ0(z0); setTx(tx); setTy(ty); }
   
    void setFwdTrack (double zRef, double dRatio, double initialArrow) 
    {
      if (m_nClusters < 4) {
	std::cout << "Cannot set the track! Hits on tracklet not enough to build a PatSeedTrack!" << std::endl;
	return;
      }

      // assume first four clusters ordered by layer (0,1,2,3)
      PatFwdHit** hits0 = m_clusters[0]->hits();
      PatFwdHit** hits1 = m_clusters[1]->hits();
      PatFwdHit** hits2 = m_clusters[2]->hits();
      PatFwdHit** hits3 = m_clusters[3]->hits();

      m_fwdTrack = PatSeedTrack( hits0[0], 
                                 hits1[0],
                           	 hits2[0],
                         	 hits3[0],
                         	 zRef,
	                         dRatio,
  	                         initialArrow );

      // add remaining cluster hits
      if (m_clusters[0]->isCluster()) m_fwdTrack.addCoord( hits0[1] );	
      if (m_clusters[1]->isCluster()) m_fwdTrack.addCoord( hits1[1] );	
      if (m_clusters[2]->isCluster()) m_fwdTrack.addCoord( hits2[1] );	
      if (m_clusters[3]->isCluster()) m_fwdTrack.addCoord( hits3[1] );	

      for (unsigned int icl = 4; icl < m_nClusters; icl++) {
	Cluster *cl = m_clusters[icl];
	for (unsigned int i = 0; i < cl->nHits(); i++) 
	  m_fwdTrack.addCoord( cl->hit(i) );
      }

    }


    /* Getters */
    Cluster** clusters() { return m_clusters; }
    PatFwdHit** hits() { return m_hits; }
    PatSeedTrack* fwdTrack() { return &m_fwdTrack; }
    Tracklet** neighbours() { return m_neighbours; }
    PatFwdHit* hit(unsigned int idx) const { return m_hits[idx]; }
    Cluster* cluster(unsigned int idx) const { return m_clusters[idx]; }
    Tracklet* neighbour(unsigned int idx) const { return m_neighbours[idx]; }
    double x0() const { return m_x0; }
    double y0() const { return m_y0; }
    double z0() const { return m_z0; }
    double tx() const { return m_tx; }
    double ty() const { return m_ty; }
    double chi2nDoF() const { return m_chi2nDoF; }
    unsigned int nNeighbours() const { return m_nNeighbours; }
    unsigned int nClusters() const { return m_nClusters; }
    unsigned int nHits() const { return m_nHits; }
    unsigned int counter() const { return m_counter; }
    bool isUsed() const { return m_isUsed; }


    /* Member functions */
    // Number of hits on layer 
    unsigned int nHits (unsigned int layer) 
    { return (hasLayer(layer) ? m_clusters[layer]->nHits() : 0); }
    //{ return (m_hits[2*layer] != 0) + (m_hits[2*layer +1] != 0); }

    // Tell if the tracklet has a cluster in layer
    bool hasLayer (unsigned int layer) { return (m_clusters[layer] != 0); }
    bool hasX1() const { return (m_clusters[0] != 0); } 
    bool hasU() const { return (m_clusters[1] != 0); } 
    bool hasV() const { return (m_clusters[2] != 0); } 
    bool hasX2() const { return (m_clusters[3] != 0); } 

    // Tracklet positions at Z-value 
    double xAtZ(double z) const { return m_x0 + m_tx * (z - m_z0); } // straight line
    double yAtZ(double z) const { return m_y0 + m_ty * (z - m_z0); } // straight line
    double x(double z) const { return m_x0 + m_tx * (z - m_z0); } 
    double y(double z) const { return m_y0 + m_ty * (z - m_z0); } 

    // Returns radius at z0
    double r0() const { return sqrt(m_x0*m_x0 + m_y0*m_y0); }

    // Tracklet angle in X-Z plane
    double cosXZ() const { return 1.0 / sqrt(1.0 + m_tx * m_tx); }

    // Tracklet kick (tx(aftMag) - tx(befMag)) 
    double kick() 
    {
      double zMag = Automata::zMagnet;

      // Slopes before magnet
      double txBef = xAtZ( zMag ) / zMag;
      double tyBef = yAtZ( zMag ) / zMag;
      return ( m_tx / sqrt(1.0 + m_tx * m_tx + m_ty * m_ty) )
	    -( txBef / sqrt(1.0 + txBef * txBef + tyBef * tyBef) );
    }

    // Momentum from kick method (actually |p/q|...)
    double momentum() 
    {
      double akick = std::abs( kick() );
      double a = Automata::magKick;
      double b = 605.0 / 1000.0;
      double c = 2656.0 / 1000.0;
      return ( akick > 0.0 ? (a + b*m_tx*m_tx + c*m_ty*m_ty) / akick : 0.0 );
    }

    // Add neighbour to tracklet
    void addNeighbour (Tracklet* nb)
    {
      if (m_nNeighbours >= maxNeighbours) {
	std::cout << "WARNING: You are trying to set more than " << 
	  maxNeighbours << " neighbours on tracklet!" << std::endl; 
	return;
      }

      m_neighbours[m_nNeighbours] = nb;
      m_nNeighbours++;
    }
    
    // Increment counter
    void incrementCounter() { m_counter++; }

    // Compare two tracklets by tx
    static bool lowerByTx (const Tracklet& lhs, const Tracklet& rhs) 
    { return (lhs.tx() < rhs.tx()); }

    // Compare two tracklets by x
    static bool lowerByX0 (const Tracklet& lhs, const Tracklet& rhs) 
    { return (lhs.x0() < rhs.x0()); }

    // Compare two tracklets by y0
    static bool lowerByY0 (const Tracklet& lhs, const Tracklet& rhs) 
    { return (lhs.y0() < rhs.y0()); }

    // Compare two tracklets by ty
    static bool lowerByTy (const Tracklet& lhs, const Tracklet& rhs) 
    { return (lhs.ty() < rhs.ty()); }

    // Returns tracklet station
    unsigned int station() const { return m_station; }

    // Returns all LHCbIDs
    std::vector<LHCb::LHCbID> lhcbIDs() 
    { 
      std::vector<LHCb::LHCbID> tmp(m_nHits);
      for (unsigned int i = 0; i < m_nHits; i++)
	tmp[i] = m_hits[i]->hit()->lhcbID();
      return tmp;
    }
  
    // Return worst cluster index
    unsigned int worstClusterIndex() 
    {
      unsigned int iWorst = 0;
      double worstChi2 = 0.0;

      for (unsigned int i = 0; i < m_nClusters; i++) {
	Cluster *cl = m_clusters[i];
	double chi2 = chi2Cluster( cl );
	if (chi2 > worstChi2) {
	  worstChi2 = chi2;
	  iWorst = i;
	}
      }

      return iWorst;
    }
 
    // Remove cluster from array
    void removeCluster(unsigned int idx) 
    {
      // re-indexing 
      for (unsigned int i = idx; i < (m_nClusters - 1); i++) 
	m_clusters[i] = m_clusters[i+1];

      m_nClusters--;

      // add hits from scratch
      m_nHits = 0;
      for (unsigned int i = 0; i < m_nClusters; i++) 
	addHits( m_clusters[i] );
    }



    //===============================================================================
    // Fitter utilities
    //===============================================================================
    // little helper class to set ambiguities in a pair of hits
    template <bool sameSign>
    struct HitPairAmbSetter {
      /// set ambiguity of h1 to amb, h2 according to sameSign
      template <class HIT>
      static inline void set(HIT& h1, HIT& h2, int amb)
      { h1->setRlAmb(amb); h2->setRlAmb(sameSign ? amb : -amb); }
    };
   
    // Resolve left-right ambiguities using two hits in different monolayers
    void resolveAmb(PatFwdHit *h1, PatFwdHit *h2) 
    {
      // have two neighbouring hits, calculate pitch residual
      // work out effective slope in module frame
      const auto sinT = h1->hit()->sinT(), cosT = h1->hit()->cosT();
      typedef decltype(sinT) F;
      //const auto z0 = (h1->hit()->zAtYEq0() + h2->hit()->zAtYEq0()) / F(2);
      const auto txeff = m_tx * cosT + m_ty * sinT;
      // get 2D wire distance vector in module coordinates
      const auto dx = (h1->hit()->xAtYEq0() - h2->hit()->xAtYEq0()) * cosT;
      const auto dzdy = h1->hit()->dzDy();
      const auto dz = (h1->hit()->zAtYEq0() - h2->hit()->zAtYEq0()) /
	std::sqrt(F(1) + dzdy * dzdy);
      // calculate effective pitch (i.e. pitch seen by tr)
      const auto scale = (dx * txeff + dz) / (F(1) + txeff * txeff);
      const auto eprx = dx - scale * txeff, eprz = dz - scale;
      const auto epr = std::sqrt(eprx * eprx + eprz * eprz);
      // get radii
      const auto r1 = h1->driftDistance(), r2 = h2->driftDistance();
      // calculate pitch residual (convert back from effective pitch)
      // case 1: track passes between hits, case 2: not case 1
      const auto pr1 = std::abs(dx / epr) * (epr - r1 - r2);
      const auto pr2 = std::abs(dx / epr) * (epr - std::abs(r1 - r2));
      if (std::abs(pr1) <= std::abs(pr2)) {
	// set ambiguities accordingly
	HitPairAmbSetter<false>::set(h1, h2, (dx > F(0)) ? -1 : +1);
      } else {
	// try to figure out ambiguity by comparing slope estimate obtained
	// from drift radii (assuming hits do not pass in between the wires)
	// to the (well measured) effective slope
	//
	// as the slopes are usually big, we have to correct for the fact that
	// the radius is no longer in x direction (using the measured txeff)
	const auto corr = std::sqrt(F(1) + txeff * txeff);
	const auto dslplus = (dx + (r1 - r2) * corr) - txeff * dz;
	const auto dslminus = (dx - (r1 - r2) * corr) - txeff * dz;
	HitPairAmbSetter<true>::set(h1, h2,
	    (std::abs(dslplus) < std::abs(dslminus)) ? +1 : -1);
      }
    }

    // Returns pitch resisual
    double pitchResidual (Cluster *cl) 
    {
      if (!cl->isCluster()) return -999.0;
      PatFwdHit *h1 = cl->hit(0);
      PatFwdHit *h2 = cl->hit(1);

      // have two neighbouring hits, calculate pitch residual
      // work out effective slope in module frame
      const auto sinT = h1->hit()->sinT(), cosT = h1->hit()->cosT();
      typedef decltype(sinT) F;
      //const auto z0 = (h1->hit()->zAtYEq0() + h2->hit()->zAtYEq0()) / F(2);
      const auto txeff = m_tx * cosT + m_ty * sinT;
      // get 2D wire distance vector in module coordinates
      const auto dx = (h1->hit()->xAtYEq0() - h2->hit()->xAtYEq0()) * cosT;
      const auto dzdy = h1->hit()->dzDy();
      const auto dz = (h1->hit()->zAtYEq0() - h2->hit()->zAtYEq0()) /
	std::sqrt(F(1) + dzdy * dzdy);
      // calculate effective pitch (i.e. pitch seen by tr)
      const auto scale = (dx * txeff + dz) / (F(1) + txeff * txeff);
      const auto eprx = dx - scale * txeff, eprz = dz - scale;
      const auto epr = std::sqrt(eprx * eprx + eprz * eprz);
      // get radii
      const auto r1 = h1->driftDistance(), r2 = h2->driftDistance();
      // calculate pitch residual (convert back from effective pitch)
      // case 1: track passes between hits, case 2: not case 1
      const auto pr_plus = std::abs(dx / epr) * (r1 + r2 - epr);
      const auto pr_minus = std::abs(dx / epr) * (std::abs(r1 - r2) - epr);

      if (std::abs(pr_plus) <= std::abs(pr_minus)) return pr_plus;
      else return pr_minus;
	    
      return -999.0;
    }

    // resolve left-right ambiguities of a cluster
    void resolveAmb(Cluster *cl) 
    {
      if (!cl->isCluster()) return;
      else resolveAmb(cl->hit(0), cl->hit(1));
    }
 
    // distance track to hit (use actual xAtZ of the tracklet)
    double distance(const PatFwdHit* hit) const
    {
      double y = ( yAtZ(0.0) + m_ty * hit->hit()->zAtYEq0() ) / ( 1. - hit->hit()->dzDy() * m_ty );
      //double y = ( 0.0 + m_ty * hit->hit()->zAtYEq0() ) / ( 1. - hit->hit()->dzDy() * m_ty );
      double xHit = hit->hit()->xAtYEq0() + y * hit->hit()->dxDy();
      double zHit = hit->hit()->zAtYEq0() + y * hit->hit()->dzDy();
      double dist = xHit - xAtZ( zHit );
      if (hit->hit()->type() == Tf::RegionID::IT) return dist;
      dist *= cosXZ();
      double dx = hit->driftDistance();
      //const Tf::OTHit* otHit = hit->hit()->othit();
      //if (otHit) dx = otHit->untruncatedDriftDistance(y);
      if (fabs( dist - dx ) < fabs( dist + dx ))
	return dist - dx;
      else
	return dist + dx;
    }

    // distance track to hit with ambiguity fixed
    double distanceWithRL( const PatFwdHit* hit ) const
    {
      if (0 == hit->rlAmb() || hit->hit()->type() == Tf::RegionID::IT)
       return distance( hit );
      const double dist = hit->x() - xAtZ(hit->z());
      const double dx = hit->driftDistance() / cosXZ();
      if (0 < hit->rlAmb())
	return dist + dx;
      else
	return dist - dx;
    }

    // chi2 contribution of a hit (take min distance)
    double chi2Hit(const PatFwdHit* hit) const
    {
      const double dist = distance(hit) * hit->hit()->errweight();
      return dist * dist;
    }
    
    // chi2 contribution of a hit (when ambiguities are fixed)
    double chi2HitRLAmb(const PatFwdHit* hit) const
    {
      const double dist = distanceWithRL(hit) * hit->hit()->errweight();
      return dist * dist;
    }

    // cluster chi2
    double chi2Cluster(unsigned int icl) {
      if (icl >= m_nClusters) return 0.0;

      double chi2 = 0.0;
      Cluster *cl = m_clusters[icl];
      for (unsigned int ih = 0; ih < cl->nHits(); ih++) {
	PatFwdHit *hit = cl->hit(ih);
	chi2 += chi2Hit(hit);
      }
      return chi2;
    }

    // cluster chi2
    double chi2Cluster(Cluster *cl) {
      double chi2 = 0.0;
      for (unsigned int ih = 0; ih < cl->nHits(); ih++) {
	PatFwdHit *hit = cl->hit(ih);
	chi2 += chi2Hit(hit);
      }
      return chi2;
    }

    // Fit tracklets and store chi2 via PatSeedTool fitters
    bool fit(PatSeedTool *tool, double initialArrow) 
    { 
      bool okFit = tool->refitStub(m_fwdTrack, initialArrow);

      if (!okFit) {
	m_chi2nDoF = Automata::BigDouble;
      } 

      else {
	// Update x position according to fitted y
	m_fwdTrack.updateHits(); 

	// Re-compute parameters after fit
	double chi2nDoF = 0.0;
	for (const PatFwdHit* hit : m_fwdTrack.coords())
	  chi2nDoF += m_fwdTrack.chi2Hit( hit );
	chi2nDoF /= double(m_fwdTrack.nCoords() - 3);

	// Update params
	setX0( m_fwdTrack.xAtZ( z0() ) );
	setY0( m_fwdTrack.yAtZ( z0() ) );
	setTx( m_fwdTrack.xSlope( z0() ) );
	setTy( m_fwdTrack.ySlope( z0() ) );
	setChi2nDoF ( chi2nDoF );
      }

      return okFit;
    }

    // find iterator of hit with worst chi2
    PatFwdHits::iterator worstHit() 
    {  
      PatFwdHits::iterator worst = m_fwdTrack.coordBegin();
      double worstChi2 = 0.0;
      for (auto it = worst; m_fwdTrack.coordEnd() != it; ++it) {
	double chi2 = m_fwdTrack.chi2Hit(*it);
	if (chi2 > worstChi2) {
	  worstChi2 = chi2;
	  worst = it;
	}
      }

      // using arrays
      //unsigned int iWorst;
      //for (unsigned int ih = 0; ih < m_nHits; ih++) {
      //  FwdHit *hit = m_hits[ih];
      //  double chi2 = chi2Hit(hit);
      //  if (chi2 > worstChi2) {
      //    worstChi2 = chi2;
      //    iWorst = ih;
      //  }
      //}

      return worst;
    }
    //===============================================================================
    // End fitter stuff
    //===============================================================================


  private:
    // Members
    Cluster* m_clusters[maxClusters] = {0};       // clusters on tracklet (max 4 clusters)
    PatFwdHit* m_hits[maxHits] = {0};             // hits on tracklet (max 2 hits / cluster)
    Tracklet* m_neighbours[maxNeighbours] = {0};  // hits on tracklet (max 2 hits / cluster)
    PatSeedTrack m_fwdTrack;                      // associated PatSeedTrack
    bool m_isUsed = false;                        // used flag 
    unsigned int m_station = 0;                   // station of tracklet
    unsigned int m_nNeighbours = 0;               // num. neighbours / tracklet
    unsigned int m_nClusters = 0;                 // num. of clusters on tracklet
    unsigned int m_nHits = 0;                     // num. of hits on tracklet
    unsigned int m_counter = 0;                   // counter
    double m_chi2nDoF;                            // chi2/nDoF 
    double m_x0 = 0.0, m_y0 = 0.0, m_z0 = 0.0;    // tracklet params.
    double m_tx = 0.0, m_ty = 0.0;                
};
typedef std::vector<Tracklet> Tracklets;
#endif

